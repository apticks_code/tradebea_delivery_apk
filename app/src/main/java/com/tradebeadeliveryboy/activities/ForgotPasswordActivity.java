package com.tradebeadeliveryboy.activities;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.core.content.ContextCompat;

import com.google.gson.Gson;
import com.hbb20.CountryCodePicker;
import com.tradebeadeliveryboy.ApiCalls.ForgotPasswordApiCall;
import com.tradebeadeliveryboy.R;
import com.tradebeadeliveryboy.interfaces.HttpReqResCallBack;
import com.tradebeadeliveryboy.models.responseModels.forgotPasswordResponse.ForgotPasswordResponse;
import com.tradebeadeliveryboy.utils.Constants;

public class ForgotPasswordActivity extends BaseActivity implements View.OnClickListener, HttpReqResCallBack {

    private TextView tvSubmit;
    private LinearLayout llSubmit;
    private ImageView ivBackArrow;
    private EditText etPhoneNumber;
    private CountryCodePicker countryCodePicker;

    private boolean isValidMobileNumber;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_forgot_password);
        setStatusBarColor();
        initializeUi();
        initializeListeners();
        prepareCountryCodePickerDetails();
    }

    private void setStatusBarColor() {
        Window window = getWindow();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.setStatusBarColor(ContextCompat.getColor(this, R.color.white));
        }
    }

    private void initializeUi() {
        tvSubmit = findViewById(R.id.tvSubmit);
        llSubmit = findViewById(R.id.llSubmit);
        ivBackArrow = findViewById(R.id.ivBackArrow);
        etPhoneNumber = findViewById(R.id.etPhoneNumber);
        countryCodePicker = findViewById(R.id.countryCodePicker);
    }

    private void initializeListeners() {
        tvSubmit.setOnClickListener(this);
        llSubmit.setOnClickListener(this);
        ivBackArrow.setOnClickListener(this);
    }

    private void prepareCountryCodePickerDetails() {
        countryCodePicker.registerCarrierNumberEditText(etPhoneNumber);
        countryCodePicker.setPhoneNumberValidityChangeListener(new CountryCodePicker.PhoneNumberValidityChangeListener() {
            @Override
            public void onValidityChanged(boolean isValidNumber) {
                isValidMobileNumber = isValidNumber;
                if (isValidNumber) {
                    etPhoneNumber.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_valid, 0);
                } else {
                    etPhoneNumber.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                }
            }
        });
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id) {
            case R.id.ivBackArrow:
                onBackPressed();
                break;
            case R.id.llSubmit:
            case R.id.tvSubmit:
                prepareSubmitDetails();
                break;
            default:
                break;
        }
    }

    private void prepareSubmitDetails() {
        String mobileNumber = etPhoneNumber.getText().toString();
        if (!mobileNumber.isEmpty()) {
            if (isValidMobileNumber) {
                showProgressBar(this);
                ForgotPasswordApiCall.serviceCallForForgotPassword(this, mobileNumber);
            } else {
                Toast.makeText(this, getString(R.string.please_enter_valid_phone_number), Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(this, getString(R.string.please_enter_phone_number), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void jsonResponseReceived(String jsonResponse, int statusCode, int requestType) {
        switch (requestType) {
            case Constants.SERVICE_CALL_FOR_FORGOT_PASSWORD:
                if (jsonResponse != null) {
                    ForgotPasswordResponse forgotPasswordResponse = new Gson().fromJson(jsonResponse, ForgotPasswordResponse.class);
                    if (forgotPasswordResponse != null) {
                        boolean status = forgotPasswordResponse.getStatus();
                        String message = forgotPasswordResponse.getMessage();
                        if (status) {
                            goToLogin();
                        }
                        Toast.makeText(this, Html.fromHtml(message), Toast.LENGTH_SHORT).show();
                    }
                }
                closeProgressbar();
                break;
            default:
                break;
        }
    }

    private void goToLogin() {
        Intent loginIntent = new Intent(this, LoginActivity.class);
        loginIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(loginIntent);
    }
}
