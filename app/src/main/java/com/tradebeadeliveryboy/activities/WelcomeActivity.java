package com.tradebeadeliveryboy.activities;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Build;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.tradebeadeliveryboy.R;

import java.security.MessageDigest;
import java.util.LinkedList;

public class WelcomeActivity extends BaseActivity implements View.OnClickListener {

    private ViewPager viewPager;
    private TextView tvSignUp, tvLogin;
    private LinearLayout llDots, llSignUp, llLoginWithPhone;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_welcome);
        setStatusBarColor();
        printHashKey();
        initializeUi();
        initializeListeners();
        prepareImagesList();
    }

    private void setStatusBarColor() {
        Window window = getWindow();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.setStatusBarColor(ContextCompat.getColor(this, R.color.white));
        }
    }

    public void printHashKey() {
        try {
            PackageInfo info = getPackageManager().getPackageInfo(getPackageName(), PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("KeyHash******", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }

    private void initializeUi() {
        llDots = findViewById(R.id.llDots);
        tvLogin = findViewById(R.id.tvLogin);
        llSignUp = findViewById(R.id.llSignUp);
        tvSignUp = findViewById(R.id.tvSignUp);
        viewPager = findViewById(R.id.viewPager);
        llLoginWithPhone = findViewById(R.id.llLoginWithPhone);
    }

    private void initializeListeners() {
        tvLogin.setOnClickListener(this);
        tvSignUp.setOnClickListener(this);
        llSignUp.setOnClickListener(this);
        llLoginWithPhone.setOnClickListener(this);
    }

    private void prepareImagesList() {
        LinkedList<Integer> listOfBannerImages = new LinkedList<>();
        listOfBannerImages.add(R.drawable.ic_splash_image_1);
        listOfBannerImages.add(R.drawable.ic_splash_image_2);
        listOfBannerImages.add(R.drawable.ic_splash_image_3);


        final WelcomeBannerImagesAdapter welcomeImagesAdapter = new WelcomeBannerImagesAdapter(this, listOfBannerImages);
        viewPager.setAdapter(welcomeImagesAdapter);
        for (int index = 0; index < welcomeImagesAdapter.getCount(); index++) {
            ImageButton ibDot = new ImageButton(this);
            ibDot.setTag(index);
            ibDot.setImageResource(R.drawable.dot_selector);
            ibDot.setBackgroundResource(0);
            ibDot.setPadding(0, 0, 5, 0);
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(60, 60);
            ibDot.setLayoutParams(params);
            if (index == 0)
                ibDot.setSelected(true);
            llDots.addView(ibDot);
        }

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                for (int index = 0; index < welcomeImagesAdapter.getCount(); index++) {
                    if (index != position) {
                        (llDots.findViewWithTag(index)).setSelected(false);
                    }
                }
                (llDots.findViewWithTag(position)).setSelected(true);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id) {
            case R.id.llSignUp:
            case R.id.tvSignUp:
                goToSignUp();
                break;
            case R.id.llLoginWithPhone:
            case R.id.tvLogin:
                goToLoginPage();
                break;
            default:
                break;
        }
    }

    private void goToLoginPage() {
        Intent loginIntent = new Intent(this, LoginActivity.class);
        loginIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(loginIntent);
    }

    private void goToSignUp() {
        Intent signUpIntent = new Intent(this, SignUpActivity.class);
        startActivity(signUpIntent);
    }


    private class WelcomeBannerImagesAdapter extends PagerAdapter {

        private Context context;
        private LinkedList<Integer> listOfBannerImages;

        public WelcomeBannerImagesAdapter(Context context, LinkedList<Integer> listOfBannerImages) {
            this.context = context;
            this.listOfBannerImages = listOfBannerImages;
        }

        @Override
        public int getCount() {
            return listOfBannerImages.size();
        }

        @Override
        public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
            return (view == object);
        }

        @NonNull
        @Override
        public Object instantiateItem(@NonNull ViewGroup container, int position) {
            LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View view = layoutInflater.inflate(R.layout.layout_welcome_banner_images_pager_item, container, false);
            int bannerImageUrl = listOfBannerImages.get(position);
            ImageView ivImage = view.findViewById(R.id.ivImage);
            ivImage.setImageResource(bannerImageUrl);
            container.addView(view);
            return view;
        }

        @Override
        public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
            //container.removeView( object);
        }
    }
}
