package com.tradebeadeliveryboy.activities;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.core.content.ContextCompat;

import com.google.gson.Gson;
import com.mukesh.OtpView;
import com.tradebeadeliveryboy.ApiCalls.GenerateOTPApiCall;
import com.tradebeadeliveryboy.ApiCalls.VerifyOtpApiCall;
import com.tradebeadeliveryboy.R;
import com.tradebeadeliveryboy.interfaces.HttpReqResCallBack;
import com.tradebeadeliveryboy.models.responseModels.generateOTPResponse.GenerateOTPResponse;
import com.tradebeadeliveryboy.models.responseModels.loginResponse.LoginData;
import com.tradebeadeliveryboy.models.responseModels.loginResponse.LoginResponse;
import com.tradebeadeliveryboy.utils.Constants;
import com.tradebeadeliveryboy.utils.PreferenceConnector;

import java.util.concurrent.TimeUnit;

import rx.Observable;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;

public class OTPVerificationActivity extends BaseActivity implements View.OnClickListener, HttpReqResCallBack {

    private OtpView otpView;
    private LinearLayout llSubmit;
    private ImageView ivBackArrow;
    private Subscription mSubscription;
    private TextView tvPhoneNumber, tvResend, tvSubmit;

    private String enteredOtp = "";
    private String otpReceived = "";
    private String mobileNumber = "";

    private boolean isResendClicked;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_otp_verification);
        setStatusBarColor();
        getDataFromIntent();
        initializeUi();
        initializeListeners();
        prepareDetails();
        setUpResendTimer(60);
    }

    private void setStatusBarColor() {
        Window window = getWindow();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.setStatusBarColor(ContextCompat.getColor(this, R.color.white));
        }
    }

    private void getDataFromIntent() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            if (bundle.containsKey(getString(R.string.mobile_number)))
                mobileNumber = bundle.getString(getString(R.string.mobile_number));
        }
    }

    private void initializeUi() {
        otpView = findViewById(R.id.otpView);
        llSubmit = findViewById(R.id.llSubmit);
        tvSubmit = findViewById(R.id.tvSubmit);
        tvResend = findViewById(R.id.tvResend);
        ivBackArrow = findViewById(R.id.ivBackArrow);
        tvPhoneNumber = findViewById(R.id.tvPhoneNumber);
    }

    private void initializeListeners() {
        llSubmit.setOnClickListener(this);
        tvSubmit.setOnClickListener(this);
        ivBackArrow.setOnClickListener(this);
    }

    private void prepareDetails() {
        tvPhoneNumber.setText(mobileNumber);
        showProgressBar(this);
        GenerateOTPApiCall.serviceCallToGenerateOTP(this, mobileNumber);
    }

    private void setUpResendTimer(int resendAfter) {
        checkAndUnsubscribeOnDemand();
        mSubscription = Observable.interval(1, TimeUnit.SECONDS)
                .take(resendAfter)
                .map(v -> resendAfter - (v + 1))
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(()
                        -> tvResend.setEnabled(false))
                .doOnTerminate(()
                        -> tvResend.setEnabled(true))
                .subscribe(new Subscriber<Long>() {

                    @Override
                    public void onCompleted() {
                        tvResend.setText(getString(R.string.resend_otp));
                        mSubscription = null;
                    }

                    @Override
                    public void onError(Throwable e) {
                        mSubscription = null;
                    }

                    @Override
                    public void onNext(Long aLong) {
                        tvResend.setText(String.format("Resend code in %1$s", aLong / 60 + ":" + aLong % 60));
                    }
                });
    }

    private void checkAndUnsubscribeOnDemand() {
        if (mSubscription != null && !mSubscription.isUnsubscribed()) {
            mSubscription.unsubscribe();
            mSubscription = null;
        }
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id) {
            case R.id.ivBackArrow:
                onBackPressed();
                break;
            case R.id.llSubmit:
            case R.id.tvSubmit:
                prepareSubmitDetails();
                break;
            case R.id.tvResend:
                prepareResendOTPDetails();
                break;
            default:
                break;
        }
    }

    private void prepareResendOTPDetails() {
        isResendClicked = true;
        showProgressBar(this);
        GenerateOTPApiCall.serviceCallToGenerateOTP(this, mobileNumber);
    }

    private void prepareSubmitDetails() {
        enteredOtp = otpView.getText().toString();
        if (enteredOtp.equals(otpReceived)) {
            showProgressBar(this);
            VerifyOtpApiCall.serviceCallForVerifyOTP(this, mobileNumber, enteredOtp);
        }
    }

    @Override
    public void jsonResponseReceived(String jsonResponse, int statusCode, int requestType) {
        switch (requestType) {
            case Constants.SERVICE_CALL_FOR_GENERATE_OTP:
                if (jsonResponse != null) {
                    GenerateOTPResponse generateOTPResponse = new Gson().fromJson(jsonResponse, GenerateOTPResponse.class);
                    if (generateOTPResponse != null) {
                        boolean status = generateOTPResponse.getStatus();
                        String message = generateOTPResponse.getMessage();
                        if (status) {
                            otpReceived = generateOTPResponse.getData();
                            Toast.makeText(this, otpReceived, Toast.LENGTH_LONG).show();
                            if (isResendClicked) {
                                setUpResendTimer(120);
                                isResendClicked = false;
                            }
                        } else {
                            Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
                        }
                    }
                }
                closeProgressbar();
                break;
            case Constants.SERVICE_CALL_FOR_VERIFY_OTP:
                if (jsonResponse != null) {
                    LoginResponse loginResponse = new Gson().fromJson(jsonResponse, LoginResponse.class);
                    if (loginResponse != null) {
                        boolean status = loginResponse.getStatus();
                        String message = loginResponse.getMessage();
                        if (status) {
                            LoginData loginData = loginResponse.getLoginData();
                            if (loginData != null) {
                                String token = loginData.getToken();
                                Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
                                PreferenceConnector.writeString(this, getString(R.string.user_token), token);
                                goToHome();
                            }
                        } else {
                            Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
                        }
                    }
                }
                closeProgressbar();
                break;
            default:
                break;
        }
    }

    private void goToHome() {
        Intent homeIntent = new Intent(this, HomeActivity.class);
        homeIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(homeIntent);
    }
}
