package com.tradebeadeliveryboy.activities;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.fragment.app.Fragment;

import com.tradebeadeliveryboy.R;
import com.tradebeadeliveryboy.interfaces.TransactionDetailsFilterCallBack;
import com.tradebeadeliveryboy.utils.UserData;

import java.util.Calendar;

public class TransactionDetailsFilterActivity extends BaseActivity implements View.OnClickListener {

    private DatePickerDialog datePickerDialog = null;
    private ImageView ivLastWeek, ivLastThirtyDays;
    private TextView tvStartDate, tvEndDate, tvApply;
    private boolean isLastWeekSelected = false;
    private boolean isLastThirtyDaysSelected = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_transaction_details_filter);
        initializeUI();
        initializeListeners();
        prepareDetails();
    }

    private void initializeUI() {
        ivLastWeek = findViewById(R.id.ivLastWeek);
        ivLastThirtyDays = findViewById(R.id.ivLastThirtyDays);

        tvStartDate = findViewById(R.id.tvStartDate);
        tvEndDate = findViewById(R.id.tvEndDate);
        tvApply = findViewById(R.id.tvApply);
    }

    private void initializeListeners() {
        tvApply.setOnClickListener(this);
        ivLastWeek.setOnClickListener(this);
        ivLastThirtyDays.setOnClickListener(this);
        tvStartDate.setOnClickListener(this);
        tvEndDate.setOnClickListener(this);
    }

    private void prepareDetails() {

    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        if (id == R.id.tvApply) {
            prepareApplyDetails();
        } else if (id == R.id.ivLastWeek) {
            ivLastWeek.setImageResource(R.drawable.ic_radio_button_checked);
            ivLastThirtyDays.setImageResource(R.drawable.ic_radio_button_unchecked);
            isLastWeekSelected = true;
            isLastThirtyDaysSelected = false;
        } else if (id == R.id.ivLastThirtyDays) {
            ivLastWeek.setImageResource(R.drawable.ic_radio_button_unchecked);
            ivLastThirtyDays.setImageResource(R.drawable.ic_radio_button_checked);
            isLastWeekSelected = false;
            isLastThirtyDaysSelected = true;
        } else if (id == R.id.tvStartDate) {
            prepareStartDateDetails();
        } else if (id == R.id.tvEndDate) {
            prepareEndDateDetails();
        }
    }

    private void prepareStartDateDetails() {
        showDatePicker(tvStartDate);
    }

    private void prepareEndDateDetails() {
        showDatePicker(tvEndDate);
    }

    private void showDatePicker(TextView textView) {
        Calendar calendar = Calendar.getInstance();
        Calendar date = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        datePickerDialog = new DatePickerDialog(this, R.style.DateAndTimePicker, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                date.set(year, monthOfYear, dayOfMonth);
                //String date = year + "-" + ((monthOfYear + 1) < 10 ? ("0" + (monthOfYear + 1)) : ((monthOfYear + 1))) + "-" + (dayOfMonth < 10 ? ("0" + dayOfMonth) : (dayOfMonth));
                //String date = (dayOfMonth < 10 ? ("0" + dayOfMonth) : (dayOfMonth)) + "-" + ((monthOfYear + 1) < 10 ? ("0" + (monthOfYear + 1)) : ((monthOfYear + 1))) + "-" + year;
                String date = year + "-" + ((monthOfYear + 1) < 10 ? ("0" + (monthOfYear + 1)) : ((monthOfYear + 1))) + "-" + (dayOfMonth < 10 ? ("0" + dayOfMonth) : (dayOfMonth));
                textView.setText(date);
            }
        }, year, month, day);
        datePickerDialog.show();
    }

    private void prepareApplyDetails() {
        String lastDays = "";
        String startDate = tvStartDate.getText().toString();
        String endDate = tvEndDate.getText().toString();
        if (isLastWeekSelected) {
            lastDays = "7";
        }
        if (isLastThirtyDaysSelected) {
            lastDays = "30";
        }

        Fragment fragment = UserData.getInstance().getWalletFragment();
        if (fragment != null) {
            TransactionDetailsFilterCallBack transactionDetailsFilterCallBack = (TransactionDetailsFilterCallBack) fragment;
            transactionDetailsFilterCallBack.transactionDetailsFilter(startDate, endDate, lastDays);
        }
        finish();
    }
}
