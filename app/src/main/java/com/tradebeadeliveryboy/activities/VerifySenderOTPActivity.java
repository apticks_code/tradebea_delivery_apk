package com.tradebeadeliveryboy.activities;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.mukesh.OtpView;
import com.tradebeadeliveryboy.ApiCalls.VerifyPaybackRequestApiCall;
import com.tradebeadeliveryboy.R;
import com.tradebeadeliveryboy.interfaces.CloseCallBack;
import com.tradebeadeliveryboy.interfaces.HttpReqResCallBack;
import com.tradebeadeliveryboy.models.responseModels.verifyPaybackResponse.VerifyPaybackResponse;
import com.tradebeadeliveryboy.utils.Constants;
import com.tradebeadeliveryboy.utils.PreferenceConnector;
import com.tradebeadeliveryboy.utils.UserData;

import java.util.Objects;

public class VerifySenderOTPActivity extends BaseActivity implements View.OnClickListener, HttpReqResCallBack {

    private OtpView otpView;
    private TextView tvSubmit;
    private ImageView ivBackArrow;
    private LinearLayout llSubmit;
    private String tradebeaOTP = "";
    private String superIntendentOTP = "";
    private String id = "";
    private String token = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_verify_sender_otp);
        getDataFromIntent();
        initializeUI();
        initializeListeners();
    }

    private void getDataFromIntent() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            if (bundle.containsKey(getString(R.string.id)))
                id = bundle.getString(getString(R.string.id), "");
            if (bundle.containsKey(getString(R.string.tradebea_otp)))
                tradebeaOTP = bundle.getString(getString(R.string.tradebea_otp), "");
            if (bundle.containsKey(getString(R.string.superintendent_otp)))
                superIntendentOTP = bundle.getString(getString(R.string.superintendent_otp), "");
        }
    }

    private void initializeUI() {
        otpView = findViewById(R.id.otpView);
        tvSubmit = findViewById(R.id.tvSubmit);
        llSubmit = findViewById(R.id.llSubmit);
        ivBackArrow = findViewById(R.id.ivBackArrow);
    }

    private void initializeListeners() {
        tvSubmit.setOnClickListener(this);
        llSubmit.setOnClickListener(this);
        ivBackArrow.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        if (id == R.id.ivBackArrow) {
            onBackPressed();
        } else if (id == R.id.llSubmit) {
            prepareSubmitDetails();
        } else if (id == R.id.tvSubmit) {
            prepareSubmitDetails();
        }
    }

    private void prepareSubmitDetails() {
        String enteredOTP = Objects.requireNonNull(otpView.getText()).toString();
        if (enteredOTP.equalsIgnoreCase(superIntendentOTP)) {
            showProgressBar(this);
            token = PreferenceConnector.readString(this, getString(R.string.user_token), "");
            VerifyPaybackRequestApiCall.serviceCallToVerifyPaybackRequest(this, null, null, token, id, superIntendentOTP);
        } else {
            Toast.makeText(this, getString(R.string.please_enter_valid_otp), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void jsonResponseReceived(String jsonResponse, int statusCode, int requestType) {
        if (requestType == Constants.SERVICE_CALL_FOR_VERIFY_PAYBACK_REQUEST) {
            if (jsonResponse != null) {
                VerifyPaybackResponse verifyPaybackResponse = new Gson().fromJson(jsonResponse, VerifyPaybackResponse.class);
                if (verifyPaybackResponse != null) {
                    boolean status = verifyPaybackResponse.getStatus();
                    if (status) {
                        Context context = UserData.getInstance().getSenderReceiverContext();
                        if (context != null) {
                            CloseCallBack closeCallBack = (CloseCallBack) context;
                            closeCallBack.close();
                        }
                        finish();
                    } else {
                        Toast.makeText(this, getString(R.string.status_error), Toast.LENGTH_SHORT).show();
                    }
                }
            }
            closeProgressbar();
        }
    }
}
