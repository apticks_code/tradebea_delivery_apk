package com.tradebeadeliveryboy.activities;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.razorpay.Checkout;
import com.razorpay.PaymentData;
import com.razorpay.PaymentResultWithDataListener;
import com.tradebeadeliveryboy.ApiCalls.GetRazorPayKeyDetailsApiCall;
import com.tradebeadeliveryboy.ApiCalls.GetWalletBalance;
import com.tradebeadeliveryboy.ApiCalls.PaybackOnlinePaymentApiCall;
import com.tradebeadeliveryboy.ApiCalls.PaymentMethodApiCall;
import com.tradebeadeliveryboy.R;
import com.tradebeadeliveryboy.adapters.CustomArrayAdapterForSpinner;
import com.tradebeadeliveryboy.fragments.WalletFragment;
import com.tradebeadeliveryboy.interfaces.HttpReqResCallBack;
import com.tradebeadeliveryboy.models.requestModels.paybackOnlinePaymentRequest.PaybackOnlinePaymentRequest;
import com.tradebeadeliveryboy.models.requestModels.paymentMethodRequest.PaymentMethodRequest;
import com.tradebeadeliveryboy.models.responseModels.dashboardDetailsResponse.DeliverySuperintendent;
import com.tradebeadeliveryboy.models.responseModels.dashboardDetailsResponse.TradebeaUser;
import com.tradebeadeliveryboy.models.responseModels.dashboardDetailsResponse.UserDetails;
import com.tradebeadeliveryboy.models.responseModels.paymentMethodResponse.PaymentMethodData;
import com.tradebeadeliveryboy.models.responseModels.paymentMethodResponse.PaymentMethodResponse;
import com.tradebeadeliveryboy.models.responseModels.paymentOnlineResponse.PaymentOnlineResponse;
import com.tradebeadeliveryboy.models.responseModels.razorPayKeyDetailsResponse.RazorPayData;
import com.tradebeadeliveryboy.models.responseModels.razorPayKeyDetailsResponse.RazorPayKeyDetailsResponse;
import com.tradebeadeliveryboy.models.responseModels.walletBalanceResponse.AccountDetails;
import com.tradebeadeliveryboy.models.responseModels.walletBalanceResponse.WalletBalanceResponse;
import com.tradebeadeliveryboy.models.responseModels.walletBalanceResponse.WalletData;
import com.tradebeadeliveryboy.utils.Constants;
import com.tradebeadeliveryboy.utils.PreferenceConnector;
import com.tradebeadeliveryboy.utils.UserData;

import org.json.JSONObject;

import java.util.LinkedList;

public class PaymentMethodActivity extends BaseActivity implements View.OnClickListener, HttpReqResCallBack, PaymentResultWithDataListener, TextWatcher {

    private EditText etAmount;
    private Spinner customSpinnerPaymentModes;
    private LinearLayout llAdmin, llSuperintendent;
    private ImageView ivBackArrow, ivAdmin, ivSuperintendent;
    private TextView tvFloatingCash, tvAdminName, tvSuperintendentName, tvSubmit;

    private LinkedList<String> listOfPaymentModes = new LinkedList<>();
    private boolean isAdminSelected = false;
    private boolean isSuperintendentSelected = false;
    private int superintendentUserId = -1;
    private int adminUserId = -1;

    private String selectedPaymentModeId = "";
    private String selectedUserId = "";
    private String token = "";
    private String razorPayKey = "";
    private String amount = "";
    private String adminFirstName = "";
    private String adminEmail = "";
    private Long adminMobileLong = 0L;
    private String superintendentFirstName = "";
    private String superintendentEmail = "";
    private Long superintendentMobileLong = 0L;
    private int paybackReqId = -1;

    private int walletAmount = 0;
    private int floatingWalletAmount = 0;
    private boolean isValidationFailed = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_payment_method);
        initializeUi();
        initializeListeners();
        prepareDetails();
        preparePaymentModeDetails();
        getWalletBalance();
    }

    private void initializeUi() {
        llAdmin = findViewById(R.id.llAdmin);
        ivAdmin = findViewById(R.id.ivAdmin);
        etAmount = findViewById(R.id.etAmount);
        tvSubmit = findViewById(R.id.tvSubmit);
        tvAdminName = findViewById(R.id.tvAdminName);
        ivBackArrow = findViewById(R.id.ivBackArrow);
        tvFloatingCash = findViewById(R.id.tvFloatingCash);
        ivSuperintendent = findViewById(R.id.ivSuperintendent);
        llSuperintendent = findViewById(R.id.llSuperintendent);
        tvSuperintendentName = findViewById(R.id.tvSuperintendentName);
        customSpinnerPaymentModes = findViewById(R.id.customSpinnerPaymentModes);
    }

    private void initializeListeners() {
        ivAdmin.setOnClickListener(this);
        tvSubmit.setOnClickListener(this);
        ivBackArrow.setOnClickListener(this);
        ivSuperintendent.setOnClickListener(this);

        etAmount.addTextChangedListener(this);
    }

    private void prepareDetails() {
        DeliverySuperintendent deliverySuperintendent = UserData.getInstance().getDeliverySuperintendent();
        TradebeaUser tradebeaUser = UserData.getInstance().getTradebeaUser();

        if (deliverySuperintendent != null) {
            llSuperintendent.setVisibility(View.VISIBLE);
            UserDetails superintendentUserDetails = deliverySuperintendent.getUserDetails();
            if (superintendentUserDetails != null) {
                superintendentFirstName = superintendentUserDetails.getFirstName();
                String lastName = superintendentUserDetails.getLastName();
                superintendentEmail = superintendentUserDetails.getEmail();
                superintendentMobileLong = superintendentUserDetails.getMobile();
                superintendentUserId = superintendentUserDetails.getUserId();
                tvSuperintendentName.setText(superintendentFirstName);
            }
        }
        if (tradebeaUser != null) {
            llAdmin.setVisibility(View.VISIBLE);
            UserDetails adminUserDetails = tradebeaUser.getUserDetails();
            if (adminUserDetails != null) {
                adminFirstName = adminUserDetails.getFirstName();
                String adminLastName = adminUserDetails.getLastName();
                adminEmail = adminUserDetails.getEmail();
                adminMobileLong = adminUserDetails.getMobile();
                adminUserId = adminUserDetails.getUserId();
                tvAdminName.setText(adminFirstName);
            }
        }
    }

    private void preparePaymentModeDetails() {
        listOfPaymentModes = new LinkedList<>();
        listOfPaymentModes.add(getString(R.string.select_mode));
        if (isAdminSelected)
            listOfPaymentModes.add(getString(R.string.online));
        listOfPaymentModes.add(getString(R.string.cash));
        CustomArrayAdapterForSpinner customArrayAdapterForSpinner = new CustomArrayAdapterForSpinner(this, R.layout.simple_spinner_dropdown_item, listOfPaymentModes);
        customSpinnerPaymentModes.setAdapter(customArrayAdapterForSpinner);
    }

    private void getWalletBalance() {
        showProgressBar(this);
        String token = PreferenceConnector.readString(this, getString(R.string.user_token), "");
        GetWalletBalance.serviceCallToGetWalletBalance(this, null, null, token);
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        if (id == R.id.ivBackArrow) {
            onBackPressed();
        } else if (id == R.id.ivAdmin) {
            isAdminSelected = true;
            isSuperintendentSelected = false;
            ivAdmin.setImageResource(R.drawable.ic_radio_button_checked);
            ivSuperintendent.setImageResource(R.drawable.ic_radio_button_unchecked);
            preparePaymentModeDetails();
        } else if (id == R.id.ivSuperintendent) {
            isAdminSelected = false;
            isSuperintendentSelected = true;
            ivSuperintendent.setImageResource(R.drawable.ic_radio_button_checked);
            ivAdmin.setImageResource(R.drawable.ic_radio_button_unchecked);
            preparePaymentModeDetails();
        } else if (id == R.id.tvSubmit) {
            if (!isValidationFailed) {
                prepareSubmitDetails();
            } else {
                Toast.makeText(this, "Please Enter amount that must be less than floating amount", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void prepareSubmitDetails() {
        amount = etAmount.getText().toString();
        String selectedPaymentMode = customSpinnerPaymentModes.getSelectedItem().toString();
        if (selectedPaymentMode.equalsIgnoreCase(getString(R.string.online))) {
            selectedPaymentModeId = "2";
        } else if (selectedPaymentMode.equalsIgnoreCase(getString(R.string.cash))) {
            selectedPaymentModeId = "1";
        }

        if (isAdminSelected) {
            selectedUserId = String.valueOf(adminUserId);
        } else if (isSuperintendentSelected) {
            selectedUserId = String.valueOf(superintendentUserId);
        }

        if (!amount.isEmpty()) {
            if (!selectedPaymentModeId.isEmpty()) {
                if (!selectedUserId.isEmpty()) {
                    showProgressBar(this);
                    token = PreferenceConnector.readString(this, getString(R.string.user_token), "");
                    PaymentMethodRequest paymentMethodRequest = new PaymentMethodRequest();
                    paymentMethodRequest.amount = amount;
                    paymentMethodRequest.source = selectedPaymentModeId;
                    paymentMethodRequest.receiverUserId = selectedUserId;

                    PaymentMethodApiCall.serviceCallToPaymentMethod(this, null, null, paymentMethodRequest, token);
                } else {
                    Toast.makeText(this, "Please select user", Toast.LENGTH_SHORT).show();
                }
            } else {
                Toast.makeText(this, "Please select payment mode", Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(this, "Please enter amount", Toast.LENGTH_SHORT).show();
        }
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void jsonResponseReceived(String jsonResponse, int statusCode, int requestType) {
        if (requestType == Constants.SERVICE_CALL_FOR_PAYMENT_METHOD_REQUEST) {
            closeProgressbar();
            if (jsonResponse != null) {
                PaymentMethodResponse paymentMethodResponse = new Gson().fromJson(jsonResponse, PaymentMethodResponse.class);
                if (paymentMethodResponse != null) {
                    boolean status = paymentMethodResponse.getStatus();
                    if (status) {
                        PaymentMethodData paymentMethodData = paymentMethodResponse.getPaymentMethodData();
                        if (paymentMethodData != null) {
                            paybackReqId = paymentMethodData.getId();
                            UserData.getInstance().setPaybackReqId(paybackReqId);
                            if (selectedPaymentModeId.equalsIgnoreCase("2")) {
                                showProgressBar(this);
                                token = PreferenceConnector.readString(this, getString(R.string.user_token), "");
                                GetRazorPayKeyDetailsApiCall.serviceCallToGetRazorPayKeyDetails(this, null, null, token);
                            } else {
                                PreferenceConnector.writeBoolean(this, getString(R.string.refresh_pay_back), true);
                                finish();
                            }
                        } else {
                            Toast.makeText(this, getString(R.string.status_error), Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(this, getString(R.string.status_error), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(this, getString(R.string.status_error), Toast.LENGTH_SHORT).show();
                }
            } else {
                Toast.makeText(this, getString(R.string.status_error), Toast.LENGTH_SHORT).show();
            }
        } else if (requestType == Constants.SERVICE_CALL_TO_GET_RAZOR_PAY_KEY_DETAILS) {
            if (jsonResponse != null) {
                RazorPayKeyDetailsResponse razorPayKeyDetailsResponse = new Gson().fromJson(jsonResponse, RazorPayKeyDetailsResponse.class);
                if (razorPayKeyDetailsResponse != null) {
                    boolean status = razorPayKeyDetailsResponse.getStatus();
                    String message = razorPayKeyDetailsResponse.getMessage();
                    if (status) {
                        RazorPayData razorPayData = razorPayKeyDetailsResponse.getRazorPayData();
                        if (razorPayData != null) {
                            razorPayKey = razorPayData.getKeyId();
                            startPayment();
                        }
                    } else {
                        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
                    }
                }
            }
            closeProgressbar();
        } else if (requestType == Constants.SERVICE_CALL_FOR_PAYMENT_ONLINE_PAYMENT_REQUEST) {
            if (jsonResponse != null) {
                PaymentOnlineResponse paymentOnlineResponse = new Gson().fromJson(jsonResponse, PaymentOnlineResponse.class);
                if (paymentOnlineResponse != null) {
                    boolean status = paymentOnlineResponse.getStatus();
                    if (status) {
                        Toast.makeText(this, "Success", Toast.LENGTH_SHORT).show();
                        PreferenceConnector.writeBoolean(this, getString(R.string.refresh_pay_back), true);
                        finish();
                    } else {
                        Toast.makeText(this, getString(R.string.status_error), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(this, getString(R.string.status_error), Toast.LENGTH_SHORT).show();
                }
            } else {
                Toast.makeText(this, getString(R.string.status_error), Toast.LENGTH_SHORT).show();
            }
            closeProgressbar();
        } else if (requestType == Constants.SERVICE_CALL_TO_GET_WALLET_BALANCE) {
            if (jsonResponse != null) {
                WalletBalanceResponse walletBalanceResponse = new Gson().fromJson(jsonResponse, WalletBalanceResponse.class);
                if (walletBalanceResponse != null) {
                    boolean status = walletBalanceResponse.getStatus();
                    if (status) {
                        WalletData walletData = walletBalanceResponse.getWalletData();
                        if (walletData != null) {
                            AccountDetails accountDetails = walletData.getAccountDetails();
                            if (accountDetails != null) {
                                walletAmount = accountDetails.getWallet();
                                floatingWalletAmount = accountDetails.getFloatingWallet();
                            }
                        }
                    }
                }
            }
            tvFloatingCash.setText(getString(R.string.floating_cash) + " : " + getString(R.string.current_currency) + "" + floatingWalletAmount);
            closeProgressbar();
        }
    }

    private void startPayment() {
        double amountDouble = Double.parseDouble(amount);
        final Activity activity = this;
        Checkout checkout = new Checkout();
        checkout.setKeyID(razorPayKey);
        checkout.setImage(R.mipmap.ic_launcher);
        try {
            JSONObject options = new JSONObject();
            if (isAdminSelected) {
                options.put("name", adminFirstName);
            } else if (isSuperintendentSelected) {
                options.put("name", superintendentFirstName);
            }

            options.put("description", "Product Checkout");
            options.put("image", "https://s3.amazonaws.com/rzp-mobile/images/rzp.png");
            options.put("theme.color", "#3F51B5");
            options.put("currency", "INR");
            if (amountDouble != 0) {
                double remainingAmount = amountDouble * 100;
                options.put("amount", remainingAmount);//pass amount in currency subunits
            }
            JSONObject preFill = new JSONObject();
            if (isAdminSelected) {
                preFill.put("email", adminEmail);
                preFill.put("contact", String.valueOf(adminMobileLong));
            } else if (isSuperintendentSelected) {
                preFill.put("email", superintendentEmail);
                preFill.put("contact", String.valueOf(superintendentMobileLong));
            }
            options.put("prefill", preFill);
            checkout.open(activity, options);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onPaymentSuccess(String razorPayPaymentId, PaymentData paymentData) {
        Toast.makeText(this, getString(R.string.payment_successfull), Toast.LENGTH_SHORT).show();
        preparePaynowDetails(razorPayPaymentId, true);
    }

    @Override
    public void onPaymentError(int statusCode, String errorMessage, PaymentData paymentData) {
        Toast.makeText(this, getString(R.string.payment_failed), Toast.LENGTH_SHORT).show();
        preparePaynowDetails(paymentData.getPaymentId(), false);
    }

    private void preparePaynowDetails(String razorPayPaymentId, boolean status) {
        showProgressBar(this);
        PaybackOnlinePaymentRequest paybackOnlinePaymentRequest = new PaybackOnlinePaymentRequest();
        paybackOnlinePaymentRequest.txnId = razorPayPaymentId;
        paybackOnlinePaymentRequest.amount = amount;
        paybackOnlinePaymentRequest.paybackReqId = String.valueOf(paybackReqId);
        paybackOnlinePaymentRequest.status = status;
        PaybackOnlinePaymentApiCall.serviceCallToPaybackOnlinePayment(this, null, null, paybackOnlinePaymentRequest, token);
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int start, int before, int count) {
        String enteredText = charSequence.toString();
        if (!enteredText.isEmpty()) {
            int enteredAmountInt = Integer.parseInt(enteredText);
            if (enteredAmountInt <= floatingWalletAmount) {
                isValidationFailed = false;
            } else {
                isValidationFailed = true;
            }
        }
    }

    @Override
    public void afterTextChanged(Editable s) {

    }
}
