package com.tradebeadeliveryboy.activities;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.mukesh.OtpView;
import com.tradebeadeliveryboy.ApiCalls.VerifyPickUpOTPApiCall;
import com.tradebeadeliveryboy.R;
import com.tradebeadeliveryboy.interfaces.CloseCallBack;
import com.tradebeadeliveryboy.interfaces.HttpReqResCallBack;
import com.tradebeadeliveryboy.models.responseModels.verifyPickUpOTPResponse.VerifyPickUpOTPResponse;
import com.tradebeadeliveryboy.utils.Constants;
import com.tradebeadeliveryboy.utils.UserData;

public class PickedUpOTPActivity extends BaseActivity implements View.OnClickListener, HttpReqResCallBack {

    private OtpView otpView;
    private TextView tvSubmit;
    private ImageView ivBackArrow;
    private LinearLayout llSubmit;

    private String selectedOrderId = "";

    private int deliveryJobId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_picked_up_otp);
        getDataFromIntent();
        initializeUi();
        initializeListeners();
    }

    private void getDataFromIntent() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            if (bundle.containsKey(getString(R.string.selected_order_id))) {
                selectedOrderId = bundle.getString(getString(R.string.selected_order_id));
            }
            if (bundle.containsKey(getString(R.string.delivery_job_id))) {
                deliveryJobId = bundle.getInt(getString(R.string.delivery_job_id));
            }
        }
    }

    private void initializeUi() {
        otpView = findViewById(R.id.otpView);
        llSubmit = findViewById(R.id.llSubmit);
        tvSubmit = findViewById(R.id.tvSubmit);
        ivBackArrow = findViewById(R.id.ivBackArrow);
    }

    private void initializeListeners() {
        llSubmit.setOnClickListener(this);
        tvSubmit.setOnClickListener(this);
        ivBackArrow.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        if (id == R.id.ivBackArrow) {
            onBackPressed();
        } else if (id == R.id.llSubmit) {
            prepareDetails();
        } else if (id == R.id.tvSubmit) {
            prepareDetails();
        }
    }

    private void prepareDetails() {
        String otp = otpView.getText().toString();
        if (!otp.isEmpty()) {
            showProgressBar(this);
            VerifyPickUpOTPApiCall.serviceCallForVerifyPickUpOTP(this, null, null, selectedOrderId, otp, deliveryJobId);
        } else {
            Toast.makeText(this, getString(R.string.please_enter_otp), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void jsonResponseReceived(String jsonResponse, int statusCode, int requestType) {
        if (requestType == Constants.SERVICE_CALL_FOR_VERIFY_PICK_UP_OTP) {
            if (jsonResponse != null) {
                VerifyPickUpOTPResponse verifyPickUpOTPResponse = new Gson().fromJson(jsonResponse, VerifyPickUpOTPResponse.class);
                if (verifyPickUpOTPResponse != null) {
                    Boolean status = verifyPickUpOTPResponse.getStatus();
                    String message = verifyPickUpOTPResponse.getMessage();
                    if (status) {
                        Context context = UserData.getInstance().getOrderDetailsContext();
                        if (context != null) {
                            CloseCallBack closeCallBack = (CloseCallBack) context;
                            closeCallBack.close();
                        }
                        finish();
                    } else {
                        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
                    }
                }
            }
            closeProgressbar();
        }
    }
}
