package com.tradebeadeliveryboy.activities;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.tradebeadeliveryboy.ApiCalls.OrderDetailsApiCall;
import com.tradebeadeliveryboy.ApiCalls.ReachedToDeliveryApiCall;
import com.tradebeadeliveryboy.ApiCalls.ReachedToPickUpPointApiCall;
import com.tradebeadeliveryboy.R;
import com.tradebeadeliveryboy.adapters.OrderedProductsAdapter;
import com.tradebeadeliveryboy.interfaces.CloseCallBack;
import com.tradebeadeliveryboy.interfaces.HttpReqResCallBack;
import com.tradebeadeliveryboy.interfaces.LocationUpdateCallBack;
import com.tradebeadeliveryboy.models.responseModels.orderDetailsResponse.AddressInfo;
import com.tradebeadeliveryboy.models.responseModels.orderDetailsResponse.Customer;
import com.tradebeadeliveryboy.models.responseModels.orderDetailsResponse.CustomerAppStatus;
import com.tradebeadeliveryboy.models.responseModels.orderDetailsResponse.CustomerDetails;
import com.tradebeadeliveryboy.models.responseModels.orderDetailsResponse.DeliveryJobs;
import com.tradebeadeliveryboy.models.responseModels.orderDetailsResponse.Details;
import com.tradebeadeliveryboy.models.responseModels.orderDetailsResponse.Location;
import com.tradebeadeliveryboy.models.responseModels.orderDetailsResponse.OrderDetail;
import com.tradebeadeliveryboy.models.responseModels.orderDetailsResponse.OrderDetailsData;
import com.tradebeadeliveryboy.models.responseModels.orderDetailsResponse.OrderDetailsResponse;
import com.tradebeadeliveryboy.models.responseModels.orderDetailsResponse.ReturnRequest;
import com.tradebeadeliveryboy.models.responseModels.orderDetailsResponse.Seller;
import com.tradebeadeliveryboy.models.responseModels.reachedToDeliveryResponse.ReachedToDeliveryResponse;
import com.tradebeadeliveryboy.models.responseModels.reachedToPickUpPointResponse.ReachedToPickUpPointResponse;
import com.tradebeadeliveryboy.utils.AppPermissions;
import com.tradebeadeliveryboy.utils.Constants;
import com.tradebeadeliveryboy.utils.GetCurrentLocation;
import com.tradebeadeliveryboy.utils.PreferenceConnector;
import com.tradebeadeliveryboy.utils.UserData;

import java.util.LinkedList;

public class OrderDetailsActivity extends BaseActivity implements View.OnClickListener, HttpReqResCallBack, LocationUpdateCallBack, CloseCallBack {

    private ImageView ivBackArrow;
    private RecyclerView rvOrderedProducts;
    private ImageView ivSellerDistance, ivDestinationDistance;
    private LinearLayout llSellerDistance, llDestinationDistance, llStatues;
    private TextView tvName, tvPhoneNumber, tvAddress, tvDestinationDistance;
    private TextView tvOrderId, tvOrderDateTime, tvOrderStatus, tvVendorOrderStatus;
    private TextView tvShopName, tvShopPhoneNumber, tvShopAddress, tvSellerDistance, tvPickedUp;
    private TextView tvSubTotal, tvDiscountOnMRP, tvGST, tvDeliveryBoyFee, tvTax, tvFinalPrice;

    private LinearLayout llReturn;
    private TextView tvReturn, tvReturned;

    private TextView tvReachedToPickUpPoint, tvReachedToDelivery, tvPickUp, tvDelivery, tvJobId;

    private ReturnRequest returnRequest;

    private LinkedList<OrderDetail> listOfOrderDetails;
    private LinkedList<DeliveryJobs> listOfDeliveryJobs;
    private LinkedList<CustomerAppStatus> listOfCustomerAppStatuses;

    private String sellerAddress = "";
    private String selectedOrderId = "";

    private String customerAddress = "";
    private String customerLandMark = "";
    private String customerGeoLocationAddress = "";

    private String shopName = "";
    private Long shopMobileNumber = 0L;

    private Double sellerLatitude = 0.0;
    private Double sellerLongitude = 0.0;

    private Double customerLatitude = 0.0;
    private Double customerLongitude = 0.0;

    private double latitude = 0.0;
    private double longitude = 0.0;


    private float subTotal = 0;
    private float totalPrice = 0;
    private float tax = 0;
    private float discount = 0;
    private float deliveryFee = 0;
    private float promoDiscount = 0;
    private float totalDiscount = 0;
    private int paymentMethodId = 0;

    private String jobId = "";
    private int returnDeliveryJobId;
    private int returnRequestId;
    private Float usedWalletAmount = 0f;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_order_details);
        getDataFromIntent();
        initializeUi();
        initializeListeners();
        checkLocationAccessPermission();
        prepareDetails();
    }

    private void getDataFromIntent() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            if (bundle.containsKey(getString(R.string.order_id)))
                selectedOrderId = bundle.getString(getString(R.string.order_id), "");
            if (bundle.containsKey(getString(R.string.job_id)))
                jobId = bundle.getString(getString(R.string.job_id));
        }
    }

    private void initializeUi() {
        tvJobId = findViewById(R.id.tvJobId);
        llStatues = findViewById(R.id.llStatues);
        ivBackArrow = findViewById(R.id.ivBackArrow);
        rvOrderedProducts = findViewById(R.id.rvOrderedProducts);

        tvName = findViewById(R.id.tvName);
        tvAddress = findViewById(R.id.tvAddress);
        tvPickedUp = findViewById(R.id.tvPickedUp);
        tvPhoneNumber = findViewById(R.id.tvPhoneNumber);
        tvDestinationDistance = findViewById(R.id.tvDestinationDistance);

        tvOrderId = findViewById(R.id.tvOrderId);
        tvOrderStatus = findViewById(R.id.tvOrderStatus);
        tvOrderDateTime = findViewById(R.id.tvOrderDateTime);
        tvVendorOrderStatus = findViewById(R.id.tvVendorOrderStatus);

        tvShopName = findViewById(R.id.tvShopName);
        tvShopAddress = findViewById(R.id.tvShopAddress);
        tvSellerDistance = findViewById(R.id.tvSellerDistance);
        tvShopPhoneNumber = findViewById(R.id.tvShopPhoneNumber);

        tvGST = findViewById(R.id.tvGST);
        tvTax = findViewById(R.id.tvTax);
        tvSubTotal = findViewById(R.id.tvSubTotal);
        tvFinalPrice = findViewById(R.id.tvFinalPrice);
        tvDiscountOnMRP = findViewById(R.id.tvDiscountOnMRP);
        tvDeliveryBoyFee = findViewById(R.id.tvDeliveryBoyFee);

        ivSellerDistance = findViewById(R.id.ivSellerDistance);
        ivDestinationDistance = findViewById(R.id.ivDestinationDistance);

        llSellerDistance = findViewById(R.id.llSellerDistance);
        llDestinationDistance = findViewById(R.id.llDestinationDistance);

        tvPickUp = findViewById(R.id.tvPickUp);
        tvDelivery = findViewById(R.id.tvDelivery);
        tvReachedToDelivery = findViewById(R.id.tvReachedToDelivery);
        tvReachedToPickUpPoint = findViewById(R.id.tvReachedToPickUpPoint);

        llReturn = findViewById(R.id.llReturn);
        tvReturn = findViewById(R.id.tvReturn);
        tvReturned = findViewById(R.id.tvReturned);
    }

    private void initializeListeners() {
        ivBackArrow.setOnClickListener(this);

        tvSellerDistance.setOnClickListener(this);
        tvDestinationDistance.setOnClickListener(this);

        ivSellerDistance.setOnClickListener(this);
        ivDestinationDistance.setOnClickListener(this);

        llSellerDistance.setOnClickListener(this);
        llDestinationDistance.setOnClickListener(this);

        tvPickUp.setOnClickListener(this);
        tvDelivery.setOnClickListener(this);
        tvPickedUp.setOnClickListener(this);
        tvReachedToDelivery.setOnClickListener(this);
        tvReachedToPickUpPoint.setOnClickListener(this);

        tvReturn.setOnClickListener(this);
        tvReturned.setOnClickListener(this);
    }

    private void checkLocationAccessPermission() {
        if (AppPermissions.checkPermissionForAccessLocation(this)) {
            LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
            new GetCurrentLocation(this, null, locationManager);
        } else {
            AppPermissions.requestPermissionForLocation(this);
        }
    }

    private void prepareDetails() {
        tvJobId.setText(jobId);
        showProgressBar(this);
        String token = PreferenceConnector.readString(this, getString(R.string.user_token), "");
        OrderDetailsApiCall.serviceCallToGetOrderDetails(this, null, null, selectedOrderId, token);
    }

    @Override
    protected void onResume() {
        super.onResume();
        UserData.getInstance().setOrderDetailsContext(this);
    }

    @SuppressLint("NonConstantResourceId")
    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id) {
            case R.id.ivBackArrow:
                onBackPressed();
                break;
            case R.id.tvSellerDistance:
                prepareSellerGoogleMaps();
                break;
            case R.id.ivSellerDistance:
                prepareSellerGoogleMaps();
                break;
            case R.id.llSellerDistance:
                prepareSellerGoogleMaps();
                break;
            case R.id.tvDestinationDistance:
                prepareDestinationGoogleMaps();
                break;
            case R.id.ivDestinationDistance:
                prepareDestinationGoogleMaps();
                break;
            case R.id.llDestinationDistance:
                prepareDestinationGoogleMaps();
                break;
            case R.id.tvPickUp:
                preparePickUpDetails();
                break;
            case R.id.tvDelivery:
                prepareDeliveryDetails();
                break;
            case R.id.tvReachedToDelivery:
                prepareReachedToDeliveryDetails();
                break;
            case R.id.tvReachedToPickUpPoint:
                prepareReachedToPickUpPointDetails();
                break;
            case R.id.tvPickedUp:
                preparePickedUpDetails();
                break;
            case R.id.tvReturn:
                prepareReturnDetails();
                break;
            case R.id.tvReturned:
                prepareReturnedDetails();
                break;
            default:
                break;
        }
    }

    private void preparePickedUpDetails() {
        if (listOfDeliveryJobs != null) {
            if (listOfDeliveryJobs.size() != 0) {
                DeliveryJobs deliveryJobs = listOfDeliveryJobs.get(0);
                Intent verifyDeliveryBoyOTPIntent = new Intent(this, PickedUpOTPActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString(getString(R.string.selected_order_id), selectedOrderId);
                bundle.putInt(getString(R.string.delivery_job_id), deliveryJobs.getId());
                verifyDeliveryBoyOTPIntent.putExtras(bundle);
                startActivity(verifyDeliveryBoyOTPIntent);
            }
        }
    }

    private void preparePickUpDetails() {
        if (listOfDeliveryJobs != null) {
            if (listOfDeliveryJobs.size() != 0) {
                DeliveryJobs deliveryJobs = listOfDeliveryJobs.get(0);
                Intent pickUpOtpIntent = new Intent(this, PickUpOTPActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString(getString(R.string.order_id), selectedOrderId);
                bundle.putInt(getString(R.string.payment_method_id), paymentMethodId);
                bundle.putInt(getString(R.string.delivery_job_id), deliveryJobs.getId());
                pickUpOtpIntent.putExtras(bundle);
                startActivity(pickUpOtpIntent);
            }
        }
    }

    private void prepareDeliveryDetails() {
        if (listOfDeliveryJobs != null) {
            if (listOfDeliveryJobs.size() != 0) {
                DeliveryJobs deliveryJobs = listOfDeliveryJobs.get(0);
                Intent deliveryOtpIntent = new Intent(this, DeliveryOTPActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString(getString(R.string.order_id), selectedOrderId);
                bundle.putInt(getString(R.string.payment_method_id), paymentMethodId);
                bundle.putFloat(getString(R.string.total_price), totalPrice);
                bundle.putFloat(getString(R.string.used_wallet_amount), usedWalletAmount);
                bundle.putInt(getString(R.string.delivery_job_id), deliveryJobs.getId());
                deliveryOtpIntent.putExtras(bundle);
                startActivity(deliveryOtpIntent);
            }
        }
    }

    private void prepareReachedToDeliveryDetails() {
        if (listOfDeliveryJobs != null) {
            if (listOfDeliveryJobs.size() != 0) {
                DeliveryJobs deliveryJobs = listOfDeliveryJobs.get(0);
                showProgressBar(this);
                String token = PreferenceConnector.readString(this, getString(R.string.user_token), "");
                ReachedToDeliveryApiCall.serviceCallForReachedToDelivery(this, null, null, selectedOrderId, deliveryJobs.getId(), token);
            } else {
                Toast.makeText(this, getString(R.string.status_error), Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(this, getString(R.string.status_error), Toast.LENGTH_SHORT).show();
        }
    }

    private void prepareReachedToPickUpPointDetails() {
        if (listOfDeliveryJobs != null) {
            if (listOfDeliveryJobs.size() != 0) {
                DeliveryJobs deliveryJobs = listOfDeliveryJobs.get(0);
                showProgressBar(this);
                ReachedToPickUpPointApiCall.serviceCallForReachedToPickUpPoint(this, null, null, selectedOrderId, deliveryJobs.getId());
            } else {
                Toast.makeText(this, getString(R.string.status_error), Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(this, getString(R.string.status_error), Toast.LENGTH_SHORT).show();
        }
    }

    private void prepareSellerGoogleMaps() {
        Intent sellerGoogleMapIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://maps.google.com/maps?saddr="
                + latitude + "," + longitude + "&" + "daddr="
                + sellerLatitude + "," + sellerLongitude));
        sellerGoogleMapIntent.setClassName("com.google.android.apps.maps", "com.google.android.maps.MapsActivity");
        startActivity(sellerGoogleMapIntent);
    }

    private void prepareDestinationGoogleMaps() {
        Intent destinationGoogleMapIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://maps.google.com/maps?saddr="
                + latitude + "," + longitude + "&" + "daddr="
                + customerLatitude + "," + customerLongitude));
        destinationGoogleMapIntent.setClassName("com.google.android.apps.maps", "com.google.android.maps.MapsActivity");
        startActivity(destinationGoogleMapIntent);
    }

    @Override
    public void jsonResponseReceived(String jsonResponse, int statusCode, int requestType) {
        switch (requestType) {
            case Constants.SERVICE_CALL_TO_GET_ORDER_DETAILS:
                if (jsonResponse != null) {
                    OrderDetailsResponse orderDetailsResponse = new Gson().fromJson(jsonResponse, OrderDetailsResponse.class);
                    if (orderDetailsResponse != null) {
                        boolean status = orderDetailsResponse.getStatus();
                        String message = orderDetailsResponse.getMessage();
                        if (status) {
                            OrderDetailsData orderDetailsData = orderDetailsResponse.getData();
                            if (orderDetailsData != null) {
                                paymentMethodId = orderDetailsData.getPaymentMethodId();
                                if (orderDetailsData.getUsedWalletAmount() != null)
                                    usedWalletAmount = orderDetailsData.getUsedWalletAmount();
                                Seller seller = orderDetailsData.getSeller();
                                Customer customer = orderDetailsData.getCustomer();
                                listOfOrderDetails = orderDetailsData.getListOfOrderDetails();
                                listOfCustomerAppStatuses = orderDetailsData.getListOfCustomerAppStatuses();
                                listOfDeliveryJobs = orderDetailsData.getListOfDeliveryJobs();
                                returnRequest = orderDetailsData.getReturnRequest();
                                getJobId();
                                prepareStatusDetails();
                                prepareHeaderDetails(orderDetailsData);
                                prepareSellerDetails(seller);
                                prepareCustomerDetails(customer);
                                prepareOrderDetails();
                                preparePriceDetails();
                                prepareButtonDetails();
                                prepareReturnOrderDetails();
                            }
                        } else {
                            Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
                        }
                    }
                }
                closeProgressbar();
                break;
            case Constants.SERVICE_CALL_FOR_REACHED_TO_PICKUP_POINT:
                if (jsonResponse != null) {
                    ReachedToPickUpPointResponse reachedToPickUpPointResponse = new Gson().fromJson(jsonResponse, ReachedToPickUpPointResponse.class);
                    if (reachedToPickUpPointResponse != null) {
                        boolean status = reachedToPickUpPointResponse.getStatus();
                        String message = reachedToPickUpPointResponse.getMessage();
                        if (status) {
                            prepareDetails();
                        } else {
                            Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
                        }
                    }
                }
                closeProgressbar();
                break;
            case Constants.SERVICE_CALL_FOR_REACHED_TO_DELIVERY:
                if (jsonResponse != null) {
                    ReachedToDeliveryResponse reachedToDeliveryResponse = new Gson().fromJson(jsonResponse, ReachedToDeliveryResponse.class);
                    if (reachedToDeliveryResponse != null) {
                        Boolean status = reachedToDeliveryResponse.getStatus();
                        String message = reachedToDeliveryResponse.getMessage();
                        if (status) {
                            prepareDetails();
                        } else {
                            Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
                        }
                    }
                }
                closeProgressbar();
                break;
            default:
                break;
        }
    }

    private void getJobId() {
        if (listOfDeliveryJobs != null) {
            if (listOfDeliveryJobs.size() != 0) {
                DeliveryJobs deliveryJobs = listOfDeliveryJobs.get(0);
                if (deliveryJobs != null) {
                    jobId = deliveryJobs.getJobId();
                    tvJobId.setText(jobId);
                }
            }
        }
    }

    private void prepareButtonDetails() {
        if (listOfCustomerAppStatuses != null) {
            if (listOfCustomerAppStatuses.size() != 0) {
                for (int index = 0; index < listOfCustomerAppStatuses.size(); index++) {
                    CustomerAppStatus customerAppStatus = listOfCustomerAppStatuses.get(index);
                    int customerStatusId = customerAppStatus.getId();
                    String name = customerAppStatus.getName();
                    String createdAt = customerAppStatus.getCreatedAt();

                    if (customerStatusId == 502) {
                        if (createdAt != null) {
                            tvReachedToPickUpPoint.setVisibility(View.VISIBLE);
                            tvPickedUp.setVisibility(View.GONE);
                            tvReachedToDelivery.setVisibility(View.GONE);
                            tvDelivery.setVisibility(View.GONE);
                        }
                    } else if (customerStatusId == 504) {
                        if (createdAt != null) {
                            tvReachedToPickUpPoint.setVisibility(View.GONE);
                            tvPickedUp.setVisibility(View.VISIBLE);
                            tvReachedToDelivery.setVisibility(View.GONE);
                            tvDelivery.setVisibility(View.GONE);
                        }
                    } else if (customerStatusId == 505) {
                        if (createdAt != null) {
                            tvReachedToPickUpPoint.setVisibility(View.GONE);
                            tvPickedUp.setVisibility(View.GONE);
                            tvReachedToDelivery.setVisibility(View.VISIBLE);
                            tvDelivery.setVisibility(View.GONE);
                        }
                    } else if (customerStatusId == 506) {
                        if (createdAt != null) {
                            tvReachedToPickUpPoint.setVisibility(View.GONE);
                            tvPickedUp.setVisibility(View.GONE);
                            tvReachedToDelivery.setVisibility(View.GONE);
                            tvDelivery.setVisibility(View.VISIBLE);
                        }
                    } else if (customerStatusId == 601) {
                        if (createdAt != null) {
                            tvReturn.setVisibility(View.VISIBLE);
                            tvReturned.setVisibility(View.GONE);
                        }
                    } else if (customerStatusId == 602) {
                        if (createdAt != null) {
                            tvReturn.setVisibility(View.GONE);
                            tvReturned.setVisibility(View.VISIBLE);
                        }
                    }
                }
            }
        }
    }

    private void prepareStatusDetails() {
        if (listOfCustomerAppStatuses != null) {
            if (listOfCustomerAppStatuses.size() != 0) {
                for (int index = 0; index < listOfCustomerAppStatuses.size(); index++) {
                    CustomerAppStatus customerAppStatus = listOfCustomerAppStatuses.get(index);
                    String name = customerAppStatus.getName();
                    Integer id = customerAppStatus.getId();
                    String createdAt = customerAppStatus.getCreatedAt();
                    if (createdAt != null) {
                        if (id == 508) {
                            llStatues.setVisibility(View.GONE);
                        }
                        tvOrderStatus.setText(name);
                    }
                }
            }
        }
    }

    private void prepareHeaderDetails(OrderDetailsData orderDetailsData) {
        String trackId = orderDetailsData.getTrackId();
        String createdAt = orderDetailsData.getCreatedAt();

        tvOrderId.setText(trackId);
        tvOrderDateTime.setText(createdAt);
    }

    private double calculateDistanceBetweenTwoLatLngs(Double endLatitude, Double endLongitude) {
        android.location.Location currentLocation = new android.location.Location("locationA");
        currentLocation.setLatitude(latitude);
        currentLocation.setLongitude(longitude);
        android.location.Location destination = new android.location.Location("locationB");
        destination.setLatitude(endLatitude);
        destination.setLongitude(endLongitude);
        return currentLocation.distanceTo(destination);
    }

    private void prepareSellerDetails(Seller seller) {
        if (seller != null) {
            Details sellerDetails = seller.getDetails();
            if (sellerDetails != null) {
                shopName = sellerDetails.getShopName();
                shopMobileNumber = sellerDetails.getMobile();
                Location location = sellerDetails.getLocation();
                if (location != null) {
                    sellerAddress = location.getGeoLocationAddress();
                    sellerLatitude = location.getLatitude();
                    sellerLongitude = location.getLongitude();
                }

                tvShopName.setText(shopName);
                tvShopPhoneNumber.setText(String.valueOf(shopMobileNumber));
                tvShopAddress.setText(sellerAddress);
                double sellerDistance = calculateDistanceBetweenTwoLatLngs(sellerLatitude, sellerLongitude);
                tvSellerDistance.setText(sellerDistance + " " + "KMS");
            }
        }
    }

    @SuppressLint("SetTextI18n")
    private void prepareCustomerDetails(Customer customer) {
        if (customer != null) {
            CustomerDetails customerDetails = customer.getCustomerDetails();
            AddressInfo addressInfo = customer.getAddressInfo();

            tvName.setText("N/A");
            tvPhoneNumber.setText("N/A");

            if (customerDetails != null) {
                String firstName = customerDetails.getFirstName();
                Long mobileNumber = customerDetails.getMobile();

                tvName.setText(firstName);
                tvPhoneNumber.setText(String.valueOf(mobileNumber));
            }

            if (addressInfo != null) {
                customerAddress = addressInfo.getAddress();
                customerLandMark = addressInfo.getLandmark();
                Location location = addressInfo.getLocation();
                if (location != null) {
                    customerLatitude = location.getLatitude();
                    customerLongitude = location.getLongitude();
                    customerGeoLocationAddress = location.getGeoLocationAddress();
                }

                tvAddress.setText(customerAddress + "\n" + customerLandMark);
                double distance = calculateDistanceBetweenTwoLatLngs(customerLatitude, customerLongitude);
                tvDestinationDistance.setText(distance + " " + "KMS");
            }
        }
    }

    private void prepareOrderDetails() {
        if (listOfOrderDetails != null) {
            if (listOfOrderDetails.size() != 0) {
                initializeAdapter();
            }
        }
    }


    @SuppressLint("SetTextI18n")
    private void preparePriceDetails() {
        if (listOfOrderDetails != null) {
            if (listOfOrderDetails.size() != 0) {
                tax = 0;
                subTotal = 0;
                totalPrice = 0;
                discount = 0;
                deliveryFee = 0;
                totalDiscount = 0;
                for (int index = 0; index < listOfOrderDetails.size(); index++) {
                    OrderDetail orderDetail = listOfOrderDetails.get(index);
                    if (orderDetail != null) {
                        float subTotal = orderDetail.getSubTotal();
                        float totalPrice = orderDetail.getTotal();
                        float tax = orderDetail.getTax();
                        float deliveryFee = orderDetail.getDeliveryFee();
                        float discount = orderDetail.getDiscount();
                        float promoDiscount = orderDetail.getPromoDiscount();

                        this.tax = this.tax + tax;
                        this.subTotal = this.subTotal + subTotal;
                        this.totalPrice = this.totalPrice + totalPrice;
                        this.deliveryFee = this.deliveryFee + deliveryFee;
                        totalDiscount = totalDiscount + discount + promoDiscount;
                    }
                }

                tvDeliveryBoyFee.setText(getString(R.string.current_currency) + " " + deliveryFee);
                tvSubTotal.setText(getString(R.string.current_currency) + " " + subTotal);
                tvTax.setText(getString(R.string.current_currency) + " " + tax);
                tvDiscountOnMRP.setText(" - " + getString(R.string.current_currency) + " " + totalDiscount);
                tvFinalPrice.setText(getString(R.string.current_currency) + " " + totalPrice);
            }
        }
    }

    private void prepareReturnOrderDetails() {
        if (listOfDeliveryJobs.size() == 2) {
            DeliveryJobs deliveryJobs = listOfDeliveryJobs.get(1);
            jobId = deliveryJobs.getJobId();
            tvJobId.setText(jobId);
        }
        if (jobId != null) {
            if (!jobId.isEmpty()) {
                if (jobId.startsWith("RJ")) {
                    if (listOfDeliveryJobs != null) {
                        if (listOfDeliveryJobs.size() != 0) {
                            if (listOfDeliveryJobs.size() == 2) {
                                llReturn.setVisibility(View.VISIBLE);
                                DeliveryJobs deliveryJobs = listOfDeliveryJobs.get(1);
                                if (deliveryJobs != null) {
                                    returnDeliveryJobId = deliveryJobs.getId();
                                    if (returnRequest != null) {
                                        returnRequestId = returnRequest.getId();
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    private void prepareReturnDetails() {
        Intent returnDetailsIntent = new Intent(this, ReturnOTPDetailsActivity.class);
        Bundle bundle = new Bundle();
        bundle.putInt(getString(R.string.job_id), returnDeliveryJobId);
        bundle.putInt(getString(R.string.return_request_id), returnRequestId);
        returnDetailsIntent.putExtras(bundle);
        startActivity(returnDetailsIntent);
    }

    private void prepareReturnedDetails() {
        Intent returnedDetailsIntent = new Intent(this, ReturnedOTPDetailsActivity.class);
        Bundle bundle = new Bundle();
        bundle.putInt(getString(R.string.job_id), returnDeliveryJobId);
        bundle.putInt(getString(R.string.return_request_id), returnRequestId);
        returnedDetailsIntent.putExtras(bundle);
        startActivity(returnedDetailsIntent);
    }

    private void initializeAdapter() {
        OrderedProductsAdapter orderedProductsAdapter = new OrderedProductsAdapter(this, listOfOrderDetails);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        rvOrderedProducts.setLayoutManager(layoutManager);
        rvOrderedProducts.setItemAnimator(new DefaultItemAnimator());
        rvOrderedProducts.setAdapter(orderedProductsAdapter);
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (permissions.length != 0 && grantResults.length != 0) {
            switch (requestCode) {
                case Constants.REQUEST_CODE_FOR_LOCATION_PERMISSION:
                    if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                        checkLocationAccessPermission();
                    } else {
                        checkLocationAccessPermission();
                    }
                    break;
                default:
                    super.onRequestPermissionsResult(requestCode, permissions, grantResults);
            }
        }
    }

    @Override
    public void OnLatLongReceived(double latitude, double longitude) {
        this.latitude = latitude;
        this.longitude = longitude;
    }

    @Override
    public void close() {
        prepareDetails();
    }
}
