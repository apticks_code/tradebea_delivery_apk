package com.tradebeadeliveryboy.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.tradebeadeliveryboy.ApiCalls.GetSelectedPaybackRequestApiCall;
import com.tradebeadeliveryboy.R;
import com.tradebeadeliveryboy.interfaces.CloseCallBack;
import com.tradebeadeliveryboy.interfaces.HttpReqResCallBack;
import com.tradebeadeliveryboy.models.responseModels.getSelecedPaybackResponse.GetSelectedPaybackResponse;
import com.tradebeadeliveryboy.models.responseModels.getSelecedPaybackResponse.SelectedPaybackData;
import com.tradebeadeliveryboy.utils.Constants;
import com.tradebeadeliveryboy.utils.PreferenceConnector;
import com.tradebeadeliveryboy.utils.UserData;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Iterator;

public class SenderReceiverActivity extends BaseActivity implements View.OnClickListener, HttpReqResCallBack, CloseCallBack {

    private LinearLayout llOTP;
    private ImageView ivBackArrow;
    private TextView tvActionBarTitle, tvSenderId, tvReceiverId, tvAmount, tvSource, tvDate, tvStatus, tvVerify, tvOTP;

    private SelectedPaybackData selectedPaybackData;

    private String otp = "";
    private String date = "";
    private String paybackId = "";
    private String senderId = "";
    private String receiverId = "";
    private String isThisMyRequest = "";
    private String paybackSourceId = "";
    private String paybackSourceName = "";

    private int id = -1;
    private int amount = 0;
    private int tradebeaOTP;
    private int superIntendentOTP;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_payback_sender_receiver);
        getDataFromIntent();
        initializeUI();
        initializeListeners();
        prepareDetails();
    }

    private void getDataFromIntent() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            if (bundle.containsKey(getString(R.string.pay_back_id)))
                paybackId = bundle.getString(getString(R.string.pay_back_id), "");
            if (bundle.containsKey(getString(R.string.is_this_my_request)))
                isThisMyRequest = bundle.getString(getString(R.string.is_this_my_request), "");
            if (bundle.containsKey(getString(R.string.pay_back_source_name)))
                paybackSourceName = bundle.getString(getString(R.string.pay_back_source_name), "");
            if (bundle.containsKey(getString(R.string.pay_back_source_id)))
                paybackSourceId = bundle.getString(getString(R.string.pay_back_source_id), "");
            if (bundle.containsKey(getString(R.string.otp)))
                otp = bundle.getString(getString(R.string.otp), "");
        }
    }

    private void initializeUI() {
        llOTP = findViewById(R.id.llOTP);
        tvOTP = findViewById(R.id.tvOTP);
        tvDate = findViewById(R.id.tvDate);
        tvAmount = findViewById(R.id.tvAmount);
        tvSource = findViewById(R.id.tvSource);
        tvStatus = findViewById(R.id.tvStatus);
        tvVerify = findViewById(R.id.tvVerify);
        tvSenderId = findViewById(R.id.tvSenderId);
        ivBackArrow = findViewById(R.id.ivBackArrow);
        tvReceiverId = findViewById(R.id.tvReceiverId);
        tvActionBarTitle = findViewById(R.id.tvActionBarTitle);
    }

    private void initializeListeners() {
        tvVerify.setOnClickListener(this);
        ivBackArrow.setOnClickListener(this);
    }

    private void prepareDetails() {
        if (otp.isEmpty()) {
            tvOTP.setText("NA");
        } else {
            tvOTP.setText(otp);
        }
        if (isThisMyRequest.equalsIgnoreCase("1")) {
            llOTP.setVisibility(View.GONE);
            tvVerify.setVisibility(View.VISIBLE);
            tvActionBarTitle.setText(getString(R.string.sender));
        } else {
            llOTP.setVisibility(View.VISIBLE);
            tvVerify.setVisibility(View.GONE);
            tvActionBarTitle.setText(getString(R.string.receiver));
        }

        if (paybackSourceId.equalsIgnoreCase("2")) {
            tvVerify.setVisibility(View.GONE);
            llOTP.setVisibility(View.GONE);
        } else if (paybackSourceId.equalsIgnoreCase("1")) {
            if (isThisMyRequest.equalsIgnoreCase("1")) {
                tvVerify.setVisibility(View.VISIBLE);
                llOTP.setVisibility(View.GONE);
            } else {
                llOTP.setVisibility(View.VISIBLE);
                tvVerify.setVisibility(View.GONE);
            }
        }
        showProgressBar(this);
        String token = PreferenceConnector.readString(this, getString(R.string.user_token), "");
        GetSelectedPaybackRequestApiCall.serviceCallToGetSelectedPaybackRequests(this, null, null, token, paybackId);
    }

    @Override
    protected void onResume() {
        super.onResume();
        UserData.getInstance().setSenderReceiverContext(this);
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        if (id == R.id.ivBackArrow) {
            onBackPressed();
        } else if (id == R.id.tvVerify) {
            prepareVerifyOTP();
        }
    }

    private void prepareVerifyOTP() {
        Intent verifySenderOTPIntent = new Intent(this, VerifySenderOTPActivity.class);
        Bundle bundle = new Bundle();
        bundle.putString(getString(R.string.id), String.valueOf(id));
        bundle.putString(getString(R.string.tradebea_otp), String.valueOf(tradebeaOTP));
        bundle.putString(getString(R.string.superintendent_otp), String.valueOf(superIntendentOTP));
        verifySenderOTPIntent.putExtras(bundle);
        startActivity(verifySenderOTPIntent);
    }

    @Override
    public void jsonResponseReceived(String jsonResponse, int statusCode, int requestType) {
        if (requestType == Constants.SERVICE_CALL_TO_GET_SELECTED_PAYBACK_REQUEST) {
            if (jsonResponse != null) {
                GetSelectedPaybackResponse getSelectedPaybackResponse = new Gson().fromJson(jsonResponse, GetSelectedPaybackResponse.class);
                if (getSelectedPaybackResponse != null) {
                    boolean status = getSelectedPaybackResponse.getStatus();
                    if (status) {
                        selectedPaybackData = getSelectedPaybackResponse.getSelectedPaybackData();
                        if (selectedPaybackData != null) {
                            id = selectedPaybackData.getId();
                            senderId = selectedPaybackData.getSenderUniqueId();
                            receiverId = selectedPaybackData.getReceiverUniqueId();
                            amount = selectedPaybackData.getAmount();
                            date = selectedPaybackData.getDate();
                            tradebeaOTP = selectedPaybackData.getTradebeaOtp();
                            superIntendentOTP = selectedPaybackData.getSuperintendentOtp();
                            String sourceString = selectedPaybackData.getSource().getLabel();
                            String statusString = selectedPaybackData.getStatus().getLabel();
                            if (selectedPaybackData.getStatus().getId() == 1) {
                                llOTP.setVisibility(View.GONE);
                                tvVerify.setVisibility(View.GONE);
                            }

                            tvSenderId.setText(senderId);
                            tvReceiverId.setText(receiverId);
                            tvAmount.setText(String.valueOf(amount));
                            tvSource.setText(sourceString);
                            tvDate.setText(date);
                            tvStatus.setText(statusString);
                        }
                    }
                }
            }
            closeProgressbar();
        }
    }

    private String getValueFromJsonObject(JsonObject jsonObject) {
        try {
            JSONObject object = new JSONObject(jsonObject.toString());
            Iterator<String> iter = object.keys();
            while (iter.hasNext()) {
                String key = iter.next();
                try {
                    return String.valueOf(jsonObject.get(key));
                } catch (Exception exception) {
                    exception.printStackTrace();
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return "NA";
    }

    @Override
    public void close() {
        finish();
    }
}
