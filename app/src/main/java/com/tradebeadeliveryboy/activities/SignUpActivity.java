package com.tradebeadeliveryboy.activities;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.core.content.ContextCompat;

import com.google.gson.Gson;
import com.tradebeadeliveryboy.ApiCalls.SignUpApiCall;
import com.tradebeadeliveryboy.R;
import com.tradebeadeliveryboy.interfaces.HttpReqResCallBack;
import com.tradebeadeliveryboy.models.responseModels.signUpResponse.SignUpResponse;
import com.tradebeadeliveryboy.utils.CommonMethods;
import com.tradebeadeliveryboy.utils.Constants;


public class SignUpActivity extends BaseActivity implements RadioGroup.OnCheckedChangeListener, View.OnClickListener, HttpReqResCallBack {

    private RadioGroup radioGroup;
    private TextView tvCreateAccount;
    private LinearLayout llCreateAccount;
    private ImageView ivBackArrow, ivShowHidePassword;
    private EditText etFirstName, etLastName, etEmailId, etMobileNumber, etPassword, etRefID;

    private String selectedGender = "";

    private boolean isShowPassword;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_sign_up);
        setStatusBarColor();
        initializeUi();
        initializeListeners();
    }

    private void setStatusBarColor() {
        Window window = getWindow();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.setStatusBarColor(ContextCompat.getColor(this, R.color.white));
        }
    }

    private void initializeUi() {
        etEmailId = findViewById(R.id.etEmailId);
        radioGroup = findViewById(R.id.radioGroup);
        etLastName = findViewById(R.id.etLastName);
        etPassword = findViewById(R.id.etPassword);
        etRefID = findViewById(R.id.etRefID);
        ivBackArrow = findViewById(R.id.ivBackArrow);
        etFirstName = findViewById(R.id.etFirstName);
        etMobileNumber = findViewById(R.id.etMobileNumber);
        tvCreateAccount = findViewById(R.id.tvCreateAccount);
        llCreateAccount = findViewById(R.id.llCreateAccount);
        ivShowHidePassword = findViewById(R.id.ivShowHidePassword);
    }

    private void initializeListeners() {
        ivBackArrow.setOnClickListener(this);
        tvCreateAccount.setOnClickListener(this);
        llCreateAccount.setOnClickListener(this);
        ivShowHidePassword.setOnClickListener(this);
        radioGroup.setOnCheckedChangeListener(this);
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id) {
            case R.id.ivBackArrow:
                onBackPressed();
                break;
            case R.id.tvCreateAccount:
            case R.id.llCreateAccount:
                prepareCreateAccountDetails();
                break;
            case R.id.ivShowHidePassword:
                prepareShowHidePasswordDetails();
                break;
            default:
                break;
        }
    }

    private void prepareCreateAccountDetails() {
        String firstName = etFirstName.getText().toString();
        String lastName = etLastName.getText().toString();
        String emailId = etEmailId.getText().toString();
        String mobileNumber = etMobileNumber.getText().toString();
        String password = etPassword.getText().toString();
        String referral_ID = etRefID.getText().toString();
        if (referral_ID == null) {
            referral_ID = "";
        }
        if (!firstName.isEmpty()) {
            if (!lastName.isEmpty()) {
                if (!emailId.isEmpty()) {
                    if (CommonMethods.isValidEmailFormat(emailId)) {
                        if (!mobileNumber.isEmpty()) {
                            if (!password.isEmpty()) {
                                if (!selectedGender.isEmpty()) {
                                    showProgressBar(this);
                                    SignUpApiCall.serviceCallToSignUp(this, firstName, lastName, selectedGender, emailId, mobileNumber, password, referral_ID);
                                } else {
                                    Toast.makeText(this, getString(R.string.please_select_gender), Toast.LENGTH_SHORT).show();
                                }
                            } else {
                                Toast.makeText(this, getString(R.string.please_enter_password), Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Toast.makeText(this, getString(R.string.please_enter_phone_number), Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(this, getString(R.string.please_enter_valid_email), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(this, getString(R.string.please_enter_email_id), Toast.LENGTH_SHORT).show();
                }
            } else {
                Toast.makeText(this, getString(R.string.please_enter_last_name), Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(this, getString(R.string.please_enter_first_name), Toast.LENGTH_SHORT).show();
        }
    }

    private void prepareShowHidePasswordDetails() {
        if (!isShowPassword) {
            isShowPassword = true;
            etPassword.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
            ivShowHidePassword.setImageResource(R.drawable.ic_eye_open);
            String password = etPassword.getText().toString();
            if (!password.isEmpty()) {
                etPassword.setSelection(password.length());
            }
        } else {
            isShowPassword = false;
            etPassword.setTransformationMethod(PasswordTransformationMethod.getInstance());
            ivShowHidePassword.setImageResource(R.drawable.ic_eye_close);
            String password = etPassword.getText().toString();
            if (!password.isEmpty()) {
                etPassword.setSelection(password.length());
            }
        }
    }

    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {
        RadioButton radioButton = radioGroup.findViewById(checkedId);
        selectedGender = radioButton.getText().toString();
    }

    @Override
    public void jsonResponseReceived(String jsonResponse, int statusCode, int requestType) {
        switch (requestType) {
            case Constants.SERVICE_CALL_FOR_SIGN_UP:
                if (jsonResponse != null) {
                    SignUpResponse signUpResponse = new Gson().fromJson(jsonResponse, SignUpResponse.class);
                    if (signUpResponse != null) {
                        boolean status = signUpResponse.getStatus();
                        String message = signUpResponse.getMessage();
                        if (status) {
                            Toast.makeText(this, Html.fromHtml(message), Toast.LENGTH_SHORT).show();
                            goToLogin();
                        } else {
                            Toast.makeText(this, Html.fromHtml(message), Toast.LENGTH_SHORT).show();
                        }
                    }
                }
                closeProgressbar();
                break;
            default:
                break;
        }
    }

    private void goToLogin() {
        Intent loginIntent = new Intent(this, LoginActivity.class);
        loginIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(loginIntent);
        finish();
    }
}
