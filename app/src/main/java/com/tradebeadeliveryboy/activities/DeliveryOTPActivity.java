package com.tradebeadeliveryboy.activities;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.mukesh.OtpView;
import com.tradebeadeliveryboy.ApiCalls.VerifyDeliveryOTPApiCall;
import com.tradebeadeliveryboy.R;
import com.tradebeadeliveryboy.interfaces.CloseCallBack;
import com.tradebeadeliveryboy.interfaces.HttpReqResCallBack;
import com.tradebeadeliveryboy.models.responseModels.verifyDeliveryOTPResponse.VerifyDeliveryOTPResponse;
import com.tradebeadeliveryboy.utils.Constants;
import com.tradebeadeliveryboy.utils.UserData;

public class DeliveryOTPActivity extends BaseActivity implements View.OnClickListener, HttpReqResCallBack {

    private OtpView otpView;
    private TextView tvSubmit;
    private ImageView ivBackArrow;
    private LinearLayout llSubmit;
    private EditText etAmountCollected, etRemarks;

    private String selectedOrderId = "";

    private int deliveryJobId;
    private int paymentMethodId = 0;// 1- COD , 2 - OnlinePayment

    private float totalPrice = 0f;
    private float usedWalletAmount = 0f;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_delivery_otp);
        getDataFromIntent();
        initializeUi();
        initializeListeners();
    }

    private void getDataFromIntent() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            if (bundle.containsKey(getString(R.string.order_id))) {
                selectedOrderId = bundle.getString(getString(R.string.order_id), "");
            }
            if (bundle.containsKey(getString(R.string.payment_method_id))) {
                paymentMethodId = bundle.getInt(getString(R.string.payment_method_id));
            }
            if (bundle.containsKey(getString(R.string.total_price))) {
                totalPrice = bundle.getFloat(getString(R.string.total_price));
            }
            if (bundle.containsKey(getString(R.string.delivery_job_id))) {
                deliveryJobId = bundle.getInt(getString(R.string.delivery_job_id));
            }
            if (bundle.containsKey(getString(R.string.used_wallet_amount)))
                usedWalletAmount = bundle.getFloat(getString(R.string.used_wallet_amount));
        }
    }

    private void initializeUi() {
        otpView = findViewById(R.id.otpView);
        llSubmit = findViewById(R.id.llSubmit);
        tvSubmit = findViewById(R.id.tvSubmit);
        etRemarks = findViewById(R.id.etRemarks);
        ivBackArrow = findViewById(R.id.ivBackArrow);
        etAmountCollected = findViewById(R.id.etAmountCollected);

        if (paymentMethodId == 1) {
            float remainingAmount = totalPrice - usedWalletAmount;
            etAmountCollected.setText(String.valueOf(remainingAmount));
            etAmountCollected.setVisibility(View.VISIBLE);
        } else {
            etAmountCollected.setVisibility(View.GONE);
        }
    }

    private void initializeListeners() {
        llSubmit.setOnClickListener(this);
        tvSubmit.setOnClickListener(this);
        ivBackArrow.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        if (id == R.id.ivBackArrow) {
            onBackPressed();
        } else if (id == R.id.llSubmit) {
            prepareDetails();
        } else if (id == R.id.tvSubmit) {
            prepareDetails();
        }
    }

    private void prepareDetails() {
        String otp = otpView.getText().toString();
        String amountCollected = etAmountCollected.getText().toString();
        String remarks = etRemarks.getText().toString();

        if (!otp.isEmpty()) {
            if (etAmountCollected.getVisibility() == View.VISIBLE) {
                if (!amountCollected.isEmpty()) {
                    showProgressBar(this);
                    VerifyDeliveryOTPApiCall.serviceCallForVerifyDeliveryOTPApiCall(this, null, null, selectedOrderId, otp, amountCollected, remarks, deliveryJobId);
                } else {
                    Toast.makeText(this, getString(R.string.please_enter_amount_collected), Toast.LENGTH_SHORT).show();
                }
            } else {
                showProgressBar(this);
                VerifyDeliveryOTPApiCall.serviceCallForVerifyDeliveryOTPApiCall(this, null, null, selectedOrderId, otp, amountCollected, remarks, deliveryJobId);
            }
        } else {
            Toast.makeText(this, getString(R.string.please_enter_otp), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void jsonResponseReceived(String jsonResponse, int statusCode, int requestType) {
        if (requestType == Constants.SERVICE_CALL_FOR_VERIFY_DELIVERY_OTP) {
            if (jsonResponse != null) {
                VerifyDeliveryOTPResponse verifyDeliveryOTPResponse = new Gson().fromJson(jsonResponse, VerifyDeliveryOTPResponse.class);
                if (verifyDeliveryOTPResponse != null) {
                    boolean status = verifyDeliveryOTPResponse.getStatus();
                    String message = verifyDeliveryOTPResponse.getMessage();
                    if (status) {
                        Context context = UserData.getInstance().getOrderDetailsContext();
                        if (context != null) {
                            CloseCallBack closeCallBack = (CloseCallBack) context;
                            closeCallBack.close();
                        }
                        finish();
                    } else {
                        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
                    }
                }
            }
            closeProgressbar();
        }
    }
}
