package com.tradebeadeliveryboy.activities;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.os.Bundle;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.switchmaterial.SwitchMaterial;
import com.google.gson.Gson;
import com.tradebeadeliveryboy.ApiCalls.GetProfileDetailsApiCall;
import com.tradebeadeliveryboy.ApiCalls.UpdateDeliveryBoySession;
import com.tradebeadeliveryboy.R;
import com.tradebeadeliveryboy.adapters.NavigationMenuAdapter;
import com.tradebeadeliveryboy.fragments.HomeFragment;
import com.tradebeadeliveryboy.fragments.MyDeliveryBoysFragment;
import com.tradebeadeliveryboy.fragments.OrdersFragment;
import com.tradebeadeliveryboy.fragments.PaybackFragment;
import com.tradebeadeliveryboy.fragments.PaymentsFragment;
import com.tradebeadeliveryboy.fragments.ProductsFragment;
import com.tradebeadeliveryboy.fragments.ReferralPageFragment;
import com.tradebeadeliveryboy.fragments.ReportsFragment;
import com.tradebeadeliveryboy.fragments.ReturnsFragment;
import com.tradebeadeliveryboy.fragments.SupportFragment;
import com.tradebeadeliveryboy.fragments.WalletFragment;
import com.tradebeadeliveryboy.interfaces.HttpReqResCallBack;
import com.tradebeadeliveryboy.interfaces.LocationUpdateCallBack;
import com.tradebeadeliveryboy.interfaces.OrdersNavigationCallBack;
import com.tradebeadeliveryboy.models.NavigationMenuItems;
import com.tradebeadeliveryboy.models.responseModels.profileResponse.GetProfileData;
import com.tradebeadeliveryboy.models.responseModels.profileResponse.ProfileResponse;
import com.tradebeadeliveryboy.models.responseModels.profileResponse.UserDetails;
import com.tradebeadeliveryboy.models.responseModels.updateDeliveryBoySessionResponse.UpdateDeliveryBoySessionResponse;
import com.tradebeadeliveryboy.utils.AppPermissions;
import com.tradebeadeliveryboy.utils.Constants;
import com.tradebeadeliveryboy.utils.GetCurrentLocation;
import com.tradebeadeliveryboy.utils.PreferenceConnector;
import com.tradebeadeliveryboy.utils.Utils;

import java.util.LinkedList;


public class HomeActivity extends BaseActivity implements View.OnClickListener, NavigationMenuAdapter.ItemClickListener, HttpReqResCallBack, CompoundButton.OnCheckedChangeListener, LocationUpdateCallBack, OrdersNavigationCallBack {

    private TextView tvTitle;
    private Fragment fragment;
    private RecyclerView rvMenu;
    private DrawerLayout drawerLayout;
    private ImageView ivMenuNav, ivNotification;
    private SwitchMaterial switchDeliveryBoyStatus;

    private LinkedList<NavigationMenuItems> listOfNavigationMenuItems;

    private String token = "";
    private String address = "";
    private String userName = "";
    private String userUniqueId = "";
    private String userImageUrl = "";

    private Long mobileNumber;

    private double latitude = 0.0;
    private double longitude = 0.0;

    private boolean isChecked;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_home);
        initializeUi();
        initializeListeners();
        checkLocationAccessPermission();
        getUserProfile();
    }

    private void initializeUi() {
        rvMenu = findViewById(R.id.rvMenu);
        tvTitle = findViewById(R.id.tvTitle);
        ivMenuNav = findViewById(R.id.ivMenuNav);
        drawerLayout = findViewById(R.id.drawerLayout);
        ivNotification = findViewById(R.id.ivNotification);
        switchDeliveryBoyStatus = findViewById(R.id.switchDeliveryBoyStatus);

        token = PreferenceConnector.readString(this, getString(R.string.user_token), "");
        PreferenceConnector.writeBoolean(this, getString(R.string.refresh_profile), false);
    }

    private void initializeListeners() {
        ivMenuNav.setOnClickListener(this);
        ivNotification.setOnClickListener(this);
        switchDeliveryBoyStatus.setOnCheckedChangeListener(this);
    }

    private void checkLocationAccessPermission() {
        if (AppPermissions.checkPermissionForAccessLocation(this)) {
            LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
            new GetCurrentLocation(this, null, locationManager);
        } else {
            AppPermissions.requestPermissionForLocation(this);
        }
    }

    private void getUserProfile() {
        showProgressBar(this);
        GetProfileDetailsApiCall.serviceCallToGetProfileDetails(this, null, null, token);
    }

    private void checkOnlineStatus() {
        boolean isOnline = PreferenceConnector.readBoolean(this, getString(R.string.online), false);
        if (isOnline) {
            switchDeliveryBoyStatus.setChecked(true);
        } else {
            switchDeliveryBoyStatus.setChecked(false);
        }
    }

    private void prepareDetails() {
        boolean amIADeliverySuperintendent = PreferenceConnector.readBoolean(this, getString(R.string.pref_is_delivery_superintendent), false);
        listOfNavigationMenuItems = new LinkedList<>();
        listOfNavigationMenuItems.add(new NavigationMenuItems(R.drawable.ic_home, getResources().getString(R.string.home)));
        listOfNavigationMenuItems.add(new NavigationMenuItems(R.drawable.ic_orders, getResources().getString(R.string.orders)));
        //listOfNavigationMenuItems.add(new NavigationMenuItems(R.drawable.ic_products, getResources().getString(R.string.products)));
        //listOfNavigationMenuItems.add(new NavigationMenuItems(R.drawable.ic_reports, getResources().getString(R.string.reports)));
        listOfNavigationMenuItems.add(new NavigationMenuItems(R.drawable.ic_payment, getResources().getString(R.string.payments)));
        //listOfNavigationMenuItems.add(new NavigationMenuItems(R.drawable.ic_return, getResources().getString(R.string.returns)));
        listOfNavigationMenuItems.add(new NavigationMenuItems(R.drawable.ic_my_wallet, getResources().getString(R.string.wallet)));
        listOfNavigationMenuItems.add(new NavigationMenuItems(R.drawable.ic_my_wallet, getResources().getString(R.string.pay_back)));
        if (amIADeliverySuperintendent) {
            listOfNavigationMenuItems.add(new NavigationMenuItems(R.drawable.ic_orders, getString(R.string.my_delivery_boys)));
            listOfNavigationMenuItems.add(new NavigationMenuItems(R.drawable.ic_orders, getString(R.string.referral_page)));
        }
        listOfNavigationMenuItems.add(new NavigationMenuItems(R.drawable.ic_support, getResources().getString(R.string.support)));
        listOfNavigationMenuItems.add(new NavigationMenuItems(R.drawable.ic_logout, getResources().getString(R.string.logout)));
        initializeAdapter();
        onItemClick(null, 1);
    }

    private void initializeAdapter() {
        NavigationMenuAdapter navigationMenuAdapter = new NavigationMenuAdapter(HomeActivity.this, listOfNavigationMenuItems, userName, userImageUrl, userUniqueId, mobileNumber);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        rvMenu.setLayoutManager(layoutManager);
        rvMenu.setItemAnimator(new DefaultItemAnimator());
        navigationMenuAdapter.setClickListener(this);
        rvMenu.setAdapter(navigationMenuAdapter);
    }

    @Override
    protected void onResume() {
        super.onResume();
        boolean refreshProfile = PreferenceConnector.readBoolean(this, getString(R.string.refresh_profile), false);
        if (refreshProfile) {
            getUserProfile();
            PreferenceConnector.writeBoolean(this, getString(R.string.refresh_profile), false);
        }
    }

    public void onClick(View view) {
        int id = view.getId();
        if (id == R.id.ivMenuNav) {
            if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
                drawerLayout.closeDrawer(GravityCompat.START);
            } else
                drawerLayout.openDrawer(GravityCompat.START);
        } else if (id == R.id.ivNotification) {
            goToNotifications();
        }
    }

    private void goToNotifications() {
        Intent notificationsIntent = new Intent(this, NotificationActivity.class);
        startActivity(notificationsIntent);
    }

    @Override
    public void onItemClick(View view, int position) {
        if (position != 0) {
            NavigationMenuItems navigationMenuItems = listOfNavigationMenuItems.get(position - 1);
            String menuTitle = navigationMenuItems.getMenuItemName();
            if (menuTitle.equalsIgnoreCase(getString(R.string.home))) {
                tvTitle.setText(menuTitle);
                fragment = new HomeFragment();
            } else if (menuTitle.equalsIgnoreCase(getString(R.string.orders))) {
                tvTitle.setText(menuTitle);
                fragment = new OrdersFragment();
            } else if (menuTitle.equalsIgnoreCase(getString(R.string.products))) {
                tvTitle.setText(menuTitle);
                fragment = new ProductsFragment();
            } else if (menuTitle.equalsIgnoreCase(getString(R.string.reports))) {
                tvTitle.setText(menuTitle);
                fragment = new ReportsFragment();
            } else if (menuTitle.equalsIgnoreCase(getString(R.string.payments))) {
                tvTitle.setText(menuTitle);
                fragment = new PaymentsFragment();
            } else if (menuTitle.equalsIgnoreCase(getString(R.string.returns))) {
                tvTitle.setText(menuTitle);
                fragment = new ReturnsFragment();
            } else if (menuTitle.equalsIgnoreCase(getString(R.string.support))) {
                tvTitle.setText(menuTitle);
                fragment = new SupportFragment();
            } else if (menuTitle.equalsIgnoreCase(getString(R.string.wallet))) {
                tvTitle.setText(menuTitle);
                fragment = new WalletFragment();
            } else if (menuTitle.equalsIgnoreCase(getString(R.string.pay_back))) {
                tvTitle.setText(menuTitle);
                fragment = new PaybackFragment();
            } else if (menuTitle.equalsIgnoreCase(getString(R.string.my_delivery_boys))) {
                tvTitle.setText(menuTitle);
                fragment = new MyDeliveryBoysFragment();
            } else if (menuTitle.equalsIgnoreCase(getString(R.string.referral_page))) {
                tvTitle.setText(menuTitle);
                fragment = new ReferralPageFragment();
            } else {
                showLogoutAlertDialog();
            }
            if (fragment != null) {
                FragmentManager fragmentManager = getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.detailsFragment, fragment);
                fragmentTransaction.setTransitionStyle(R.style.Theme_Transparent);
                fragmentTransaction.commitAllowingStateLoss();
            }

            if (view != null) {
                if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
                    drawerLayout.closeDrawer(GravityCompat.START);
                } else
                    drawerLayout.openDrawer(GravityCompat.START);
            }
        } else {
            goToEditProfile();
        }
    }

    private void goToEditProfile() {
        Intent editProfileIntent = new Intent(this, EditProfileDetailsActivity.class);
        startActivity(editProfileIntent);
    }

    private void showLogoutAlertDialog() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this, R.style.MyDialogTheme);
        alertDialogBuilder.setMessage(getString(R.string.are_you_sure_do_you_want_to_logout));
        alertDialogBuilder.setCancelable(false);
        alertDialogBuilder.setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                clearDataInPreferences();
                goToLogin();
                dialog.cancel();
            }
        });
        alertDialogBuilder.setNegativeButton(getString(R.string.no), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    private void goToLogin() {
        Intent loginIntent = new Intent(this, LoginActivity.class);
        loginIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(loginIntent);
    }

    private void clearDataInPreferences() {
        PreferenceConnector.clearData(this);
    }

    @Override
    public void jsonResponseReceived(String jsonResponse, int statusCode, int requestType) {
        switch (requestType) {
            case Constants.SERVICE_CALL_TO_GET_PROFILE_DETAILS:
                if (jsonResponse != null) {
                    ProfileResponse profileResponse = new Gson().fromJson(jsonResponse, ProfileResponse.class);
                    if (profileResponse != null) {
                        boolean status = profileResponse.getStatus();
                        String message = profileResponse.getMessage();
                        if (status) {
                            GetProfileData getProfileData = profileResponse.getGetProfileData();
                            if (getProfileData != null) {
                                UserDetails userDetails = getProfileData.getUserDetails();
                                if (userDetails != null) {
                                    userName = userDetails.getFirstName();
                                    userUniqueId = userDetails.getUniqueId();
                                    userImageUrl = userDetails.getProfileImage();
                                    mobileNumber = userDetails.getMobile();
                                    PreferenceConnector.writeString(this, getString(R.string.pref_unique_id), userUniqueId);
                                }
                            }
                        } else {
                            Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
                        }
                    }
                }
                checkOnlineStatus();
                prepareDetails();
                closeProgressbar();
                break;
            case Constants.SERVICE_CALL_FOR_UPDATE_DELIVERY_BOY_SESSION:
                if (jsonResponse != null) {
                    UpdateDeliveryBoySessionResponse updateDeliveryBoySessionResponse = new Gson().fromJson(jsonResponse, UpdateDeliveryBoySessionResponse.class);
                    if (updateDeliveryBoySessionResponse != null) {
                        boolean status = updateDeliveryBoySessionResponse.getStatus();
                        String message = updateDeliveryBoySessionResponse.getMessage();
                        if (status) {
                            if (isChecked) {
                                PreferenceConnector.writeBoolean(this, getString(R.string.online), true);
                            } else {
                                PreferenceConnector.writeBoolean(this, getString(R.string.online), false);
                            }
                            Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
                        }
                    }
                }
                closeProgressbar();
                break;
            default:
                break;
        }
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if (isChecked) {
            this.isChecked = true;
            switchDeliveryBoyStatus.setText(getString(R.string.on));
            showProgressBar(this);
            UpdateDeliveryBoySession.serviceCallForUpdateDeliveryBoySession(this, null, null, latitude, longitude, 1, address);
        } else {
            this.isChecked = false;
            switchDeliveryBoyStatus.setText(getString(R.string.off));
            showProgressBar(this);
            UpdateDeliveryBoySession.serviceCallForUpdateDeliveryBoySession(this, null, null, latitude, longitude, 2, address);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (permissions.length != 0 && grantResults.length != 0) {
            switch (requestCode) {
                case Constants.REQUEST_CODE_FOR_LOCATION_PERMISSION:
                    if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                        prepareDetails();
                    } else {
                        prepareDetails();
                    }
                    break;
                default:
                    super.onRequestPermissionsResult(requestCode, permissions, grantResults);
            }
        }
    }

    @Override
    public void OnLatLongReceived(double latitude, double longitude) {
        this.latitude = latitude;
        this.longitude = longitude;
        address = Utils.geoCode(this, latitude, longitude);
    }

    @Override
    public void orderNavigation() {
        onItemClick(null, 2);
    }
}
