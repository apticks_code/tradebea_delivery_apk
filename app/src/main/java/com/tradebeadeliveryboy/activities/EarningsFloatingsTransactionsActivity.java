package com.tradebeadeliveryboy.activities;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.google.gson.Gson;
import com.tradebeadeliveryboy.ApiCalls.GetTransactionDetails;
import com.tradebeadeliveryboy.R;
import com.tradebeadeliveryboy.adapters.TransactionDetailsAdapter;
import com.tradebeadeliveryboy.interfaces.HttpReqResCallBack;
import com.tradebeadeliveryboy.models.responseModels.getTransactionDetailsResponse.GetTransactionDetailsResponse;
import com.tradebeadeliveryboy.models.responseModels.getTransactionDetailsResponse.TransactionDetails;
import com.tradebeadeliveryboy.models.responseModels.getTransactionDetailsResponse.TransactionDetailsData;
import com.tradebeadeliveryboy.utils.Constants;
import com.tradebeadeliveryboy.utils.PreferenceConnector;

import java.util.LinkedList;

public class EarningsFloatingsTransactionsActivity extends BaseActivity implements View.OnClickListener, HttpReqResCallBack {

    private ImageView ivBackArrow;
    private RecyclerView rvTransactionDetails;
    private TextView tvError, tvActionBarTitle;

    private LinkedList<TransactionDetails> listOfTransactionDetails;
    private String comingFrom = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_earnings_floating_transactions);
        getDataFromIntent();
        initializeUi();
        initializeListeners();
        prepareDetails();
    }

    private void getDataFromIntent() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            if (bundle.containsKey(getString(R.string.coming_from))) {
                comingFrom = bundle.getString(getString(R.string.coming_from), "");
            }
        }
    }

    private void initializeUi() {
        tvError = findViewById(R.id.tvError);
        ivBackArrow = findViewById(R.id.ivBackArrow);
        tvActionBarTitle = findViewById(R.id.tvActionBarTitle);
        rvTransactionDetails = findViewById(R.id.rvTransactionDetails);
    }

    private void initializeListeners() {
        ivBackArrow.setOnClickListener(this);
    }

    private void prepareDetails() {
        tvActionBarTitle.setText(comingFrom);
        String status = "";
        if (comingFrom.equalsIgnoreCase(getString(R.string.earnings))) {
            status = "1";
        } else if (comingFrom.equalsIgnoreCase(getString(R.string.floatings))) {
            status = "2";
        }

        showProgressBar(this);
        String token = PreferenceConnector.readString(this, getString(R.string.user_token), "");
        GetTransactionDetails.serviceCallToGetTransactionDetails(this, null, null, token, "", "", "30", status);
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        if (id == R.id.ivBackArrow) {
            onBackPressed();
        }
    }

    @Override
    public void jsonResponseReceived(String jsonResponse, int statusCode, int requestType) {
        if (requestType == Constants.SERVICE_CALL_TO_GET_TRANSACTION_DETAILS) {
            if (jsonResponse != null) {
                GetTransactionDetailsResponse getTransactionDetailsResponse = new Gson().fromJson(jsonResponse, GetTransactionDetailsResponse.class);
                if (getTransactionDetailsResponse != null) {
                    boolean status = getTransactionDetailsResponse.getStatus();
                    if (status) {
                        TransactionDetailsData transactionDetailsData = getTransactionDetailsResponse.getTransactionDetailsData();
                        if (transactionDetailsData != null) {
                            listOfTransactionDetails = transactionDetailsData.getListOfTransactionDetails();
                            if (listOfTransactionDetails != null) {
                                if (listOfTransactionDetails.size() != 0) {
                                    listIsFull();
                                    initializeAdapter();
                                } else {
                                    listIsEmpty();
                                }
                            } else {
                                listIsEmpty();
                            }
                        }
                    } else {
                        listIsEmpty();
                    }
                }
            }
            closeProgressbar();
        }
    }

    private void initializeAdapter() {
        TransactionDetailsAdapter transactionDetailsAdapter = new TransactionDetailsAdapter(this, listOfTransactionDetails);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        rvTransactionDetails.setLayoutManager(layoutManager);
        rvTransactionDetails.setItemAnimator(new DefaultItemAnimator());
        rvTransactionDetails.setAdapter(transactionDetailsAdapter);
    }

    private void listIsFull() {
        tvError.setVisibility(View.GONE);
        rvTransactionDetails.setVisibility(View.VISIBLE);
    }

    private void listIsEmpty() {
        tvError.setVisibility(View.VISIBLE);
        rvTransactionDetails.setVisibility(View.GONE);
    }
}
