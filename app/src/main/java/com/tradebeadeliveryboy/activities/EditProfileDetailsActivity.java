package com.tradebeadeliveryboy.activities;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.esafirm.imagepicker.features.ImagePicker;
import com.esafirm.imagepicker.model.Image;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.gson.Gson;
import com.tradebeadeliveryboy.ApiCalls.GetProfileDetailsApiCall;
import com.tradebeadeliveryboy.ApiCalls.UpdateEditProfileDetailsApiCall;
import com.tradebeadeliveryboy.R;
import com.tradebeadeliveryboy.interfaces.HttpReqResCallBack;
import com.tradebeadeliveryboy.models.responseModels.profileResponse.GetProfileData;
import com.tradebeadeliveryboy.models.responseModels.profileResponse.ProfileResponse;
import com.tradebeadeliveryboy.models.responseModels.profileResponse.UserDetails;
import com.tradebeadeliveryboy.models.responseModels.updateEditProfileDetailsResponse.UpdateEditProfileDetailsResponse;
import com.tradebeadeliveryboy.utils.AppPermissions;
import com.tradebeadeliveryboy.utils.Constants;
import com.tradebeadeliveryboy.utils.PreferenceConnector;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

import droidninja.filepicker.FilePickerBuilder;
import droidninja.filepicker.FilePickerConst;

public class EditProfileDetailsActivity extends BaseActivity implements RadioGroup.OnCheckedChangeListener, View.OnClickListener, HttpReqResCallBack {

    private RadioGroup radioGroup;
    private LinearLayout llSaveChanges;
    private RadioButton rbMale, rbFemale, rbOthers;
    private BottomSheetDialog pickerBottomSheetDialog;
    private LinearLayout llAadharCardImage, llDrivingLicenseImage, llElectricityBillImage;
    private TextView tvSaveChanges, tvAadharCardImage, tvDrivingLicenseImage, tvElectricityBillImage;
    private ImageView ivBackArrow, ivProfilePic, ivAadharCardImage, ivDrivingLicenseImage, ivElectricityBillImage;
    private EditText etFirstName, etMiddleName, etLastName, etEmailId, etMobile, etAddress, etAadharNumber, etDrivingLicenseNumber;

    private ArrayList<String> listOfPhotoPaths;

    private String token = "";
    private String selectedGender = "";
    private String selectedImageName = "";
    private String selectedProfileImageUrl = "";
    private String selectedAadharNumberImageUrl = "";
    private String selectedElectricityNumberImageUrl = "";
    private String selectedDrivingLicenseNumberImageUrl = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_edit_profile_details);
        initializeUi();
        initializeListeners();
        prepareProfileDetails();
    }

    private void initializeUi() {
        rbMale = findViewById(R.id.rbMale);
        rbFemale = findViewById(R.id.rbFemale);
        rbOthers = findViewById(R.id.rbOthers);
        etMobile = findViewById(R.id.etMobile);
        etEmailId = findViewById(R.id.etEmailId);
        etAddress = findViewById(R.id.etAddress);
        etLastName = findViewById(R.id.etLastName);
        radioGroup = findViewById(R.id.radioGroup);
        etFirstName = findViewById(R.id.etFirstName);
        ivBackArrow = findViewById(R.id.ivBackArrow);
        etMiddleName = findViewById(R.id.etMiddleName);
        ivProfilePic = findViewById(R.id.ivProfilePic);
        tvSaveChanges = findViewById(R.id.tvSaveChanges);
        llSaveChanges = findViewById(R.id.llSaveChanges);
        etAadharNumber = findViewById(R.id.etAadharNumber);
        etDrivingLicenseNumber = findViewById(R.id.etDrivingLicenseNumber);

        llAadharCardImage = findViewById(R.id.llAadharCardImage);
        tvAadharCardImage = findViewById(R.id.tvAadharCardImage);
        ivAadharCardImage = findViewById(R.id.ivAadharCardImage);
        llDrivingLicenseImage = findViewById(R.id.llDrivingLicenseImage);
        tvDrivingLicenseImage = findViewById(R.id.tvDrivingLicenseImage);
        ivDrivingLicenseImage = findViewById(R.id.ivDrivingLicenseImage);
        llElectricityBillImage = findViewById(R.id.llElectricityBillImage);
        tvElectricityBillImage = findViewById(R.id.tvElectricityBillImage);
        ivElectricityBillImage = findViewById(R.id.ivElectricityBillImage);

        listOfPhotoPaths = new ArrayList<>();
    }

    private void initializeListeners() {
        ivBackArrow.setOnClickListener(this);
        ivProfilePic.setOnClickListener(this);
        tvSaveChanges.setOnClickListener(this);
        llSaveChanges.setOnClickListener(this);
        radioGroup.setOnCheckedChangeListener(this);
        llAadharCardImage.setOnClickListener(this);
        tvAadharCardImage.setOnClickListener(this);
        ivAadharCardImage.setOnClickListener(this);
        llDrivingLicenseImage.setOnClickListener(this);
        tvDrivingLicenseImage.setOnClickListener(this);
        ivDrivingLicenseImage.setOnClickListener(this);
        llElectricityBillImage.setOnClickListener(this);
        tvElectricityBillImage.setOnClickListener(this);
        ivElectricityBillImage.setOnClickListener(this);
    }

    private void prepareProfileDetails() {
        showProgressBar(this);
        token = PreferenceConnector.readString(this, getString(R.string.user_token), "");
        GetProfileDetailsApiCall.serviceCallToGetProfileDetails(this, null, null, token);
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id) {
            case R.id.ivBackArrow:
                onBackPressed();
                break;
            case R.id.ivProfilePic:
                selectedImageName = getString(R.string.profile_image);
                prepareImageDetails();
                break;
            case R.id.llAadharCardImage:
            case R.id.tvAadharCardImage:
            case R.id.ivAadharCardImage:
                selectedImageName = getString(R.string.aadhar_number);
                prepareImageDetails();
                break;
            case R.id.llDrivingLicenseImage:
            case R.id.tvDrivingLicenseImage:
            case R.id.ivDrivingLicenseImage:
                selectedImageName = getString(R.string.driving_license_number);
                prepareImageDetails();
                break;
            case R.id.llElectricityBillImage:
            case R.id.tvElectricityBillImage:
            case R.id.ivElectricityBillImage:
                selectedImageName = getString(R.string.electricity_bill);
                prepareImageDetails();
                break;
            case R.id.tvImagePicker:
                listOfPhotoPaths = new ArrayList<>();
                onPickPhoto();
                closeBottomSheetView();
                break;
            case R.id.tvCamera:
                listOfPhotoPaths = new ArrayList<>();
                captureImage();
                closeBottomSheetView();
                break;
            case R.id.tvCancel:
                closeBottomSheetView();
                break;
            case R.id.llSaveChanges:
            case R.id.tvSaveChanges:
                prepareSaveChanges();
                break;
            default:
                break;
        }
    }

    private void prepareImageDetails() {
        if (AppPermissions.checkPermissionForAccessExternalStorage(this)) {
            if (AppPermissions.checkPermissionForCamera(this)) {
                showBottomSheetView();
            } else {
                AppPermissions.requestPermissionForCamera(this);
            }
        } else {
            AppPermissions.requestPermissionForAccessExternalStorage(this);
        }
    }


    @SuppressLint("InflateParams")
    private void showBottomSheetView() {
        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        assert inflater != null;
        View view = inflater.inflate(R.layout.layout_image_picker_sheet, null);

        TextView tvCancel = view.findViewById(R.id.tvCancel);
        TextView tvCamera = view.findViewById(R.id.tvCamera);
        TextView tvImagePicker = view.findViewById(R.id.tvImagePicker);

        tvCancel.setOnClickListener(this);
        tvCamera.setOnClickListener(this);
        tvImagePicker.setOnClickListener(this);

        pickerBottomSheetDialog = new BottomSheetDialog(this, R.style.BottomSheetDialog);
        pickerBottomSheetDialog.setContentView(view);
        pickerBottomSheetDialog.setCanceledOnTouchOutside(true);
        pickerBottomSheetDialog.show();
    }

    private void closeBottomSheetView() {
        if (pickerBottomSheetDialog != null) {
            pickerBottomSheetDialog.cancel();
        }
    }

    private void onPickPhoto() {
        FilePickerBuilder.getInstance()
                .setMaxCount(1)
                .setSelectedFiles(new ArrayList<>())
                .setActivityTheme(R.style.FilePickerTheme)
                .setActivityTitle(getString(R.string.select_image))
                .enableVideoPicker(false)
                .enableCameraSupport(true)
                .showGifs(true)
                .showFolderView(true)
                .enableSelectAll(false)
                .enableImagePicker(true)
                .setCameraPlaceholder(R.drawable.image_placeholder)
                .withOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED)
                .pickPhoto(this, Constants.PICK_GALLERY);
    }

    private void captureImage() {
        ImagePicker.cameraOnly().start(this, Constants.REQUEST_CODE_CAPTURE_IMAGE);
    }

    private void prepareSaveChanges() {
        showProgressBar(this);
        String firstName = etFirstName.getText().toString();
        String middleName = etMiddleName.getText().toString();
        String lastName = etLastName.getText().toString();
        String emailId = etEmailId.getText().toString();
        String mobile = etMobile.getText().toString();
        String address = etAddress.getText().toString();
        String aadharNumber = etAadharNumber.getText().toString();
        String drivingLicenseNumber = etDrivingLicenseNumber.getText().toString();
        String profileImageBase64 = base64Converter(ivProfilePic);
        String aadharNumberBase64 = base64Converter(ivAadharCardImage);
        String drivingLicenseBase64 = base64Converter(ivDrivingLicenseImage);
        String electricityBillBase64 = base64Converter(ivElectricityBillImage);
        if (!firstName.isEmpty()) {
            if (!middleName.isEmpty()) {
                if (!lastName.isEmpty()) {
                    if (!emailId.isEmpty()) {
                        if (!mobile.isEmpty()) {
                            if (selectedGender != null) {
                                if (!selectedGender.isEmpty()) {
                                    if (!address.isEmpty()) {
                                        if (!aadharNumber.isEmpty()) {
                                            if (!drivingLicenseNumber.isEmpty()) {
                                                if (!profileImageBase64.isEmpty()) {
                                                    if (!aadharNumberBase64.isEmpty()) {
                                                        if (!drivingLicenseBase64.isEmpty()) {
                                                            if (!electricityBillBase64.isEmpty()) {
                                                                UpdateEditProfileDetailsApiCall.serviceCallToUpdateEditProfileDetails(this, firstName, middleName, lastName, emailId, mobile, selectedGender, address,
                                                                        aadharNumber, drivingLicenseNumber, profileImageBase64, aadharNumberBase64, drivingLicenseBase64, electricityBillBase64, token);
                                                            } else {
                                                                closeProgressbar();
                                                                Toast.makeText(this, getString(R.string.please_select_electricity_bill_image), Toast.LENGTH_SHORT).show();
                                                            }
                                                        } else {
                                                            closeProgressbar();
                                                            Toast.makeText(this, getString(R.string.please_select_driving_license_image), Toast.LENGTH_SHORT).show();
                                                        }
                                                    } else {
                                                        closeProgressbar();
                                                        Toast.makeText(this, getString(R.string.please_select_aadhar_image), Toast.LENGTH_SHORT).show();
                                                    }
                                                } else {
                                                    closeProgressbar();
                                                    Toast.makeText(this, getString(R.string.please_select_profile_image), Toast.LENGTH_SHORT).show();
                                                }
                                            } else {
                                                closeProgressbar();
                                                Toast.makeText(this, getString(R.string.please_enter_driving_license_number), Toast.LENGTH_SHORT).show();
                                            }
                                        } else {
                                            closeProgressbar();
                                            Toast.makeText(this, getString(R.string.please_enter_aadhar_number), Toast.LENGTH_SHORT).show();
                                        }
                                    } else {
                                        closeProgressbar();
                                        Toast.makeText(this, getString(R.string.please_enter_address), Toast.LENGTH_SHORT).show();
                                    }
                                } else {
                                    closeProgressbar();
                                    Toast.makeText(this, getString(R.string.please_select_gender), Toast.LENGTH_SHORT).show();
                                }
                            } else {
                                closeProgressbar();
                                Toast.makeText(this, getString(R.string.please_select_gender), Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            closeProgressbar();
                            Toast.makeText(this, getString(R.string.please_enter_mobile_number), Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        closeProgressbar();
                        Toast.makeText(this, getString(R.string.please_enter_email_id), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    closeProgressbar();
                    Toast.makeText(this, getString(R.string.please_enter_last_name), Toast.LENGTH_SHORT).show();
                }
            } else {
                closeProgressbar();
                Toast.makeText(this, getString(R.string.please_enter_middle_name), Toast.LENGTH_SHORT).show();
            }
        } else {
            closeProgressbar();
            Toast.makeText(this, getString(R.string.please_enter_first_name), Toast.LENGTH_SHORT).show();
        }
    }

    private String base64Converter(ImageView imageView) {
        imageView.buildDrawingCache();
        Bitmap bitmap = imageView.getDrawingCache();
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 90, stream);
        byte[] image = stream.toByteArray();
        return Base64.encodeToString(image, 0);
    }

    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {
        RadioButton radioButton = radioGroup.findViewById(checkedId);
        selectedGender = radioButton.getText().toString();
    }

    @Override
    public void jsonResponseReceived(String jsonResponse, int statusCode, int requestType) {
        switch (requestType) {
            case Constants.SERVICE_CALL_TO_GET_PROFILE_DETAILS:
                if (jsonResponse != null) {
                    ProfileResponse profileResponse = new Gson().fromJson(jsonResponse, ProfileResponse.class);
                    if (profileResponse != null) {
                        boolean status = profileResponse.getStatus();
                        String message = profileResponse.getMessage();
                        if (status) {
                            GetProfileData getProfileData = profileResponse.getGetProfileData();
                            if (getProfileData != null) {
                                UserDetails userDetails = getProfileData.getUserDetails();
                                if (userDetails != null) {
                                    prepareUserDetails(userDetails);
                                }
                            }
                        } else {
                            Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
                        }
                    }
                }
                closeProgressbar();
                break;
            case Constants.SERVICE_CALL_FOR_UPDATE_PROFILE:
                if (jsonResponse != null) {
                    UpdateEditProfileDetailsResponse updateEditProfileDetailsResponse = new Gson().fromJson(jsonResponse, UpdateEditProfileDetailsResponse.class);
                    if (updateEditProfileDetailsResponse != null) {
                        boolean status = updateEditProfileDetailsResponse.getStatus();
                        String message = updateEditProfileDetailsResponse.getMessage();
                        if (status) {
                            PreferenceConnector.writeBoolean(this, getString(R.string.refresh_profile), true);
                            finish();
                        } else {
                            Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
                        }
                    }
                }
                closeProgressbar();
                break;
            default:
                break;
        }
    }

    private void prepareUserDetails(UserDetails userDetails) {
        String name = userDetails.getFirstName();
        String lastName = userDetails.getLastName();
        Long mobile = userDetails.getMobile();
        String emailId = userDetails.getEmail();
        selectedGender = userDetails.getGender();
        String profileImageUrl = userDetails.getProfileImage();
        String aadharCardImageUrl = userDetails.getAadharCardImage();
        String drivingLicenseImageUrl = userDetails.getDrivingLicenseImage();
        String electricityBillImageUrl = userDetails.getElectricityBillImage();

        if (name != null) {
            if (!name.isEmpty()) {
                etFirstName.setText(name);
            }
        }

        if (lastName != null) {
            if (!lastName.isEmpty()) {
                etLastName.setText(lastName);
            }
        }

        if (mobile != null) {
            etMobile.setText(String.valueOf(mobile));
        }

        if (emailId != null) {
            if (!emailId.isEmpty()) {
                etEmailId.setText(emailId);
            }
        }

        if (profileImageUrl != null) {
            if (!profileImageUrl.isEmpty()) {
                Glide.with(this)
                        .load(profileImageUrl)
                        .diskCacheStrategy(DiskCacheStrategy.NONE)
                        .skipMemoryCache(true)
                        .placeholder(R.drawable.ic_user_image)
                        .error(R.drawable.ic_user_image)
                        .into(ivProfilePic);
            }
        }

        if (aadharCardImageUrl != null) {
            if (!aadharCardImageUrl.isEmpty()) {
                Glide.with(this)
                        .load(aadharCardImageUrl)
                        .diskCacheStrategy(DiskCacheStrategy.NONE)
                        .skipMemoryCache(true)
                        .placeholder(R.drawable.ic_user_image)
                        .error(R.drawable.ic_user_image)
                        .into(ivAadharCardImage);
            }
        }

        if (drivingLicenseImageUrl != null) {
            if (!drivingLicenseImageUrl.isEmpty()) {
                Glide.with(this)
                        .load(drivingLicenseImageUrl)
                        .diskCacheStrategy(DiskCacheStrategy.NONE)
                        .skipMemoryCache(true)
                        .placeholder(R.drawable.ic_user_image)
                        .error(R.drawable.ic_user_image)
                        .into(ivDrivingLicenseImage);
            }
        }

        if (electricityBillImageUrl != null) {
            if (!electricityBillImageUrl.isEmpty()) {
                Glide.with(this)
                        .load(electricityBillImageUrl)
                        .diskCacheStrategy(DiskCacheStrategy.NONE)
                        .skipMemoryCache(true)
                        .placeholder(R.drawable.ic_user_image)
                        .error(R.drawable.ic_user_image)
                        .into(ivElectricityBillImage);
            }
        }

        if (selectedGender != null) {
            if (selectedGender.equalsIgnoreCase(getString(R.string.male))) {
                rbMale.setChecked(true);
            } else if (selectedGender.equalsIgnoreCase(getString(R.string.female))) {
                rbFemale.setChecked(true);
            } else if (selectedGender.equalsIgnoreCase(getString(R.string.others))) {
                rbOthers.setChecked(true);
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent resultIntent) {
        super.onActivityResult(requestCode, resultCode, resultIntent);
        switch (requestCode) {
            case Constants.PICK_GALLERY:
                listOfPhotoPaths = new ArrayList<>();
                if (resultIntent != null) {
                    if (resultCode == Activity.RESULT_OK) {
                        listOfPhotoPaths.addAll(resultIntent.getStringArrayListExtra(FilePickerConst.KEY_SELECTED_MEDIA));
                        if (listOfPhotoPaths != null) {
                            if (listOfPhotoPaths.size() != 0) {
                                String selectedImageUrl = listOfPhotoPaths.get(0);
                                if (selectedImageName.equalsIgnoreCase(getString(R.string.profile_image))) {
                                    selectedProfileImageUrl = selectedImageUrl;
                                    Glide.with(this)
                                            .load(new File(selectedProfileImageUrl))
                                            .diskCacheStrategy(DiskCacheStrategy.NONE)
                                            .skipMemoryCache(true)
                                            .placeholder(R.drawable.image_placeholder)
                                            .error(R.drawable.image_placeholder)
                                            .into(ivProfilePic);
                                } else if (selectedImageName.equalsIgnoreCase(getString(R.string.aadhar_number))) {
                                    selectedAadharNumberImageUrl = selectedImageUrl;
                                    Glide.with(this)
                                            .load(new File(selectedAadharNumberImageUrl))
                                            .diskCacheStrategy(DiskCacheStrategy.NONE)
                                            .skipMemoryCache(true)
                                            .placeholder(R.drawable.image_placeholder)
                                            .error(R.drawable.image_placeholder)
                                            .into(ivAadharCardImage);
                                } else if (selectedImageName.equalsIgnoreCase(getString(R.string.driving_license_number))) {
                                    selectedDrivingLicenseNumberImageUrl = selectedImageUrl;
                                    Glide.with(this)
                                            .load(new File(selectedDrivingLicenseNumberImageUrl))
                                            .diskCacheStrategy(DiskCacheStrategy.NONE)
                                            .skipMemoryCache(true)
                                            .placeholder(R.drawable.image_placeholder)
                                            .error(R.drawable.image_placeholder)
                                            .into(ivDrivingLicenseImage);
                                } else if (selectedImageName.equalsIgnoreCase(getString(R.string.electricity_bill))) {
                                    selectedElectricityNumberImageUrl = selectedImageUrl;
                                    Glide.with(this)
                                            .load(new File(selectedElectricityNumberImageUrl))
                                            .diskCacheStrategy(DiskCacheStrategy.NONE)
                                            .skipMemoryCache(true)
                                            .placeholder(R.drawable.image_placeholder)
                                            .error(R.drawable.image_placeholder)
                                            .into(ivElectricityBillImage);
                                }
                            }
                        }
                    }
                }
                break;
            case Constants.REQUEST_CODE_CAPTURE_IMAGE:
                listOfPhotoPaths = new ArrayList<>();
                List<Image> listOfImages = ImagePicker.getImages(resultIntent);
                if (listOfImages != null) {
                    if (listOfImages.size() != 0) {
                        String selectedImageUrl = listOfImages.get(0).getPath();
                        listOfPhotoPaths.add(selectedImageUrl);

                        if (selectedImageName.equalsIgnoreCase(getString(R.string.profile_image))) {
                            selectedProfileImageUrl = selectedImageUrl;
                            Glide.with(this)
                                    .load(new File(selectedProfileImageUrl))
                                    .diskCacheStrategy(DiskCacheStrategy.NONE)
                                    .skipMemoryCache(true)
                                    .placeholder(R.drawable.image_placeholder)
                                    .error(R.drawable.image_placeholder)
                                    .into(ivProfilePic);
                        } else if (selectedImageName.equalsIgnoreCase(getString(R.string.aadhar_number))) {
                            selectedAadharNumberImageUrl = selectedImageUrl;
                            Glide.with(this)
                                    .load(new File(selectedAadharNumberImageUrl))
                                    .diskCacheStrategy(DiskCacheStrategy.NONE)
                                    .skipMemoryCache(true)
                                    .placeholder(R.drawable.image_placeholder)
                                    .error(R.drawable.image_placeholder)
                                    .into(ivAadharCardImage);
                        } else if (selectedImageName.equalsIgnoreCase(getString(R.string.driving_license_number))) {
                            selectedDrivingLicenseNumberImageUrl = selectedImageUrl;
                            Glide.with(this)
                                    .load(new File(selectedDrivingLicenseNumberImageUrl))
                                    .diskCacheStrategy(DiskCacheStrategy.NONE)
                                    .skipMemoryCache(true)
                                    .placeholder(R.drawable.image_placeholder)
                                    .error(R.drawable.image_placeholder)
                                    .into(ivDrivingLicenseImage);
                        } else if (selectedImageName.equalsIgnoreCase(getString(R.string.electricity_bill))) {
                            selectedElectricityNumberImageUrl = selectedImageUrl;
                            Glide.with(this)
                                    .load(new File(selectedElectricityNumberImageUrl))
                                    .diskCacheStrategy(DiskCacheStrategy.NONE)
                                    .skipMemoryCache(true)
                                    .placeholder(R.drawable.image_placeholder)
                                    .error(R.drawable.image_placeholder)
                                    .into(ivElectricityBillImage);
                        }
                    }
                }
                break;
            default:
                break;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (permissions.length != 0 && grantResults.length != 0) {
            switch (requestCode) {
                case Constants.REQUEST_CODE_FOR_CAMERA_PERMISSION:
                    if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                        prepareImageDetails();
                    } else {
                        prepareImageDetails();
                    }
                    break;
                case Constants.REQUEST_CODE_FOR_EXTERNAL_STORAGE_PERMISSION:
                    if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                        prepareImageDetails();
                    } else {
                        prepareImageDetails();
                    }
                    break;
                default:
                    super.onRequestPermissionsResult(requestCode, permissions, grantResults);
            }
        }
    }
}
