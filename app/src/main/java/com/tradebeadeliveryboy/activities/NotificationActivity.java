package com.tradebeadeliveryboy.activities;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.gson.Gson;
import com.tradebeadeliveryboy.ApiCalls.GetOrdersApiCall;
import com.tradebeadeliveryboy.ApiCalls.NotificationsApiCall;
import com.tradebeadeliveryboy.R;
import com.tradebeadeliveryboy.adapters.NotificationAdapter;
import com.tradebeadeliveryboy.fragments.OrdersFragment;
import com.tradebeadeliveryboy.interfaces.HttpReqResCallBack;
import com.tradebeadeliveryboy.interfaces.LocationUpdateCallBack;
import com.tradebeadeliveryboy.models.responseModels.notificationsResponse.NotificationData;
import com.tradebeadeliveryboy.models.responseModels.notificationsResponse.NotificationsResponse;
import com.tradebeadeliveryboy.utils.AppPermissions;
import com.tradebeadeliveryboy.utils.Constants;
import com.tradebeadeliveryboy.utils.GetCurrentLocation;
import com.tradebeadeliveryboy.utils.Utils;

import java.util.LinkedList;

public class NotificationActivity extends BaseActivity implements View.OnClickListener, LocationUpdateCallBack, HttpReqResCallBack {

    private TextView tvError;
    private ImageView ivBackArrow;
    private RecyclerView rvNotifications;
    private SwipeRefreshLayout swipeRefreshLayout;

    private LinkedList<NotificationData> listOfNotificationData;

    private double latitude = 0.0;
    private double longitude = 0.0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_notification);
        initializeUi();
        initializeListeners();
        prepareDetails();
        refreshContent();
    }

    private void initializeUi() {
        tvError = findViewById(R.id.tvError);
        ivBackArrow = findViewById(R.id.ivBackArrow);
        rvNotifications = findViewById(R.id.rvNotifications);
        swipeRefreshLayout = findViewById(R.id.swipeRefreshLayout);
    }

    private void initializeListeners() {
        ivBackArrow.setOnClickListener(this);
    }

    private void prepareDetails() {
        //checkLocationAccessPermission();
        showProgressBar(this);
        NotificationsApiCall.serviceCallToGetNotifications(this, 0.0, 0.0, "");
    }

    private void refreshContent() {
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeRefreshLayout.setRefreshing(false);
                prepareDetails();
            }
        });
    }

    private void checkLocationAccessPermission() {
        if (AppPermissions.checkPermissionForAccessLocation(this)) {
            showProgressBar(this);
            LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
            new GetCurrentLocation(this, null, locationManager);
        } else {
            AppPermissions.requestPermissionForLocation(this);
        }
    }

    @SuppressLint("NonConstantResourceId")
    @Override
    public void onClick(View view) {
        int id = view.getId();
        if (id == R.id.ivBackArrow) {
            onBackPressed();
        }
    }

    @Override
    public void OnLatLongReceived(double latitude, double longitude) {
        this.latitude = latitude;
        this.longitude = longitude;
        String address = Utils.geoCode(this, latitude, longitude);
        closeProgressbar();
        showProgressBar(this);
        NotificationsApiCall.serviceCallToGetNotifications(this, latitude, longitude, address);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (permissions.length != 0 && grantResults.length != 0) {
            switch (requestCode) {
                case Constants.REQUEST_CODE_FOR_LOCATION_PERMISSION:
                    if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                        prepareDetails();
                    } else {
                        prepareDetails();
                    }
                    break;
                default:
                    super.onRequestPermissionsResult(requestCode, permissions, grantResults);
            }
        }
    }

    @Override
    public void jsonResponseReceived(String jsonResponse, int statusCode, int requestType) {
        switch (requestType) {
            case Constants.SERVICE_CALL_TO_GET_NOTIFICATIONS:
                if (jsonResponse != null) {
                    NotificationsResponse notificationsResponse = new Gson().fromJson(jsonResponse, NotificationsResponse.class);
                    if (notificationsResponse != null) {
                        Boolean status = notificationsResponse.getStatus();
                        String message = notificationsResponse.getMessage();
                        if (status) {
                            listOfNotificationData = notificationsResponse.getListOfNotificationData();
                            if (listOfNotificationData != null) {
                                if (listOfNotificationData.size() != 0) {
                                    listIsFull();
                                    initializeAdapter();
                                } else {
                                    listIsEmpty();
                                }
                            } else {
                                listIsEmpty();
                            }
                        } else {
                            Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
                        }
                    }
                }
                closeProgressbar();
                break;
            default:
                break;
        }
    }

    private void initializeAdapter() {
        NotificationAdapter notificationAdapter = new NotificationAdapter(this, listOfNotificationData, latitude, longitude);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        rvNotifications.setLayoutManager(layoutManager);
        rvNotifications.setItemAnimator(new DefaultItemAnimator());
        rvNotifications.setAdapter(notificationAdapter);
    }

    private void listIsFull() {
        tvError.setVisibility(View.GONE);
        rvNotifications.setVisibility(View.VISIBLE);
    }

    private void listIsEmpty() {
        tvError.setVisibility(View.VISIBLE);
        rvNotifications.setVisibility(View.GONE);
    }
}
