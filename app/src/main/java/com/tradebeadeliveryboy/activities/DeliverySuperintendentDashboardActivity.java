package com.tradebeadeliveryboy.activities;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.gson.Gson;
import com.tradebeadeliveryboy.ApiCalls.DeliverySuperintendentDashboardApiCall;
import com.tradebeadeliveryboy.R;
import com.tradebeadeliveryboy.interfaces.HttpReqResCallBack;
import com.tradebeadeliveryboy.models.responseModels.dashboardDetailsResponse.DashboardData;
import com.tradebeadeliveryboy.models.responseModels.dashboardDetailsResponse.DashboardDetailsResponse;
import com.tradebeadeliveryboy.models.responseModels.dashboardDetailsResponse.DashboardLocation;
import com.tradebeadeliveryboy.models.responseModels.dashboardDetailsResponse.DashboardOnGoingOrder;
import com.tradebeadeliveryboy.models.responseModels.orderDetailsResponse.DeliveryJobs;
import com.tradebeadeliveryboy.utils.Constants;
import com.tradebeadeliveryboy.utils.PreferenceConnector;

import java.util.LinkedList;

public class DeliverySuperintendentDashboardActivity extends BaseActivity implements View.OnClickListener, HttpReqResCallBack {

    private ImageView ivBackArrow;
    private LinearLayout llOnGoingOrders;
    private SwipeRefreshLayout swipeRefreshLayout;
    private TextView tvActionBarTitle, tvOrderDate, tvOrderId, tvOrderStatus, tvPreparationTime, tvTravelDistance,
            tvTodayEarnings, tvFloatingCash, tvTodayDeliveries, tvTotalDeliveries;

    private LinkedList<DeliveryJobs> listOfDeliveryJobs;
    private String id = "";
    private int orderId = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_delivery_superintendent_dashboard);
        getDataFromIntent();
        initializeUi();
        setActionBarTitle();
        initializeListeners();
        setDefaultDetails();
        prepareDetails();
        refreshContent();
    }

    private void getDataFromIntent() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            if (bundle.containsKey(getString(R.string.id))) {
                id = bundle.getString(getString(R.string.id));
            }
        }
    }

    private void initializeUi() {
        ivBackArrow = findViewById(R.id.ivBackArrow);
        tvActionBarTitle = findViewById(R.id.tvActionBarTitle);
        tvTodayEarnings = findViewById(R.id.tvTodayEarnings);
        tvFloatingCash = findViewById(R.id.tvFloatingCash);
        tvTodayDeliveries = findViewById(R.id.tvTodayDeliveries);
        tvTotalDeliveries = findViewById(R.id.tvTotalDeliveries);

        tvOrderId = findViewById(R.id.tvOrderId);
        tvOrderDate = findViewById(R.id.tvOrderDate);
        tvOrderStatus = findViewById(R.id.tvOrderStatus);
        llOnGoingOrders = findViewById(R.id.llOnGoingOrders);
        tvTravelDistance = findViewById(R.id.tvTravelDistance);
        tvPreparationTime = findViewById(R.id.tvPreparationTime);
        swipeRefreshLayout = findViewById(R.id.swipeRefreshLayout);
    }

    private void setActionBarTitle() {
        tvActionBarTitle.setText(getString(R.string.dashboard));
    }

    private void initializeListeners() {
        ivBackArrow.setOnClickListener(this);
    }

    @SuppressLint("SetTextI18n")
    private void setDefaultDetails() {
        tvTodayEarnings.setText(getString(R.string.ruppee) + " " + "0" + " /-");
        tvFloatingCash.setText(getString(R.string.ruppee) + " " + "0" + " /-");
        tvTodayDeliveries.setText("0");
        tvTotalDeliveries.setText("0");
    }

    private void refreshContent() {
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeRefreshLayout.setRefreshing(false);
                prepareDetails();
            }
        });
    }

    private void prepareDetails() {
        showProgressBar(this);
        String token = PreferenceConnector.readString(this, getString(R.string.user_token), "");
        DeliverySuperintendentDashboardApiCall.serviceCallToGetDeliverySuperintendentDashboard(this, null, null, token, id);
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        if (id == R.id.ivBackArrow) {
            onBackPressed();
        }
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void jsonResponseReceived(String jsonResponse, int statusCode, int requestType) {
        if (requestType == Constants.SERVICE_CALL_TO_GET_DELIVERY_SUPERINTENDENT_DASHBOARD) {
            if (jsonResponse != null) {
                DashboardDetailsResponse dashboardDetailsResponse = new Gson().fromJson(jsonResponse, DashboardDetailsResponse.class);
                if (dashboardDetailsResponse != null) {
                    Boolean status = dashboardDetailsResponse.getStatus();
                    String message = dashboardDetailsResponse.getMessage();
                    if (status) {
                        DashboardData dashboardData = dashboardDetailsResponse.getDashboardData();
                        if (dashboardData != null) {
                            int todayEarnings = 0;
                            int todayFloatingCash = 0;
                            int todayDeliveries = 0;
                            int totalDeliveries = 0;

                            DashboardOnGoingOrder dashboardOnGoingOrder = dashboardData.getDashboardOnGoingOrder();
                            if (dashboardOnGoingOrder != null) {
                                llOnGoingOrders.setVisibility(View.VISIBLE);
                                String date = dashboardOnGoingOrder.getDate();
                                int preparationTime = dashboardOnGoingOrder.getPreparationTime();
                                String trackId = dashboardOnGoingOrder.getTrackId();
                                orderId = dashboardOnGoingOrder.getId();
                                listOfDeliveryJobs = dashboardOnGoingOrder.getListOfDeliveryJobs();
                                String orderStatus = dashboardOnGoingOrder.getOrderStatus().getCurrentStatus();
                                DashboardLocation dashboardLocation = dashboardOnGoingOrder.getDashboardSellerLatLong().getDashboardLocation();

                                tvOrderDate.setText("Date :" + " " + date);
                                tvOrderId.setText("Track Id : " + "" + trackId);
                                tvPreparationTime.setText("Prep Time :" + " " + preparationTime + "mins");
                                tvOrderStatus.setText(orderStatus);
                                tvTravelDistance.setText("Travel Dst : 0.90KMS");

                                if (listOfDeliveryJobs != null) {
                                    if (listOfDeliveryJobs.size() != 0) {
                                        if (listOfDeliveryJobs.size() == 2) {
                                            tvPreparationTime.setVisibility(View.GONE);
                                        } else {
                                            tvPreparationTime.setVisibility(View.VISIBLE);
                                        }
                                    }
                                }

                                if (dashboardLocation != null) {
                                    Double latitude = dashboardLocation.getLatitude();
                                    Double longitude = dashboardLocation.getLongitude();
                                }
                            } else {
                                llOnGoingOrders.setVisibility(View.GONE);
                            }

                            if (dashboardData.getTodayEarnings() != null)
                                todayEarnings = dashboardData.getTodayEarnings();
                            if (dashboardData.getTodayFloatingCash() != null)
                                todayFloatingCash = dashboardData.getTodayFloatingCash();
                            if (dashboardData.getTotalDeliveries() != null)
                                todayDeliveries = dashboardData.getTodayDeliveries();
                            if (dashboardData.getTotalDeliveries() != null)
                                totalDeliveries = dashboardData.getTotalDeliveries();

                            tvTodayEarnings.setText(getString(R.string.ruppee) + " " + todayEarnings + " /-");
                            tvFloatingCash.setText(getString(R.string.ruppee) + " " + todayFloatingCash + " /-");
                            tvTodayDeliveries.setText(String.valueOf(todayDeliveries));
                            tvTotalDeliveries.setText(String.valueOf(totalDeliveries));
                        }
                    }
                }
            }
            closeProgressbar();
        }
    }
}
