package com.tradebeadeliveryboy.activities;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.core.content.ContextCompat;

import com.hbb20.CountryCodePicker;
import com.tradebeadeliveryboy.R;

public class LoginByOTPActivity extends BaseActivity implements View.OnClickListener {

    private EditText etPhoneNumber;
    private CountryCodePicker countryCodePicker;
    private ImageView ivBackArrow, ivGmail, ivFacebook;
    private TextView tvContinue, tvSignUp, tvLoginByPassword;
    private LinearLayout llContinue, llLoginByPassword, llSignUp;

    private boolean isValidMobileNumber;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_login_by_otp);
        setStatusBarColor();
        initializeUi();
        initializeListeners();
        prepareCountryCodePickerDetails();
    }

    private void setStatusBarColor() {
        Window window = getWindow();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.setStatusBarColor(ContextCompat.getColor(this, R.color.white));
        }
    }

    private void initializeUi() {
        ivGmail = findViewById(R.id.ivGmail);
        tvSignUp = findViewById(R.id.tvSignUp);
        llSignUp = findViewById(R.id.llSignUp);
        ivFacebook = findViewById(R.id.ivFacebook);
        tvContinue = findViewById(R.id.tvContinue);
        llContinue = findViewById(R.id.llContinue);
        ivBackArrow = findViewById(R.id.ivBackArrow);
        etPhoneNumber = findViewById(R.id.etPhoneNumber);
        tvLoginByPassword = findViewById(R.id.tvLoginByPassword);
        llLoginByPassword = findViewById(R.id.llLoginByPassword);
        countryCodePicker = findViewById(R.id.countryCodePicker);
    }

    private void initializeListeners() {
        ivGmail.setOnClickListener(this);
        tvSignUp.setOnClickListener(this);
        llSignUp.setOnClickListener(this);
        tvContinue.setOnClickListener(this);
        ivFacebook.setOnClickListener(this);
        llContinue.setOnClickListener(this);
        ivBackArrow.setOnClickListener(this);
        tvLoginByPassword.setOnClickListener(this);
        llLoginByPassword.setOnClickListener(this);
    }

    private void prepareCountryCodePickerDetails() {
        countryCodePicker.registerCarrierNumberEditText(etPhoneNumber);
        countryCodePicker.setPhoneNumberValidityChangeListener(new CountryCodePicker.PhoneNumberValidityChangeListener() {
            @Override
            public void onValidityChanged(boolean isValidNumber) {
                isValidMobileNumber = isValidNumber;
                if (isValidNumber) {
                    etPhoneNumber.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_valid, 0);
                } else {
                    etPhoneNumber.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                }
            }
        });
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id) {
            case R.id.ivBackArrow:
                onBackPressed();
                break;
            case R.id.ivGmail:
                prepareGmailDetails();
                break;
            case R.id.ivFacebook:
                prepareFacebookDetails();
                break;
            case R.id.llSignUp:
            case R.id.tvSignUp:
                prepareSignUpDetails();
                break;
            case R.id.llContinue:
            case R.id.tvContinue:
                prepareContinueDetails();
                break;
            case R.id.llLoginByPassword:
            case R.id.tvLoginByPassword:
                prepareLoginByPassword();
                break;
            default:
                break;
        }
    }

    private void prepareGmailDetails() {

    }

    private void prepareFacebookDetails() {

    }

    private void prepareContinueDetails() {
        String phoneNumber = etPhoneNumber.getText().toString();
        if (!phoneNumber.isEmpty()) {
            if (isValidMobileNumber) {
                Intent optVerificationIntent = new Intent(this, OTPVerificationActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString(getString(R.string.mobile_number), phoneNumber);
                optVerificationIntent.putExtras(bundle);
                startActivity(optVerificationIntent);
            } else {
                Toast.makeText(this, getString(R.string.please_enter_valid_phone_number), Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(this, getString(R.string.please_enter_phone_number), Toast.LENGTH_SHORT).show();
        }
    }

    private void prepareSignUpDetails() {
        Intent signUpIntent = new Intent(this, SignUpActivity.class);
        startActivity(signUpIntent);
    }

    private void prepareLoginByPassword() {
        Intent loginIntent = new Intent(this, LoginActivity.class);
        loginIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(loginIntent);
    }
}
