package com.tradebeadeliveryboy.activities;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.mukesh.OtpView;
import com.tradebeadeliveryboy.ApiCalls.VerifyPickUpOTPApiCall;
import com.tradebeadeliveryboy.R;
import com.tradebeadeliveryboy.interfaces.HttpReqResCallBack;
import com.tradebeadeliveryboy.utils.Constants;

public class PickUpOTPActivity extends BaseActivity implements View.OnClickListener, HttpReqResCallBack {

    private OtpView otpView;
    private ImageView ivBackArrow;
    private LinearLayout llSubmit, tvSubmit;

    private String selectedOrderId = "";

    private int deliveryJobId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_pickup_otp);
        getDataFromIntent();
        initializeUi();
        initializeListeners();
    }

    private void getDataFromIntent() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            if (bundle.containsKey(getString(R.string.order_id)))
                selectedOrderId = bundle.getString(getString(R.string.order_id), "");
            if (bundle.containsKey(getString(R.string.delivery_job_id)))
                deliveryJobId = bundle.getInt(getString(R.string.delivery_job_id));
        }
    }

    private void initializeUi() {
        otpView = findViewById(R.id.otpView);
        llSubmit = findViewById(R.id.llSubmit);
        tvSubmit = findViewById(R.id.tvSubmit);
        ivBackArrow = findViewById(R.id.ivBackArrow);
    }

    private void initializeListeners() {
        llSubmit.setOnClickListener(this);
        tvSubmit.setOnClickListener(this);
        ivBackArrow.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        if (id == R.id.ivBackArrow) {
            onBackPressed();
        } else if (id == R.id.llSubmit) {
            prepareDetails();
        } else if (id == R.id.tvSubmit) {
            prepareDetails();
        }
    }

    private void prepareDetails() {
        String otp = otpView.getText().toString();
        if (!otp.isEmpty()) {
            showProgressBar(this);
            VerifyPickUpOTPApiCall.serviceCallForVerifyPickUpOTP(this, null, null, selectedOrderId, otp, deliveryJobId);
        } else {
            Toast.makeText(this, getString(R.string.please_enter_otp), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void jsonResponseReceived(String jsonResponse, int statusCode, int requestType) {
        switch (requestType) {
            case Constants.SERVICE_CALL_FOR_VERIFY_PICK_UP_OTP:
                if (jsonResponse != null) {
                    Log.d("response****", jsonResponse);
                }
                closeProgressbar();
                break;
            default:
                break;
        }
    }
}
