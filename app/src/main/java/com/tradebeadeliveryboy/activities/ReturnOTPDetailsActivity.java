package com.tradebeadeliveryboy.activities;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.mukesh.OtpView;
import com.tradebeadeliveryboy.ApiCalls.VerifyReturnOTPApiCall;
import com.tradebeadeliveryboy.R;
import com.tradebeadeliveryboy.interfaces.CloseCallBack;
import com.tradebeadeliveryboy.interfaces.HttpReqResCallBack;
import com.tradebeadeliveryboy.models.requestModels.verifyReturnOTPRequest.VerifyReturnOTPRequest;
import com.tradebeadeliveryboy.models.responseModels.verifyReturnOTPResponse.VerifyReturnOTPResponse;
import com.tradebeadeliveryboy.utils.Constants;
import com.tradebeadeliveryboy.utils.UserData;

public class ReturnOTPDetailsActivity extends BaseActivity implements View.OnClickListener, HttpReqResCallBack {

    private OtpView otpView;
    private TextView tvSubmit;
    private ImageView ivBackArrow;
    private LinearLayout llSubmit;

    private int returnDeliveryJobId;
    private int returnRequestId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_return_otp_details);
        getDataFromIntent();
        initializeUi();
        initializeListeners();
    }

    private void getDataFromIntent() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            if (bundle.containsKey(getString(R.string.job_id)))
                returnDeliveryJobId = bundle.getInt(getString(R.string.job_id));
            if (bundle.containsKey(getString(R.string.return_request_id)))
                returnRequestId = bundle.getInt(getString(R.string.return_request_id));
        }
    }

    private void initializeUi() {
        otpView = findViewById(R.id.otpView);
        llSubmit = findViewById(R.id.llSubmit);
        tvSubmit = findViewById(R.id.tvSubmit);
        ivBackArrow = findViewById(R.id.ivBackArrow);
    }

    private void initializeListeners() {
        llSubmit.setOnClickListener(this);
        tvSubmit.setOnClickListener(this);
        ivBackArrow.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        if (id == R.id.llSubmit) {
            prepareSubmitDetails();
        } else if (id == R.id.tvSubmit) {
            prepareSubmitDetails();
        } else if (id == R.id.ivBackArrow) {
            onBackPressed();
        }
    }

    private void prepareSubmitDetails() {
        String otp = otpView.getText().toString();
        if (!otp.isEmpty()) {
            VerifyReturnOTPRequest verifyReturnOTPRequest = new VerifyReturnOTPRequest();
            verifyReturnOTPRequest.setReturnRequestId(returnRequestId);
            verifyReturnOTPRequest.setDeliveryJobId(returnDeliveryJobId);
            verifyReturnOTPRequest.setPickupOtp(otp);
            showProgressBar(this);
            VerifyReturnOTPApiCall.serviceCallForVerifyReturnOTP(this, null, null, verifyReturnOTPRequest);
        } else {
            Toast.makeText(this, getString(R.string.please_enter_otp), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void jsonResponseReceived(String jsonResponse, int statusCode, int requestType) {
        if (requestType == Constants.SERVICE_CALL_FOR_VERIFY_RETURN_OTP) {
            if (jsonResponse != null) {
                VerifyReturnOTPResponse verifyReturnOTPResponse = new Gson().fromJson(jsonResponse, VerifyReturnOTPResponse.class);
                if (verifyReturnOTPResponse != null) {
                    boolean status = verifyReturnOTPResponse.getStatus();
                    String message = verifyReturnOTPResponse.getMessage();
                    if (status) {
                        Context context = UserData.getInstance().getOrderDetailsContext();
                        if (context != null) {
                            CloseCallBack closeCallBack = (CloseCallBack) context;
                            closeCallBack.close();
                        }
                        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
                        finish();
                    } else {
                        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
                    }
                }
            }
            closeProgressbar();
        }
    }
}