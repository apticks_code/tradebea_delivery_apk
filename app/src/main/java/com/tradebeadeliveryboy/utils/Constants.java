package com.tradebeadeliveryboy.utils;

public class Constants {

    public static final String APP_ID = "TXc9PQ==";

    public static final String GROUP_ID = "3";
    public static final int GROUP_ID_INT = 3;

    public static final int SPLASH_TIME_OUT = 3000;
    public static final int TIME_OUT_THIRTY_SECONDS = 30000;

    public static final String BASE_URL = "https://apticks.com/tradebea/";
    public static final String AUTH_TOKEN = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6IjkiLCJ1c2VyZGV0YWlsIjp7InVzZXJuYW1lIjoiODUyMjgwODc4NSIsImVtYWlsIjoibWVoYXJncmVwdGhvckBnbWFpbC5jb20iLCJwaG9uZSI6Ijg1MjI4MDg3ODUiLCJpZCI6IjkiLCJncm91cHMiOnsiMyI6eyJpZCI6IjMiLCJ1c2VyX2lkIjoiOSIsIm5hbWUiOiJkZWxpdmVyeSJ9LCI1Ijp7ImlkIjoiNSIsInVzZXJfaWQiOiI5IiwibmFtZSI6InVzZXIifX19LCJ0aW1lIjoxNjQxNzk0NDg1fQ.vLUBdNx1mo9C1oXcxE6OyxMOiqE7Tgpe_BieDqslu5E";
    //public static final String ORDERS_AUTH_TOKEN = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6IjI3IiwidXNlcmRldGFpbCI6eyJ1c2VybmFtZSI6IlRCVTAwMDkiLCJlbWFpbCI6Im1laGFya290aUBnbWFpbC5jb20iLCJwaG9uZSI6Ijg1MjI4MDg3ODkiLCJpZCI6IjI3IiwiZ3JvdXBzIjp7IjIiOnsiaWQiOiIyIiwidXNlcl9pZCI6IjI3IiwibmFtZSI6InNlbGxlciJ9LCI1Ijp7ImlkIjoiNSIsInVzZXJfaWQiOiIyNyIsIm5hbWUiOiJ1c2VyIn19fSwidGltZSI6MTYzNTY4ODE3Nn0.mvne0EkXHUbWwSRdxE_VQj0VPPiocfoDDH9S30rRhhQ";
    public static final String ORDERS_AUTH_TOKEN = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6IjkiLCJ1c2VyZGV0YWlsIjp7InVzZXJuYW1lIjoiODUyMjgwODc4NSIsImVtYWlsIjoibWVoYXJncmVwdGhvckBnbWFpbC5jb20iLCJwaG9uZSI6Ijg1MjI4MDg3ODUiLCJpZCI6IjkiLCJncm91cHMiOnsiMyI6eyJpZCI6IjMiLCJ1c2VyX2lkIjoiOSIsIm5hbWUiOiJkZWxpdmVyeSJ9LCI1Ijp7ImlkIjoiNSIsInVzZXJfaWQiOiI5IiwibmFtZSI6InVzZXIifX19LCJ0aW1lIjoxNjYyNzg5OTc1fQ.pLBN_BH9pdOnuu-0q-5_Tc9NjBXHOAejWc0Vn2oe2jU";

    public static final String LOGIN_URL = BASE_URL + "auth/api/auth/login";
    public static final String SOCIAL_LOGIN_URL = BASE_URL + "auth/api/auth/social_login/google";
    public static final String GENERATE_OTP_URL = BASE_URL + "auth/api/auth/otp_gen";
    public static final String VERIFY_OTP_URL = BASE_URL + "auth/api/auth/verify_otp";
    public static final String SIGN_UP_URL = BASE_URL + "auth/api/auth/create_user/user";
    public static final String FORGOT_PASSWORD_URL = BASE_URL + "auth/api/auth/forgot_password";
    public static final String GET_PROFILE_DETAILS_URL = BASE_URL + "auth/api/users/profile/r";
    public static final String UPDATE_PROFILE_URL = BASE_URL + "auth/api/users/profile/u/delivery";
    public static final String DASHBOARD_DETAILS_URL = BASE_URL + "ecom/api/delivery/dashboard";
    public static final String GET_ORDERS_URL = BASE_URL + "ecom/api/ecom/orders/delivery_boy_orders";
    public static final String GET_NOTIFICATIONS_URL = BASE_URL + "ecom/api/delivery/notifications/r";
    public static final String DEVICE_TOKEN_URL = BASE_URL + "admin/api/fcm_notify/grant_fcm_permission";
    public static final String ACCEPT_ORDER_URL = BASE_URL + "ecom/api/delivery/notifications/accept";
    public static final String UPDATE_DELIVERY_BOY_SESSION_URL = BASE_URL + "ecom/api/delivery/manage_login_session";
    public static final String ORDER_DETAILS_URL = BASE_URL + "ecom/api/ecom/orders/order_details";
    public static final String REACHED_TO_PICK_UP_POINT_URL = BASE_URL + "ecom/api/ecom/orders/delivery_boy_changes_status";
    public static final String REACHED_TO_DELIVERY_URL = BASE_URL + "ecom/api/ecom/orders/delivery_boy_changes_status";
    public static final String VERIFY_DELIVERY_OTP_URL = BASE_URL + "ecom/api/ecom/orders/verify_delivery_otp";
    public static final String VERIFY_PICK_UP_OTP_URL = BASE_URL + "ecom/api/ecom/orders/verify_pickup_otp";

    public static final String VERIFY_RETURN_OTP_URL = BASE_URL + "ecom/api/returns/verify_pickup_otp";
    public static final String VERIFY_RETURNED_OTP_URL = BASE_URL + "ecom/api/returns/verify_returned_otp";
    public static final String WALLET_BALANCE_URL = BASE_URL + "payment/api/payment/wallet_account";
    public static final String VERIFY_PAYBACK_REQUEST_URL = BASE_URL + "payment/api/delivery/verify_payback_request";
    public static final String PAYMENT_METHOD_REQUEST_URL = BASE_URL + "payment/api/delivery/payback_request";
    public static final String PAYBACK_ONLINE_PAYMENT_REQUEST_URL = BASE_URL + "payment/api/delivery/delivey_paynow";

    public static final String PAYMENT_GATEWAY_CREDENTIALS = BASE_URL + "payment/api/payment/payment_gateway_credentials";

    public static final String MY_DELIVERY_BOY_DETAILS_REQUEST_URL = BASE_URL + "ecom/api/delivery/delivery_boys_under_me";
    public static final String DELIVERY_SUPERINTENDENT_DASHBOARD_REQUEST_URL = BASE_URL + "ecom/api/delivery/dashboard?delivery_boy_id=";

    public static final int REQUEST_CODE_FOR_EXTERNAL_STORAGE_PERMISSION = 1;
    public static final int REQUEST_CODE_FOR_CAMERA_PERMISSION = 2;
    public static final int REQUEST_CODE_FOR_LOCATION_PERMISSION = 3;
    public static final int REQUEST_CODE_FOR_PHONE_PERMISSION = 4;
    public static final int PICK_GALLERY = 5;
    public static final int REQUEST_CODE_CAPTURE_IMAGE = 6;

    // Api calls
    public static final int SERVICE_CALL_FOR_LOGIN = 1;
    public static final int SERVICE_CALL_FOR_GENERATE_OTP = 2;
    public static final int SERVICE_CALL_FOR_VERIFY_OTP = 3;
    public static final int SERVICE_CALL_FOR_SIGN_UP = 4;
    public static final int SERVICE_CALL_FOR_FORGOT_PASSWORD = 5;
    public static final int SERVICE_CALL_TO_GET_PROFILE_DETAILS = 6;
    public static final int SERVICE_CALL_FOR_SOCIAL_LOGIN = 7;
    public static final int SERVICE_CALL_FOR_UPDATE_PROFILE = 8;
    public static final int SERVICE_CALL_TO_GET_DASHBOARD_DETAILS = 9;
    public static final int SERVICE_CALL_TO_GET_ORDERS = 10;
    public static final int SERVICE_CALL_TO_GET_NOTIFICATIONS = 11;
    public static final int SERVICE_CALL_FOR_DEVICE_TOKEN = 12;
    public static final int SERVICE_CALL_FOR_ACCEPT_ORDER = 13;
    public static final int SERVICE_CALL_TO_ACCEPT_ORDER = 14;
    public static final int SERVICE_CALL_FOR_UPDATE_DELIVERY_BOY_SESSION = 15;
    public static final int SERVICE_CALL_TO_GET_ORDER_DETAILS = 16;
    public static final int SERVICE_CALL_FOR_REACHED_TO_PICKUP_POINT = 17;
    public static final int SERVICE_CALL_FOR_REACHED_TO_DELIVERY = 18;
    public static final int SERVICE_CALL_FOR_VERIFY_DELIVERY_OTP = 19;
    public static final int SERVICE_CALL_FOR_VERIFY_PICK_UP_OTP = 20;
    public static final int SERVICE_CALL_FOR_VERIFY_RETURN_OTP = 21;
    public static final int SERVICE_CALL_FOR_VERIFY_RETURNED_OTP = 22;
    public static final int SERVICE_CALL_TO_GET_WALLET_BALANCE = 23;
    public static final int SERVICE_CALL_TO_GET_TRANSACTION_DETAILS = 24;
    public static final int SERVICE_CALL_TO_GET_PAYBACK_REQUEST = 25;

    public static final int SERVICE_CALL_TO_GET_SELECTED_PAYBACK_REQUEST = 26;
    public static final int SERVICE_CALL_FOR_VERIFY_PAYBACK_REQUEST = 27;
    public static final int SERVICE_CALL_FOR_PAYMENT_METHOD_REQUEST = 28;
    public static final int SERVICE_CALL_TO_GET_RAZOR_PAY_KEY_DETAILS = 29;
    public static final int SERVICE_CALL_FOR_PAYMENT_ONLINE_PAYMENT_REQUEST = 30;
    public static final int SERVICE_CALL_TO_GET_MY_DELIVERY_BOY_DETAILS = 31;
    public static final int SERVICE_CALL_TO_GET_DELIVERY_SUPERINTENDENT_DASHBOARD = 32;
    public static final String NOTIFICATION_CHANNEL_ID = "tradebeaDeliveryBoy_channel";
}
