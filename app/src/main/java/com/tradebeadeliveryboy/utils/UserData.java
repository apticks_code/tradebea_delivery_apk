package com.tradebeadeliveryboy.utils;

import android.content.Context;

import androidx.fragment.app.Fragment;

import com.tradebeadeliveryboy.models.responseModels.dashboardDetailsResponse.DeliverySuperintendent;
import com.tradebeadeliveryboy.models.responseModels.dashboardDetailsResponse.TradebeaUser;
import com.tradebeadeliveryboy.models.responseModels.getOrderResponse.OrderData;

public class UserData {

    private static UserData userData = null;

    private Context orderDetailsContext;
    private Context senderReceiverContext;

    private Fragment walletFragment;
    private OrderData selectedOrderData;
    private DeliverySuperintendent deliverySuperintendent;
    private TradebeaUser tradebeaUser;
    private int paybackReqId = -1;

    private UserData() {
    }

    public static UserData getInstance() {
        if (userData == null) {
            userData = new UserData();
        }
        return userData;
    }

    public OrderData getSelectedOrderData() {
        return selectedOrderData;
    }

    public void setSelectedOrderData(OrderData selectedOrderData) {
        this.selectedOrderData = selectedOrderData;
    }

    public Context getOrderDetailsContext() {
        return orderDetailsContext;
    }

    public void setOrderDetailsContext(Context orderDetailsContext) {
        this.orderDetailsContext = orderDetailsContext;
    }

    public Fragment getWalletFragment() {
        return walletFragment;
    }

    public void setWalletFragment(Fragment walletFragment) {
        this.walletFragment = walletFragment;
    }

    public Context getSenderReceiverContext() {
        return senderReceiverContext;
    }

    public void setSenderReceiverContext(Context senderReceiverContext) {
        this.senderReceiverContext = senderReceiverContext;
    }

    public DeliverySuperintendent getDeliverySuperintendent() {
        return deliverySuperintendent;
    }

    public void setDeliverySuperintendent(DeliverySuperintendent deliverySuperintendent) {
        this.deliverySuperintendent = deliverySuperintendent;
    }

    public TradebeaUser getTradebeaUser() {
        return tradebeaUser;
    }

    public void setTradebeaUser(TradebeaUser tradebeaUser) {
        this.tradebeaUser = tradebeaUser;
    }

    public int getPaybackReqId() {
        return paybackReqId;
    }

    public void setPaybackReqId(int paybackReqId) {
        this.paybackReqId = paybackReqId;
    }
}
