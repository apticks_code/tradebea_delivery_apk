package com.tradebeadeliveryboy.utils;

import android.annotation.SuppressLint;
import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import com.tradebeadeliveryboy.interfaces.LocationUpdateCallBack;

public class GetCurrentLocation implements LocationListener {

    private Context context;
    private Fragment fragment;
    private LocationManager locationManager;

    private Double latitude;
    private Double longitude;

    public GetCurrentLocation(Context context, Fragment fragment, LocationManager locationManager) {
        this.context = context;
        this.fragment = fragment;
        this.locationManager = locationManager;

        getLocationUpdates(context, fragment, locationManager);
    }


    @SuppressLint("MissingPermission")
    public void getLocationUpdates(Context context, Fragment fragment, LocationManager locationManager) {
        if (this.locationManager != null) {
            this.locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,
                    0, 0,
                    this);

            this.locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER,
                    0, 0,
                    this);
        }

    }

    @SuppressLint("MissingPermission")
    @Override
    public void onLocationChanged(Location location) {
        if (location != null) {
            locationManager.removeUpdates(this);
            if (fragment != null) {
                LocationUpdateCallBack locationUpdateCallBack = (LocationUpdateCallBack) fragment;
                locationUpdateCallBack.OnLatLongReceived(location.getLatitude(), location.getLongitude());
            }
            if (context != null) {
                LocationUpdateCallBack locationUpdateCallBack = (LocationUpdateCallBack) context;
                locationUpdateCallBack.OnLatLongReceived(location.getLatitude(), location.getLongitude());
            }
        }
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }
}
