package com.tradebeadeliveryboy.utils;

import android.content.Context;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import com.android.volley.NoConnectionError;
import com.android.volley.VolleyError;
import com.androidnetworking.error.ANError;
import com.tradebeadeliveryboy.R;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.Charset;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CommonMethods {

    /**
     * to hide the keyboard
     *
     * @param context context of the activity or fragment
     * @param view    any view in that particular activity or fragment
     */
    public static void hideSoftKeyboard(Context context, View view) {
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
            assert imm != null;
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    /**
     * to convert jsonobject to inputstream
     *
     * @param response json object of service
     * @return returns the input stream through reader
     */
    public static Reader getInputStream(String response) {
        InputStream result = new ByteArrayInputStream(response.getBytes(Charset.forName("UTF-8")));
        return new InputStreamReader(result);
    }

    public static boolean isValidEmailFormat(String emailTxt) {
        try {
            Pattern pattern = Pattern.compile("^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");
            Matcher matcher = pattern.matcher(emailTxt);
            return matcher.matches();
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public static boolean isValidPhoneNumber(String phoneTxt) {
        try {
            Pattern pattern = Pattern.compile("^[0-9]\\d{9}$");
            Matcher matcher = pattern.matcher(phoneTxt);
            return matcher.matches();
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public static void handleVolleyErrorResponse(Context context, VolleyError error) {
        if (error instanceof NoConnectionError) {
            DialogUtils.getDialogUtilsInstance().displayToast(context, context.getString(R.string.network_error));
        } else {
            DialogUtils.getDialogUtilsInstance().displayToast(context, context.getString(R.string.status_error));
        }
    }

    public static void handleErrorResponse(Context context, ANError error) {
        DialogUtils.getDialogUtilsInstance().displayToast(context, context.getString(R.string.status_error));
    }

    public static String currentDateTime() {
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        return sdf.format(calendar.getTime());
    }

    public static String currentDate() {
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
        return sdf.format(calendar.getTime());
    }
}