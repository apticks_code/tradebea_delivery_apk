package com.tradebeadeliveryboy.utils;

import android.app.Dialog;
import android.content.Context;
import android.view.Gravity;
import android.view.Window;
import android.widget.TextView;
import android.widget.Toast;

import com.tradebeadeliveryboy.R;

import java.util.logging.Level;
import java.util.logging.Logger;

public class DialogUtils {
    private static DialogUtils dialogUtils = null;

    private DialogUtils() {
    }

    public static DialogUtils getDialogUtilsInstance() {
        if (dialogUtils == null) {
            dialogUtils = new DialogUtils();
        }
        return dialogUtils;
    }

    /**
     * to display toast message
     *
     * @param context context of activity or fragment
     * @param message Toast message
     */
    public void displayToast(Context context, String message) {
        Toast toast = Toast.makeText(context, message, Toast.LENGTH_LONG);
        TextView v = toast.getView().findViewById(android.R.id.message);
        if (v != null) v.setGravity(Gravity.CENTER);
        toast.show();
    }

    public Dialog progressDialog(Context context) {
        Dialog dialog = null;
        try {
            dialog = new Dialog(context);
            if (dialog.getWindow() != null)
                dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.layout_progress_dialog);
            dialog.setCancelable(false);
            dialog.show();
        } catch (Exception e) {
            Logger.getLogger(DialogUtils.class.getName()).log(Level.SEVERE, null, e);
        }
        return dialog;
    }
}
