package com.tradebeadeliveryboy.ApiCalls;

import android.content.Context;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.tradebeadeliveryboy.interfaces.HttpReqResCallBack;
import com.tradebeadeliveryboy.jsonBuilderParser.JsonBuilderParser;
import com.tradebeadeliveryboy.models.requestModels.otpLoginRequest.OTPLoginRequest;
import com.tradebeadeliveryboy.utils.CommonMethods;
import com.tradebeadeliveryboy.utils.Constants;

import org.json.JSONObject;

public class LoginWithOTPApiCall {

    private static HttpReqResCallBack callBack;

    public static void serviceCallToLoginWithOTP(final Context context, String otpLoginUrl, String mobileNumber, String finalOTP, String appId) {
        OTPLoginRequest otpLoginRequest = new OTPLoginRequest();
        otpLoginRequest.mobile = mobileNumber;
        otpLoginRequest.otp = finalOTP;
        JSONObject postObject = JsonBuilderParser.jsonBuilder(otpLoginRequest);

        AndroidNetworking.post(otpLoginUrl)
                .addJSONObjectBody(postObject) // posting json
                .setPriority(Priority.HIGH)
                .addHeaders("APP_id", appId)
                .addHeaders("Content-Type", "application/json")
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        callBack = (HttpReqResCallBack) context;
                        callBack.jsonResponseReceived(response.toString(), 200, Constants.SERVICE_CALL_FOR_VERIFY_OTP);
                    }

                    @Override
                    public void onError(ANError anError) {
                        CommonMethods.handleErrorResponse(context, anError);
                        callBack = (HttpReqResCallBack) context;
                        callBack.jsonResponseReceived(null, 500, Constants.SERVICE_CALL_FOR_VERIFY_OTP);
                    }
                });
    }
}
