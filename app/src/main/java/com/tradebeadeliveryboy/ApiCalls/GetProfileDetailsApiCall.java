package com.tradebeadeliveryboy.ApiCalls;

import android.content.Context;
import android.util.Log;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.tradebeadeliveryboy.interfaces.HttpReqResCallBack;
import com.tradebeadeliveryboy.utils.Constants;

import org.json.JSONObject;

public class GetProfileDetailsApiCall {

    private static HttpReqResCallBack callBack;

    public static void serviceCallToGetProfileDetails(final Context context, final Fragment fragment, final RecyclerView.Adapter adapter, String token) {
        String url = Constants.GET_PROFILE_DETAILS_URL;
        AndroidNetworking.post(url)
                .setPriority(Priority.HIGH)
                .addHeaders("X_AUTH_TOKEN", token)
                .addHeaders("Content-Type", "application/json")
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {

                        Log.d("response.toString()",response.toString());
                        if (fragment != null) {
                            callBack = (HttpReqResCallBack) fragment;
                            callBack.jsonResponseReceived(response.toString(), 200, Constants.SERVICE_CALL_TO_GET_PROFILE_DETAILS);
                        } else if (adapter != null) {
                            callBack = (HttpReqResCallBack) adapter;
                            callBack.jsonResponseReceived(response.toString(), 200, Constants.SERVICE_CALL_TO_GET_PROFILE_DETAILS);
                        } else {
                            callBack = (HttpReqResCallBack) context;
                            callBack.jsonResponseReceived(response.toString(), 200, Constants.SERVICE_CALL_TO_GET_PROFILE_DETAILS);
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        if (fragment != null) {
                            callBack = (HttpReqResCallBack) fragment;
                            callBack.jsonResponseReceived(null, 500, Constants.SERVICE_CALL_TO_GET_PROFILE_DETAILS);
                        } else if (adapter != null) {
                            callBack = (HttpReqResCallBack) adapter;
                            callBack.jsonResponseReceived(null, 500, Constants.SERVICE_CALL_TO_GET_PROFILE_DETAILS);
                        } else {
                            callBack = (HttpReqResCallBack) context;
                            callBack.jsonResponseReceived(null, 500, Constants.SERVICE_CALL_TO_GET_PROFILE_DETAILS);
                        }
                    }
                });
    }
}
