package com.tradebeadeliveryboy.ApiCalls;

import android.annotation.SuppressLint;
import android.content.Context;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.tradebeadeliveryboy.R;
import com.tradebeadeliveryboy.application.AppController;
import com.tradebeadeliveryboy.interfaces.HttpReqResCallBack;
import com.tradebeadeliveryboy.utils.CommonMethods;
import com.tradebeadeliveryboy.utils.Constants;
import com.tradebeadeliveryboy.utils.PreferenceConnector;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class OrderDetailsApiCall {

    private static HttpReqResCallBack callBack;
    private static String authToken;

    public static void serviceCallToGetOrderDetails(Context context, Fragment fragment, RecyclerView.Adapter adapter, String selectedOrderId, String token) {
        String baseUrl = Constants.ORDER_DETAILS_URL + "/" + selectedOrderId;
        //params.put("X_AUTH_TOKEN", Constants.ORDERS_AUTH_TOKEN);
        authToken = token;
        RequestQueue queue = Volley.newRequestQueue(context);
        queue.getCache().clear();
        StringRequest postRequest = new StringRequest(Request.Method.POST, baseUrl, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if (fragment != null) {
                    callBack = (HttpReqResCallBack) fragment;
                    callBack.jsonResponseReceived(response.toString(), 200, Constants.SERVICE_CALL_TO_GET_ORDER_DETAILS);
                } else if (adapter != null) {
                    callBack = (HttpReqResCallBack) adapter;
                    callBack.jsonResponseReceived(response.toString(), 200, Constants.SERVICE_CALL_TO_GET_ORDER_DETAILS);
                } else {
                    callBack = (HttpReqResCallBack) context;
                    callBack.jsonResponseReceived(response.toString(), 200, Constants.SERVICE_CALL_TO_GET_ORDER_DETAILS);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                CommonMethods.handleVolleyErrorResponse(context, error);
                if (fragment != null) {
                    callBack = (HttpReqResCallBack) fragment;
                    callBack.jsonResponseReceived(null, 500, Constants.SERVICE_CALL_TO_GET_ORDER_DETAILS);
                } else if (adapter != null) {
                    callBack = (HttpReqResCallBack) adapter;
                    callBack.jsonResponseReceived(null, 500, Constants.SERVICE_CALL_TO_GET_ORDER_DETAILS);
                } else {
                    callBack = (HttpReqResCallBack) context;
                    callBack.jsonResponseReceived(null, 500, Constants.SERVICE_CALL_TO_GET_ORDER_DETAILS);
                }
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                return headers();
            }
        };
        RetryPolicy policy = new DefaultRetryPolicy(Constants.TIME_OUT_THIRTY_SECONDS, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postRequest.setRetryPolicy(policy);
        queue.add(postRequest);
    }

    @SuppressLint("HardwareIds")
    private static Map<String, String> headers() {
        Map<String, String> params = new HashMap<String, String>();
        params.put("APP_id",Constants.APP_ID);
        params.put("Content-Type", "application/json");
        params.put("X_AUTH_TOKEN", authToken);
        return params;
    }
}
