package com.tradebeadeliveryboy.ApiCalls;

import android.content.Context;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.tradebeadeliveryboy.R;
import com.tradebeadeliveryboy.interfaces.HttpReqResCallBack;
import com.tradebeadeliveryboy.jsonBuilderParser.JsonBuilderParser;
import com.tradebeadeliveryboy.models.requestModels.acceptOrderRequest.AcceptOrderRequest;
import com.tradebeadeliveryboy.utils.CommonMethods;
import com.tradebeadeliveryboy.utils.Constants;
import com.tradebeadeliveryboy.utils.PreferenceConnector;

import org.json.JSONObject;

public class AcceptOrderApiCall {

    private static HttpReqResCallBack callBack;

    public static void serviceCallToAcceptOrder(Context context, Fragment fragment, RecyclerView.Adapter adapter, int notificationId, int orderId) {
        AcceptOrderRequest acceptOrderRequest = new AcceptOrderRequest();
        acceptOrderRequest.notificationId = notificationId;
        acceptOrderRequest.orderId = orderId;
        acceptOrderRequest.jobType = 1;
        String url = Constants.ACCEPT_ORDER_URL;
        String token = PreferenceConnector.readString(context, context.getString(R.string.user_token), "");
        //.addHeaders("X_AUTH_TOKEN", Constants.AUTH_TOKEN)
        JSONObject postObject = JsonBuilderParser.jsonBuilder(acceptOrderRequest);
        AndroidNetworking.post(url)
                .addJSONObjectBody(postObject)
                .setPriority(Priority.HIGH)
                .addHeaders("APP_id", Constants.APP_ID)
                .addHeaders("Content-Type", "application/json")
                .addHeaders("X_AUTH_TOKEN", token)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        if (fragment != null) {
                            callBack = (HttpReqResCallBack) fragment;
                            callBack.jsonResponseReceived(response.toString(), 200, Constants.SERVICE_CALL_TO_ACCEPT_ORDER);
                        } else if (adapter != null) {
                            callBack = (HttpReqResCallBack) adapter;
                            callBack.jsonResponseReceived(response.toString(), 200, Constants.SERVICE_CALL_TO_ACCEPT_ORDER);
                        } else {
                            callBack = (HttpReqResCallBack) context;
                            callBack.jsonResponseReceived(response.toString(), 200, Constants.SERVICE_CALL_TO_ACCEPT_ORDER);
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        CommonMethods.handleErrorResponse(context, anError);
                        if (fragment != null) {
                            callBack = (HttpReqResCallBack) fragment;
                            callBack.jsonResponseReceived(null, 500, Constants.SERVICE_CALL_TO_ACCEPT_ORDER);
                        } else if (adapter != null) {
                            callBack = (HttpReqResCallBack) adapter;
                            callBack.jsonResponseReceived(null, 500, Constants.SERVICE_CALL_TO_ACCEPT_ORDER);
                        } else {
                            callBack = (HttpReqResCallBack) context;
                            callBack.jsonResponseReceived(null, 500, Constants.SERVICE_CALL_TO_ACCEPT_ORDER);
                        }
                    }
                });
    }

}
