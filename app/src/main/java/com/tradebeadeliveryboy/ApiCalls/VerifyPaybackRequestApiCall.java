package com.tradebeadeliveryboy.ApiCalls;

import android.content.Context;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.tradebeadeliveryboy.interfaces.HttpReqResCallBack;
import com.tradebeadeliveryboy.jsonBuilderParser.JsonBuilderParser;
import com.tradebeadeliveryboy.models.requestModels.verifyPaybackRequest.VerifyPaybackRequest;
import com.tradebeadeliveryboy.utils.CommonMethods;
import com.tradebeadeliveryboy.utils.Constants;

import org.json.JSONObject;

public class VerifyPaybackRequestApiCall {

    private static HttpReqResCallBack callBack;

    public static void serviceCallToVerifyPaybackRequest(Context context, Fragment fragment, final RecyclerView.Adapter adapter, String token, String id, String superIntendentOTP) {
        String url = Constants.VERIFY_PAYBACK_REQUEST_URL;
        VerifyPaybackRequest verifyPaybackRequest = new VerifyPaybackRequest();
        verifyPaybackRequest.setPaybackReqId(id);
        verifyPaybackRequest.setOtp(superIntendentOTP);
        JSONObject postObject = JsonBuilderParser.jsonBuilder(verifyPaybackRequest);

        AndroidNetworking.post(url)
                .addJSONObjectBody(postObject) // posting json
                .setPriority(Priority.HIGH)
                .addHeaders("APP_id", Constants.APP_ID)
                .addHeaders("Content-Type", "application/json")
                .addHeaders("X_AUTH_TOKEN", token)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        callBack = (HttpReqResCallBack) context;
                        callBack.jsonResponseReceived(response.toString(), 200, Constants.SERVICE_CALL_FOR_VERIFY_PAYBACK_REQUEST);
                    }

                    @Override
                    public void onError(ANError anError) {
                        CommonMethods.handleErrorResponse(context, anError);
                        callBack = (HttpReqResCallBack) context;
                        callBack.jsonResponseReceived(null, 500, Constants.SERVICE_CALL_FOR_VERIFY_PAYBACK_REQUEST);
                    }
                });
    }
}
