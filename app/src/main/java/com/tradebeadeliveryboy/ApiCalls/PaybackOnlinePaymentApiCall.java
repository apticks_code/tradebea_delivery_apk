package com.tradebeadeliveryboy.ApiCalls;

import android.content.Context;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.tradebeadeliveryboy.interfaces.HttpReqResCallBack;
import com.tradebeadeliveryboy.jsonBuilderParser.JsonBuilderParser;
import com.tradebeadeliveryboy.models.requestModels.paybackOnlinePaymentRequest.PaybackOnlinePaymentRequest;
import com.tradebeadeliveryboy.models.requestModels.paymentMethodRequest.PaymentMethodRequest;
import com.tradebeadeliveryboy.utils.CommonMethods;
import com.tradebeadeliveryboy.utils.Constants;

import org.json.JSONObject;

public class PaybackOnlinePaymentApiCall {

    private static HttpReqResCallBack callBack;

    public static void serviceCallToPaybackOnlinePayment(Context context, Fragment fragment, final RecyclerView.Adapter adapter, PaybackOnlinePaymentRequest paybackOnlinePaymentRequest, String token) {
        String url = Constants.PAYBACK_ONLINE_PAYMENT_REQUEST_URL;
        JSONObject postObject = JsonBuilderParser.jsonBuilder(paybackOnlinePaymentRequest);

        AndroidNetworking.post(url)
                .addJSONObjectBody(postObject) // posting json
                .setPriority(Priority.HIGH)
                .addHeaders("APP_id", Constants.APP_ID)
                .addHeaders("Content-Type", "application/json")
                .addHeaders("X_AUTH_TOKEN", token)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        callBack = (HttpReqResCallBack) context;
                        callBack.jsonResponseReceived(response.toString(), 200, Constants.SERVICE_CALL_FOR_PAYMENT_ONLINE_PAYMENT_REQUEST);
                    }

                    @Override
                    public void onError(ANError anError) {
                        CommonMethods.handleErrorResponse(context, anError);
                        callBack = (HttpReqResCallBack) context;
                        callBack.jsonResponseReceived(null, 500, Constants.SERVICE_CALL_FOR_PAYMENT_ONLINE_PAYMENT_REQUEST);
                    }
                });
    }
}
