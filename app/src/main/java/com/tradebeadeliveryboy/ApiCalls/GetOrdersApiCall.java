package com.tradebeadeliveryboy.ApiCalls;

import android.content.Context;

import androidx.fragment.app.Fragment;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.tradebeadeliveryboy.R;
import com.tradebeadeliveryboy.interfaces.HttpReqResCallBack;
import com.tradebeadeliveryboy.jsonBuilderParser.JsonBuilderParser;
import com.tradebeadeliveryboy.models.requestModels.getOrdersRequest.GetOrdersRequest;
import com.tradebeadeliveryboy.utils.CommonMethods;
import com.tradebeadeliveryboy.utils.Constants;
import com.tradebeadeliveryboy.utils.PreferenceConnector;

import org.json.JSONObject;

public class GetOrdersApiCall {

    private static HttpReqResCallBack callBack;

    public static void serviceCallToGetOrders(Context context, Fragment fragment, String startDate, String endDate) {
        String url = Constants.GET_ORDERS_URL;
        //.addHeaders("X_AUTH_TOKEN", Constants.AUTH_TOKEN)
        String token = PreferenceConnector.readString(context, context.getString(R.string.user_token), "");
        GetOrdersRequest getOrdersRequest = new GetOrdersRequest();
        getOrdersRequest.setStartDate(startDate);
        getOrdersRequest.setEndDate(endDate);
        JSONObject postObject = JsonBuilderParser.jsonBuilder(getOrdersRequest);
        AndroidNetworking.post(url)
                .addJSONObjectBody(postObject) // posting json
                .setPriority(Priority.HIGH)
                .addHeaders("APP_id", Constants.APP_ID)
                .addHeaders("Content-Type", "application/json")
                .addHeaders("X_AUTH_TOKEN", token)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        if (fragment != null) {
                            callBack = (HttpReqResCallBack) fragment;
                            callBack.jsonResponseReceived(response.toString(), 200, Constants.SERVICE_CALL_TO_GET_ORDERS);
                        } else {
                            callBack = (HttpReqResCallBack) context;
                            callBack.jsonResponseReceived(response.toString(), 200, Constants.SERVICE_CALL_TO_GET_ORDERS);
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        CommonMethods.handleErrorResponse(context, anError);
                        if (fragment != null) {
                            callBack = (HttpReqResCallBack) fragment;
                            callBack.jsonResponseReceived(null, 500, Constants.SERVICE_CALL_TO_GET_ORDERS);
                        } else {
                            callBack = (HttpReqResCallBack) context;
                            callBack.jsonResponseReceived(null, 500, Constants.SERVICE_CALL_TO_GET_ORDERS);
                        }
                    }
                });
    }
}
