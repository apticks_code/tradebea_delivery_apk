package com.tradebeadeliveryboy.ApiCalls;

import android.content.Context;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.tradebeadeliveryboy.R;
import com.tradebeadeliveryboy.interfaces.HttpReqResCallBack;
import com.tradebeadeliveryboy.jsonBuilderParser.JsonBuilderParser;
import com.tradebeadeliveryboy.models.requestModels.updateDeliveryBoySessionRequest.UpdateDeliveryBoySessionRequest;
import com.tradebeadeliveryboy.utils.CommonMethods;
import com.tradebeadeliveryboy.utils.Constants;
import com.tradebeadeliveryboy.utils.PreferenceConnector;

import org.json.JSONObject;

public class UpdateDeliveryBoySession {

    private static HttpReqResCallBack callBack;

    public static void serviceCallForUpdateDeliveryBoySession(Context context, Fragment fragment, RecyclerView.Adapter adapter, double latitude, double longitude, int status, String address) {
        String token = PreferenceConnector.readString(context, context.getString(R.string.user_token), "");
        UpdateDeliveryBoySessionRequest updateDeliveryBoySessionRequest = new UpdateDeliveryBoySessionRequest();
        updateDeliveryBoySessionRequest.loginStatus = status;
        updateDeliveryBoySessionRequest.latitude = String.valueOf(latitude);
        updateDeliveryBoySessionRequest.longitude = String.valueOf(longitude);
        updateDeliveryBoySessionRequest.address = address;
        String url = Constants.UPDATE_DELIVERY_BOY_SESSION_URL;
        JSONObject postObject = JsonBuilderParser.jsonBuilder(updateDeliveryBoySessionRequest);
        AndroidNetworking.post(url)
                .addJSONObjectBody(postObject)
                .setPriority(Priority.HIGH)
                .addHeaders("APP_id", Constants.APP_ID)
                .addHeaders("Content-Type", "application/json")
                .addHeaders("X_AUTH_TOKEN", token)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        if (fragment != null) {
                            callBack = (HttpReqResCallBack) fragment;
                            callBack.jsonResponseReceived(response.toString(), 200, Constants.SERVICE_CALL_FOR_UPDATE_DELIVERY_BOY_SESSION);
                        } else if (adapter != null) {
                            callBack = (HttpReqResCallBack) adapter;
                            callBack.jsonResponseReceived(response.toString(), 200, Constants.SERVICE_CALL_FOR_UPDATE_DELIVERY_BOY_SESSION);
                        } else {
                            callBack = (HttpReqResCallBack) context;
                            callBack.jsonResponseReceived(response.toString(), 200, Constants.SERVICE_CALL_FOR_UPDATE_DELIVERY_BOY_SESSION);
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        CommonMethods.handleErrorResponse(context, anError);
                        if (fragment != null) {
                            callBack = (HttpReqResCallBack) fragment;
                            callBack.jsonResponseReceived(null, 500, Constants.SERVICE_CALL_FOR_UPDATE_DELIVERY_BOY_SESSION);
                        } else if (adapter != null) {
                            callBack = (HttpReqResCallBack) adapter;
                            callBack.jsonResponseReceived(null, 500, Constants.SERVICE_CALL_FOR_UPDATE_DELIVERY_BOY_SESSION);
                        } else {
                            callBack = (HttpReqResCallBack) context;
                            callBack.jsonResponseReceived(null, 500, Constants.SERVICE_CALL_FOR_UPDATE_DELIVERY_BOY_SESSION);
                        }
                    }
                });
    }
}
