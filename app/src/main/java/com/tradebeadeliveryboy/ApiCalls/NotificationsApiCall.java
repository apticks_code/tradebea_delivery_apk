package com.tradebeadeliveryboy.ApiCalls;

import android.content.Context;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.tradebeadeliveryboy.R;
import com.tradebeadeliveryboy.interfaces.HttpReqResCallBack;
import com.tradebeadeliveryboy.jsonBuilderParser.JsonBuilderParser;
import com.tradebeadeliveryboy.models.requestModels.getNotificationsRequest.GetNotificationsRequest;
import com.tradebeadeliveryboy.utils.CommonMethods;
import com.tradebeadeliveryboy.utils.Constants;
import com.tradebeadeliveryboy.utils.PreferenceConnector;

import org.json.JSONObject;

public class NotificationsApiCall {

    private static HttpReqResCallBack callBack;

    public static void serviceCallToGetNotifications(Context context, double latitude, double longitude, String address) {
        String url = Constants.GET_NOTIFICATIONS_URL;
        //.addHeaders("X_AUTH_TOKEN", Constants.AUTH_TOKEN)
        String token = PreferenceConnector.readString(context, context.getString(R.string.user_token), "");
        GetNotificationsRequest getNotificationsRequest = new GetNotificationsRequest();
        getNotificationsRequest.setLatitude(String.valueOf(latitude));
        getNotificationsRequest.setLongitude(String.valueOf(longitude));
        getNotificationsRequest.setAddress(address);
        JSONObject postObject = JsonBuilderParser.jsonBuilder(getNotificationsRequest);
        AndroidNetworking.post(url)
                .setPriority(Priority.HIGH)
                .addHeaders("APP_id", Constants.APP_ID)
                .addHeaders("Content-Type", "application/json")
                .addHeaders("X_AUTH_TOKEN", token)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        callBack = (HttpReqResCallBack) context;
                        callBack.jsonResponseReceived(response.toString(), 200, Constants.SERVICE_CALL_TO_GET_NOTIFICATIONS);
                    }

                    @Override
                    public void onError(ANError anError) {
                        CommonMethods.handleErrorResponse(context, anError);
                        callBack = (HttpReqResCallBack) context;
                        callBack.jsonResponseReceived(null, 500, Constants.SERVICE_CALL_TO_GET_NOTIFICATIONS);
                    }
                });
    }
}
