package com.tradebeadeliveryboy.ApiCalls;

import android.content.Context;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.tradebeadeliveryboy.interfaces.HttpReqResCallBack;
import com.tradebeadeliveryboy.utils.Constants;

import java.util.HashMap;
import java.util.Map;

public class GetSelectedPaybackRequestApiCall {

    private static HttpReqResCallBack callBack;

    public static void serviceCallToGetSelectedPaybackRequests(Context context, Fragment fragment, final RecyclerView.Adapter adapter, String token, String id) {
        String url = Constants.BASE_URL + "payment/api/delivery/payback_request/" + id;
        StringRequest postRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if (fragment != null) {
                    callBack = (HttpReqResCallBack) fragment;
                    callBack.jsonResponseReceived(response.toString(), 200, Constants.SERVICE_CALL_TO_GET_SELECTED_PAYBACK_REQUEST);
                } else if (adapter != null) {
                    callBack = (HttpReqResCallBack) adapter;
                    callBack.jsonResponseReceived(response.toString(), 200, Constants.SERVICE_CALL_TO_GET_SELECTED_PAYBACK_REQUEST);
                } else {
                    callBack = (HttpReqResCallBack) context;
                    callBack.jsonResponseReceived(response.toString(), 200, Constants.SERVICE_CALL_TO_GET_SELECTED_PAYBACK_REQUEST);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (fragment != null) {
                    callBack = (HttpReqResCallBack) fragment;
                    callBack.jsonResponseReceived(null, 500, Constants.SERVICE_CALL_TO_GET_SELECTED_PAYBACK_REQUEST);
                } else if (adapter != null) {
                    callBack = (HttpReqResCallBack) adapter;
                    callBack.jsonResponseReceived(null, 500, Constants.SERVICE_CALL_TO_GET_SELECTED_PAYBACK_REQUEST);
                } else {
                    callBack = (HttpReqResCallBack) context;
                    callBack.jsonResponseReceived(null, 500, Constants.SERVICE_CALL_TO_GET_SELECTED_PAYBACK_REQUEST);
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/json");
                headers.put("X_AUTH_TOKEN", token);
                headers.put("APP_id", Constants.APP_ID);
                return headers;

            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(context);
        RetryPolicy policy = new DefaultRetryPolicy(Constants.TIME_OUT_THIRTY_SECONDS, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postRequest.setRetryPolicy(policy);
        requestQueue.add(postRequest);
    }
}
