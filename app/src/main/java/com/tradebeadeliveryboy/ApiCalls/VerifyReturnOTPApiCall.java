package com.tradebeadeliveryboy.ApiCalls;

import android.annotation.SuppressLint;
import android.content.Context;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.tradebeadeliveryboy.R;
import com.tradebeadeliveryboy.interfaces.HttpReqResCallBack;
import com.tradebeadeliveryboy.jsonBuilderParser.JsonBuilderParser;
import com.tradebeadeliveryboy.models.requestModels.verifyReturnOTPRequest.VerifyReturnOTPRequest;
import com.tradebeadeliveryboy.utils.CommonMethods;
import com.tradebeadeliveryboy.utils.Constants;
import com.tradebeadeliveryboy.utils.PreferenceConnector;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class VerifyReturnOTPApiCall {

    private static HttpReqResCallBack callBack;
    private static String token;

    public static void serviceCallForVerifyReturnOTP(Context context, Fragment fragment, RecyclerView.Adapter adapter, VerifyReturnOTPRequest verifyReturnOTPRequest) {
        String baseUrl = Constants.VERIFY_RETURN_OTP_URL;
        token = PreferenceConnector.readString(context, context.getString(R.string.user_token), "");
        RequestQueue queue = Volley.newRequestQueue(context);
        queue.getCache().clear();
        JSONObject postObject = JsonBuilderParser.jsonBuilder(verifyReturnOTPRequest);
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST, baseUrl, postObject,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        if (fragment != null) {
                            callBack = (HttpReqResCallBack) fragment;
                            callBack.jsonResponseReceived(response.toString(), 200, Constants.SERVICE_CALL_FOR_VERIFY_RETURN_OTP);
                        } else if (adapter != null) {
                            callBack = (HttpReqResCallBack) adapter;
                            callBack.jsonResponseReceived(response.toString(), 200, Constants.SERVICE_CALL_FOR_VERIFY_RETURN_OTP);
                        } else {
                            callBack = (HttpReqResCallBack) context;
                            callBack.jsonResponseReceived(response.toString(), 200, Constants.SERVICE_CALL_FOR_VERIFY_RETURN_OTP);
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                CommonMethods.handleVolleyErrorResponse(context, error);
                if (fragment != null) {
                    callBack = (HttpReqResCallBack) fragment;
                    callBack.jsonResponseReceived(null, 500, Constants.SERVICE_CALL_FOR_VERIFY_RETURN_OTP);
                } else if (adapter != null) {
                    callBack = (HttpReqResCallBack) adapter;
                    callBack.jsonResponseReceived(null, 500, Constants.SERVICE_CALL_FOR_VERIFY_RETURN_OTP);
                } else {
                    callBack = (HttpReqResCallBack) context;
                    callBack.jsonResponseReceived(null, 500, Constants.SERVICE_CALL_FOR_VERIFY_RETURN_OTP);
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                return headers();
            }
        };
        RetryPolicy policy = new DefaultRetryPolicy(Constants.TIME_OUT_THIRTY_SECONDS, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        jsonObjReq.setRetryPolicy(policy);
        queue.add(jsonObjReq);
    }

    @SuppressLint("HardwareIds")
    private static Map<String, String> headers() {
        Map<String, String> params = new HashMap<String, String>();
        params.put("APP_id", Constants.APP_ID);
        params.put("Content-Type", "application/json");
        params.put("X_AUTH_TOKEN", token);
        return params;
    }
}
