package com.tradebeadeliveryboy.ApiCalls;

import android.content.Context;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.tradebeadeliveryboy.interfaces.HttpReqResCallBack;
import com.tradebeadeliveryboy.jsonBuilderParser.JsonBuilderParser;
import com.tradebeadeliveryboy.models.requestModels.passwordLoginRequest.PasswordLoginRequest;
import com.tradebeadeliveryboy.utils.CommonMethods;
import com.tradebeadeliveryboy.utils.Constants;

import org.json.JSONObject;

public class LoginDetailsApiCall {

    private static HttpReqResCallBack callBack;

    public static void serviceCallForLoginDetails(final Context context, String mobileNumber, String password) {
        String url = Constants.LOGIN_URL;
        PasswordLoginRequest passwordLoginRequest = new PasswordLoginRequest();
        passwordLoginRequest.identity = mobileNumber;
        passwordLoginRequest.password = password;
        JSONObject postObject = JsonBuilderParser.jsonBuilder(passwordLoginRequest);

        AndroidNetworking.post(url)
                .addJSONObjectBody(postObject) // posting json
                .setPriority(Priority.HIGH)
                .addHeaders("APP_id", Constants.APP_ID)
                .addHeaders("Content-Type", "application/json")
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        callBack = (HttpReqResCallBack) context;
                        callBack.jsonResponseReceived(response.toString(), 200, Constants.SERVICE_CALL_FOR_LOGIN);
                    }

                    @Override
                    public void onError(ANError anError) {
                        CommonMethods.handleErrorResponse(context, anError);
                        callBack = (HttpReqResCallBack) context;
                        callBack.jsonResponseReceived(null, 500, Constants.SERVICE_CALL_FOR_LOGIN);
                    }
                });
    }
}
