package com.tradebeadeliveryboy.ApiCalls;

import android.content.Context;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.tradebeadeliveryboy.interfaces.HttpReqResCallBack;
import com.tradebeadeliveryboy.jsonBuilderParser.JsonBuilderParser;
import com.tradebeadeliveryboy.models.requestModels.updateEditProfileDetailsRequest.UpdateEditProfileDetailsRequest;
import com.tradebeadeliveryboy.utils.CommonMethods;
import com.tradebeadeliveryboy.utils.Constants;

import org.json.JSONObject;

public class UpdateEditProfileDetailsApiCall {

    private static HttpReqResCallBack callBack;

    public static void serviceCallToUpdateEditProfileDetails(Context context, String firstName, String middleName, String lastName, String emailId, String mobile, String selectedGender, String address, String aadharNumber, String drivingLicenseNumber, String profileImageBase64, String aadharNumberBase64, String drivingLicenseBase64, String electricityBillBase64, String token) {
        String url = Constants.UPDATE_PROFILE_URL;
        UpdateEditProfileDetailsRequest updateEditProfileDetailsRequest = new UpdateEditProfileDetailsRequest();
        updateEditProfileDetailsRequest.firstName = firstName;
        updateEditProfileDetailsRequest.middleName = middleName;
        updateEditProfileDetailsRequest.lastName = lastName;
        updateEditProfileDetailsRequest.gender = selectedGender;
        updateEditProfileDetailsRequest.address = address;
        updateEditProfileDetailsRequest.aadharNumber = aadharNumber;
        updateEditProfileDetailsRequest.drivingLicenseNumber = drivingLicenseNumber;
        updateEditProfileDetailsRequest.mobile = mobile;
        updateEditProfileDetailsRequest.email = emailId;
        updateEditProfileDetailsRequest.group = Constants.GROUP_ID_INT;
        updateEditProfileDetailsRequest.aadharCardImage = aadharNumberBase64;
        updateEditProfileDetailsRequest.drivingLicenseImage = drivingLicenseBase64;
        updateEditProfileDetailsRequest.electricityBillImage = electricityBillBase64;
        updateEditProfileDetailsRequest.profileImage = profileImageBase64;
        JSONObject postObject = JsonBuilderParser.jsonBuilder(updateEditProfileDetailsRequest);
        AndroidNetworking.post(url)
                .addJSONObjectBody(postObject) // posting json
                .setPriority(Priority.HIGH)
                .addHeaders("X_AUTH_TOKEN", token)
                .addHeaders("Content-Type", "application/json")
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        callBack = (HttpReqResCallBack) context;
                        callBack.jsonResponseReceived(response.toString(), 200, Constants.SERVICE_CALL_FOR_UPDATE_PROFILE);
                    }

                    @Override
                    public void onError(ANError anError) {
                        CommonMethods.handleErrorResponse(context, anError);
                        callBack = (HttpReqResCallBack) context;
                        callBack.jsonResponseReceived(null, 500, Constants.SERVICE_CALL_FOR_UPDATE_PROFILE);
                    }
                });
    }
}
