package com.tradebeadeliveryboy.models.responseModels.orderDetailsResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.LinkedList;

public class OrderDetailsData {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("track_id")
    @Expose
    private String trackId;
    @SerializedName("order_pickup_otp")
    @Expose
    private Integer orderPickupOtp;
    @SerializedName("order_delivery_otp")
    @Expose
    private Integer orderDeliveryOtp;
    @SerializedName("delivery_address_id")
    @Expose
    private Integer deliveryAddressId;
    @SerializedName("payment_method_id")
    @Expose
    private Integer paymentMethodId;
    @SerializedName("payment_id")
    @Expose
    private Integer paymentId;
    @SerializedName("promo_id")
    @Expose
    private Object promoId;
    @SerializedName("sub_total")
    @Expose
    private Integer subTotal;
    @SerializedName("discount")
    @Expose
    private Object discount;
    @SerializedName("promo_discount")
    @Expose
    private Object promoDiscount;
    @SerializedName("delivery_fee")
    @Expose
    private Integer deliveryFee;
    @SerializedName("tax")
    @Expose
    private Object tax;
    @SerializedName("used_wallet_amount")
    @Expose
    private Float usedWalletAmount;
    @SerializedName("total")
    @Expose
    private Object total;
    @SerializedName("message")
    @Expose
    private Object message;
    @SerializedName("preparation_time")
    @Expose
    private Integer preparationTime;
    @SerializedName("created_user_id")
    @Expose
    private Integer createdUserId;
    @SerializedName("seller_user_id")
    @Expose
    private Integer sellerUserId;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("deleted_at")
    @Expose
    private Object deletedAt;
    @SerializedName("seller_updated_at")
    @Expose
    private String sellerUpdatedAt;
    @SerializedName("order_status")
    @Expose
    private Integer orderStatus;
    @SerializedName("order_details")
    @Expose
    private LinkedList<OrderDetail> listOfOrderDetails = null;
    @SerializedName("delivery_jobs")
    @Expose
    private LinkedList<DeliveryJobs> listOfDeliveryJobs;
    @SerializedName("invoice")
    @Expose
    private Invoice invoice;
    @SerializedName("return_request")
    @Expose
    private ReturnRequest returnRequest;
    @SerializedName("time")
    @Expose
    private String time;
    @SerializedName("date")
    @Expose
    private String date;
    @SerializedName("customer_app_statuses")
    @Expose
    private LinkedList<CustomerAppStatus> listOfCustomerAppStatuses;
    @SerializedName("customer")
    @Expose
    private Customer customer;
    @SerializedName("seller")
    @Expose
    private Seller seller;
    @SerializedName("delivery_boy")
    @Expose
    private DeliveryBoy deliveryBoy;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTrackId() {
        return trackId;
    }

    public void setTrackId(String trackId) {
        this.trackId = trackId;
    }

    public Integer getOrderPickupOtp() {
        return orderPickupOtp;
    }

    public void setOrderPickupOtp(Integer orderPickupOtp) {
        this.orderPickupOtp = orderPickupOtp;
    }

    public Integer getOrderDeliveryOtp() {
        return orderDeliveryOtp;
    }

    public void setOrderDeliveryOtp(Integer orderDeliveryOtp) {
        this.orderDeliveryOtp = orderDeliveryOtp;
    }

    public Integer getDeliveryAddressId() {
        return deliveryAddressId;
    }

    public void setDeliveryAddressId(Integer deliveryAddressId) {
        this.deliveryAddressId = deliveryAddressId;
    }

    public Integer getPaymentMethodId() {
        return paymentMethodId;
    }

    public void setPaymentMethodId(Integer paymentMethodId) {
        this.paymentMethodId = paymentMethodId;
    }

    public Integer getPaymentId() {
        return paymentId;
    }

    public void setPaymentId(Integer paymentId) {
        this.paymentId = paymentId;
    }

    public Object getPromoId() {
        return promoId;
    }

    public void setPromoId(Object promoId) {
        this.promoId = promoId;
    }

    public Integer getSubTotal() {
        return subTotal;
    }

    public void setSubTotal(Integer subTotal) {
        this.subTotal = subTotal;
    }

    public Object getDiscount() {
        return discount;
    }

    public void setDiscount(Object discount) {
        this.discount = discount;
    }

    public Object getPromoDiscount() {
        return promoDiscount;
    }

    public void setPromoDiscount(Object promoDiscount) {
        this.promoDiscount = promoDiscount;
    }

    public Integer getDeliveryFee() {
        return deliveryFee;
    }

    public void setDeliveryFee(Integer deliveryFee) {
        this.deliveryFee = deliveryFee;
    }

    public Object getTax() {
        return tax;
    }

    public void setTax(Object tax) {
        this.tax = tax;
    }

    public Float getUsedWalletAmount() {
        return usedWalletAmount;
    }

    public void setUsedWalletAmount(Float usedWalletAmount) {
        this.usedWalletAmount = usedWalletAmount;
    }

    public Object getTotal() {
        return total;
    }

    public void setTotal(Object total) {
        this.total = total;
    }

    public Object getMessage() {
        return message;
    }

    public void setMessage(Object message) {
        this.message = message;
    }

    public Integer getPreparationTime() {
        return preparationTime;
    }

    public void setPreparationTime(Integer preparationTime) {
        this.preparationTime = preparationTime;
    }

    public Integer getCreatedUserId() {
        return createdUserId;
    }

    public void setCreatedUserId(Integer createdUserId) {
        this.createdUserId = createdUserId;
    }

    public Integer getSellerUserId() {
        return sellerUserId;
    }

    public void setSellerUserId(Integer sellerUserId) {
        this.sellerUserId = sellerUserId;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Object getDeletedAt() {
        return deletedAt;
    }

    public void setDeletedAt(Object deletedAt) {
        this.deletedAt = deletedAt;
    }

    public String getSellerUpdatedAt() {
        return sellerUpdatedAt;
    }

    public void setSellerUpdatedAt(String sellerUpdatedAt) {
        this.sellerUpdatedAt = sellerUpdatedAt;
    }

    public LinkedList<OrderDetail> getListOfOrderDetails() {
        return listOfOrderDetails;
    }

    public void setListOfOrderDetails(LinkedList<OrderDetail> listOfOrderDetails) {
        this.listOfOrderDetails = listOfOrderDetails;
    }

    public LinkedList<DeliveryJobs> getListOfDeliveryJobs() {
        return listOfDeliveryJobs;
    }

    public void setListOfDeliveryJobs(LinkedList<DeliveryJobs> listOfDeliveryJobs) {
        this.listOfDeliveryJobs = listOfDeliveryJobs;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public LinkedList<CustomerAppStatus> getListOfCustomerAppStatuses() {
        return listOfCustomerAppStatuses;
    }

    public void setListOfCustomerAppStatuses(LinkedList<CustomerAppStatus> listOfCustomerAppStatuses) {
        this.listOfCustomerAppStatuses = listOfCustomerAppStatuses;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public Seller getSeller() {
        return seller;
    }

    public void setSeller(Seller seller) {
        this.seller = seller;
    }

    public Integer getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(Integer orderStatus) {
        this.orderStatus = orderStatus;
    }

    public DeliveryBoy getDeliveryBoy() {
        return deliveryBoy;
    }

    public void setDeliveryBoy(DeliveryBoy deliveryBoy) {
        this.deliveryBoy = deliveryBoy;
    }

    public Invoice getInvoice() {
        return invoice;
    }

    public void setInvoice(Invoice invoice) {
        this.invoice = invoice;
    }

    public ReturnRequest getReturnRequest() {
        return returnRequest;
    }

    public void setReturnRequest(ReturnRequest returnRequest) {
        this.returnRequest = returnRequest;
    }
}
