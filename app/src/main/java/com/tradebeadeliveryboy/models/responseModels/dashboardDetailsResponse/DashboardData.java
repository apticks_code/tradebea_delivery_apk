package com.tradebeadeliveryboy.models.responseModels.dashboardDetailsResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DashboardData {

    @SerializedName("on_going_order")
    @Expose
    private DashboardOnGoingOrder dashboardOnGoingOrder;
    @SerializedName("delivery_super_intendent")
    @Expose
    private DeliverySuperintendent deliverySuperintendent;
    @SerializedName("Tradebea_user")
    @Expose
    private TradebeaUser tradebeaUser;
    @SerializedName("current_status")
    @Expose
    private Boolean currentStatus;
    @SerializedName("today_earnings")
    @Expose
    private Integer todayEarnings;
    @SerializedName("today_floating_cash")
    @Expose
    private Integer todayFloatingCash;
    @SerializedName("today_deliveries")
    @Expose
    private Integer todayDeliveries;
    @SerializedName("total_deliveries")
    @Expose
    private Integer totalDeliveries;

    public DashboardOnGoingOrder getDashboardOnGoingOrder() {
        return dashboardOnGoingOrder;
    }

    public void setDashboardOnGoingOrder(DashboardOnGoingOrder dashboardOnGoingOrder) {
        this.dashboardOnGoingOrder = dashboardOnGoingOrder;
    }

    public Boolean getCurrentStatus() {
        return currentStatus;
    }

    public void setCurrentStatus(Boolean currentStatus) {
        this.currentStatus = currentStatus;
    }

    public Integer getTodayEarnings() {
        return todayEarnings;
    }

    public void setTodayEarnings(Integer todayEarnings) {
        this.todayEarnings = todayEarnings;
    }

    public Integer getTodayFloatingCash() {
        return todayFloatingCash;
    }

    public void setTodayFloatingCash(Integer todayFloatingCash) {
        this.todayFloatingCash = todayFloatingCash;
    }

    public Integer getTodayDeliveries() {
        return todayDeliveries;
    }

    public void setTodayDeliveries(Integer todayDeliveries) {
        this.todayDeliveries = todayDeliveries;
    }

    public Integer getTotalDeliveries() {
        return totalDeliveries;
    }

    public void setTotalDeliveries(Integer totalDeliveries) {
        this.totalDeliveries = totalDeliveries;
    }

    public DeliverySuperintendent getDeliverySuperintendent() {
        return deliverySuperintendent;
    }

    public void setDeliverySuperintendent(DeliverySuperintendent deliverySuperintendent) {
        this.deliverySuperintendent = deliverySuperintendent;
    }

    public TradebeaUser getTradebeaUser() {
        return tradebeaUser;
    }

    public void setTradebeaUser(TradebeaUser tradebeaUser) {
        this.tradebeaUser = tradebeaUser;
    }
}
