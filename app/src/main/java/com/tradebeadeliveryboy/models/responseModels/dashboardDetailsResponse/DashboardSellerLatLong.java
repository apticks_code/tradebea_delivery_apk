package com.tradebeadeliveryboy.models.responseModels.dashboardDetailsResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DashboardSellerLatLong {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("location_id")
    @Expose
    private Integer locationId;
    @SerializedName("location")
    @Expose
    private DashboardLocation dashboardLocation;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getLocationId() {
        return locationId;
    }

    public void setLocationId(Integer locationId) {
        this.locationId = locationId;
    }

    public DashboardLocation getDashboardLocation() {
        return dashboardLocation;
    }

    public void setDashboardLocation(DashboardLocation dashboardLocation) {
        this.dashboardLocation = dashboardLocation;
    }
}
