package com.tradebeadeliveryboy.models.responseModels.profileResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UserDetails {

    @SerializedName("user_id")
    @Expose
    private Integer userId;
    @SerializedName("unique_id")
    @Expose
    private String uniqueId;
    @SerializedName("first_name")
    @Expose
    private String firstName;
    @SerializedName("middle_name")
    @Expose
    private Object middleName;
    @SerializedName("last_name")
    @Expose
    private String lastName;
    @SerializedName("gender")
    @Expose
    private String gender;
    @SerializedName("mobile")
    @Expose
    private Long mobile;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("shop_name")
    @Expose
    private String shopName;
    @SerializedName("aadhar_number")
    @Expose
    private String aadharNumber;
    @SerializedName("driving_license_number")
    @Expose
    private String drivingLicenseNumber;
    @SerializedName("ownership_type")
    @Expose
    private String ownershipType;
    @SerializedName("designation")
    @Expose
    private String designation;
    @SerializedName("gst")
    @Expose
    private String gst;
    @SerializedName("pan")
    @Expose
    private String pan;
    @SerializedName("location_id")
    @Expose
    private String locationId;
    @SerializedName("location")
    @Expose
    private GetLocationDetails location;
    @SerializedName("profile_image")
    @Expose
    private String profileImage;
    @SerializedName("signature_image")
    @Expose
    private String signatureImage;
    @SerializedName("electricity_bill_image")
    @Expose
    private String electricityBillImage;
    @SerializedName("pan_card_image")
    @Expose
    private String panCardImage;
    @SerializedName("shop_image")
    @Expose
    private String shopImage;
    @SerializedName("aadhar_card_image")
    @Expose
    private String aadharCardImage;
    @SerializedName("driving_license_image")
    @Expose
    private String drivingLicenseImage;


    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getUniqueId() {
        return uniqueId;
    }

    public void setUniqueId(String uniqueId) {
        this.uniqueId = uniqueId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public Object getMiddleName() {
        return middleName;
    }

    public void setMiddleName(Object middleName) {
        this.middleName = middleName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public Long getMobile() {
        return mobile;
    }

    public void setMobile(Long mobile) {
        this.mobile = mobile;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }

    public String getAadharNumber() {
        return aadharNumber;
    }

    public void setAadharNumber(String aadharNumber) {
        this.aadharNumber = aadharNumber;
    }

    public String getDrivingLicenseNumber() {
        return drivingLicenseNumber;
    }

    public void setDrivingLicenseNumber(String drivingLicenseNumber) {
        this.drivingLicenseNumber = drivingLicenseNumber;
    }

    public String getOwnershipType() {
        return ownershipType;
    }

    public void setOwnershipType(String ownershipType) {
        this.ownershipType = ownershipType;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public String getGst() {
        return gst;
    }

    public void setGst(String gst) {
        this.gst = gst;
    }

    public String getPan() {
        return pan;
    }

    public void setPan(String pan) {
        this.pan = pan;
    }

    public String getLocationId() {
        return locationId;
    }

    public void setLocationId(String locationId) {
        this.locationId = locationId;
    }

    public GetLocationDetails getLocation() {
        return location;
    }

    public void setLocation(GetLocationDetails location) {
        this.location = location;
    }

    public String getProfileImage() {
        return profileImage;
    }

    public void setProfileImage(String profileImage) {
        this.profileImage = profileImage;
    }

    public String getSignatureImage() {
        return signatureImage;
    }

    public void setSignatureImage(String signatureImage) {
        this.signatureImage = signatureImage;
    }

    public String getElectricityBillImage() {
        return electricityBillImage;
    }

    public void setElectricityBillImage(String electricityBillImage) {
        this.electricityBillImage = electricityBillImage;
    }

    public String getPanCardImage() {
        return panCardImage;
    }

    public void setPanCardImage(String panCardImage) {
        this.panCardImage = panCardImage;
    }

    public String getShopImage() {
        return shopImage;
    }

    public void setShopImage(String shopImage) {
        this.shopImage = shopImage;
    }

    public String getAadharCardImage() {
        return aadharCardImage;
    }

    public void setAadharCardImage(String aadharCardImage) {
        this.aadharCardImage = aadharCardImage;
    }

    public String getDrivingLicenseImage() {
        return drivingLicenseImage;
    }

    public void setDrivingLicenseImage(String drivingLicenseImage) {
        this.drivingLicenseImage = drivingLicenseImage;
    }
}
