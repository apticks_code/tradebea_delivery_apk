package com.tradebeadeliveryboy.models.responseModels.walletBalanceResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AccountDetails {
    @SerializedName("user_id")
    @Expose
    private Integer userId;
    @SerializedName("external_id")
    @Expose
    private Object externalId;
    @SerializedName("wallet")
    @Expose
    private Integer wallet;
    @SerializedName("floating_wallet")
    @Expose
    private Integer floatingWallet;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Object getExternalId() {
        return externalId;
    }

    public void setExternalId(Object externalId) {
        this.externalId = externalId;
    }

    public Integer getWallet() {
        return wallet;
    }

    public void setWallet(Integer wallet) {
        this.wallet = wallet;
    }

    public Integer getFloatingWallet() {
        return floatingWallet;
    }

    public void setFloatingWallet(Integer floatingWallet) {
        this.floatingWallet = floatingWallet;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }
}
