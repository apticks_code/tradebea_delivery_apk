package com.tradebeadeliveryboy.models.responseModels.paybackResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.LinkedList;

public class PaybackResponse {
    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("http_code")
    @Expose
    private Integer httpCode;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private LinkedList<PaybackData> listOfPaybackData;

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public Integer getHttpCode() {
        return httpCode;
    }

    public void setHttpCode(Integer httpCode) {
        this.httpCode = httpCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public LinkedList<PaybackData> getListOfPaybackData() {
        return listOfPaybackData;
    }

    public void setListOfPaybackData(LinkedList<PaybackData> listOfPaybackData) {
        this.listOfPaybackData = listOfPaybackData;
    }
}
