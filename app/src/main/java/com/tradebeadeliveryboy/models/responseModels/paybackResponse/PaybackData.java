package com.tradebeadeliveryboy.models.responseModels.paybackResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PaybackData {
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("ticket_id")
    @Expose
    private Object ticketId;
    @SerializedName("superintendent_otp")
    @Expose
    private Integer superintendentOtp;
    @SerializedName("tradebea_otp")
    @Expose
    private Integer tradebeaOtp;
    @SerializedName("amount")
    @Expose
    private Integer amount;
    @SerializedName("source")
    @Expose
    private PaybackSource paybackSource;
    @SerializedName("req_type")
    @Expose
    private Integer reqType;
    @SerializedName("sender_user_id")
    @Expose
    private Integer senderUserId;
    @SerializedName("receiver_user_id")
    @Expose
    private Integer receiverUserId;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private Object updatedAt;
    @SerializedName("status")
    @Expose
    private PaybackStatus paybackStatus;
    @SerializedName("sender_unique_id")
    @Expose
    private String senderUniqueId;
    @SerializedName("receiver_unique_id")
    @Expose
    private String receiverUniqueId;
    @SerializedName("is_this_my_req")
    @Expose
    private Integer isThisMyReq;
    @SerializedName("time")
    @Expose
    private String time;
    @SerializedName("date")
    @Expose
    private String date;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Object getTicketId() {
        return ticketId;
    }

    public void setTicketId(Object ticketId) {
        this.ticketId = ticketId;
    }

    public Integer getSuperintendentOtp() {
        return superintendentOtp;
    }

    public void setSuperintendentOtp(Integer superintendentOtp) {
        this.superintendentOtp = superintendentOtp;
    }

    public Integer getTradebeaOtp() {
        return tradebeaOtp;
    }

    public void setTradebeaOtp(Integer tradebeaOtp) {
        this.tradebeaOtp = tradebeaOtp;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public PaybackSource getPaybackSource() {
        return paybackSource;
    }

    public void setPaybackSource(PaybackSource paybackSource) {
        this.paybackSource = paybackSource;
    }

    public Integer getReqType() {
        return reqType;
    }

    public void setReqType(Integer reqType) {
        this.reqType = reqType;
    }

    public Integer getSenderUserId() {
        return senderUserId;
    }

    public void setSenderUserId(Integer senderUserId) {
        this.senderUserId = senderUserId;
    }

    public Integer getReceiverUserId() {
        return receiverUserId;
    }

    public void setReceiverUserId(Integer receiverUserId) {
        this.receiverUserId = receiverUserId;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public Object getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Object updatedAt) {
        this.updatedAt = updatedAt;
    }

    public PaybackStatus getPaybackStatus() {
        return paybackStatus;
    }

    public void setPaybackStatus(PaybackStatus paybackStatus) {
        this.paybackStatus = paybackStatus;
    }

    public String getSenderUniqueId() {
        return senderUniqueId;
    }

    public void setSenderUniqueId(String senderUniqueId) {
        this.senderUniqueId = senderUniqueId;
    }

    public String getReceiverUniqueId() {
        return receiverUniqueId;
    }

    public void setReceiverUniqueId(String receiverUniqueId) {
        this.receiverUniqueId = receiverUniqueId;
    }

    public Integer getIsThisMyReq() {
        return isThisMyReq;
    }

    public void setIsThisMyReq(Integer isThisMyReq) {
        this.isThisMyReq = isThisMyReq;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
