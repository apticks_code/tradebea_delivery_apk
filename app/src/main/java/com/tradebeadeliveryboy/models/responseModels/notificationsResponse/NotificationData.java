package com.tradebeadeliveryboy.models.responseModels.notificationsResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class NotificationData {
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("notification_type_id")
    @Expose
    private Integer notificationTypeId;
    @SerializedName("order_id")
    @Expose
    private Integer orderId;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("notified_user_id")
    @Expose
    private Integer notifiedUserId;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("order")
    @Expose
    private NotificationOrderDetails notificationOrderDetails;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getNotificationTypeId() {
        return notificationTypeId;
    }

    public void setNotificationTypeId(Integer notificationTypeId) {
        this.notificationTypeId = notificationTypeId;
    }

    public Integer getOrderId() {
        return orderId;
    }

    public void setOrderId(Integer orderId) {
        this.orderId = orderId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Integer getNotifiedUserId() {
        return notifiedUserId;
    }

    public void setNotifiedUserId(Integer notifiedUserId) {
        this.notifiedUserId = notifiedUserId;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public NotificationOrderDetails getNotificationOrderDetails() {
        return notificationOrderDetails;
    }

    public void setNotificationOrderDetails(NotificationOrderDetails notificationOrderDetails) {
        this.notificationOrderDetails = notificationOrderDetails;
    }
}
