package com.tradebeadeliveryboy.models.responseModels.orderDetailsResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class VariantValue {

    @SerializedName("variant_id")
    @Expose
    private Integer variantId;
    @SerializedName("option_id")
    @Expose
    private Integer optionId;
    @SerializedName("option_group_id")
    @Expose
    private Integer optionGroupId;
    @SerializedName("option_item_id")
    @Expose
    private Integer optionItemId;
    @SerializedName("option")
    @Expose
    private Option option;
    @SerializedName("option_group")
    @Expose
    private OptionGroup optionGroup;
    @SerializedName("option_item")
    @Expose
    private OptionItem optionItem;


    public Integer getVariantId() {
        return variantId;
    }

    public void setVariantId(Integer variantId) {
        this.variantId = variantId;
    }

    public Integer getOptionId() {
        return optionId;
    }

    public void setOptionId(Integer optionId) {
        this.optionId = optionId;
    }

    public Integer getOptionGroupId() {
        return optionGroupId;
    }

    public void setOptionGroupId(Integer optionGroupId) {
        this.optionGroupId = optionGroupId;
    }

    public Integer getOptionItemId() {
        return optionItemId;
    }

    public void setOptionItemId(Integer optionItemId) {
        this.optionItemId = optionItemId;
    }

    public Option getOption() {
        return option;
    }

    public void setOption(Option option) {
        this.option = option;
    }

    public OptionGroup getOptionGroup() {
        return optionGroup;
    }

    public void setOptionGroup(OptionGroup optionGroup) {
        this.optionGroup = optionGroup;
    }

    public OptionItem getOptionItem() {
        return optionItem;
    }

    public void setOptionItem(OptionItem optionItem) {
        this.optionItem = optionItem;
    }
}
