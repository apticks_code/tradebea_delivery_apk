package com.tradebeadeliveryboy.models.responseModels.orderDetailsResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.LinkedList;

public class DeliveryJobs {

    /*@SerializedName("status")
    @Expose
    private LinkedList<DeliveryJobsStatus> listOfDeliveryJobsStatus;

    public LinkedList<DeliveryJobsStatus> getListOfDeliveryJobsStatus() {
        return listOfDeliveryJobsStatus;
    }

    public void setListOfDeliveryJobsStatus(LinkedList<DeliveryJobsStatus> listOfDeliveryJobsStatus) {
        this.listOfDeliveryJobsStatus = listOfDeliveryJobsStatus;
    }*/

    @SerializedName("order_id")
    @Expose
    private Integer orderId;
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("job_id")
    @Expose
    private String jobId;
    @SerializedName("rating")
    @Expose
    private Object rating;
    @SerializedName("feedback")
    @Expose
    private Object feedback;
    @SerializedName("job_type")
    @Expose
    private Integer jobType;
    @SerializedName("cancellation_reason")
    @Expose
    private Object cancellationReason;
    @SerializedName("amount_collected")
    @Expose
    private Integer amountCollected;
    @SerializedName("delivery_partner_user_id")
    @Expose
    private Integer deliveryPartnerUserId;
    @SerializedName("status")
    @Expose
    private Integer status;


    public Integer getOrderId() {
        return orderId;
    }

    public void setOrderId(Integer orderId) {
        this.orderId = orderId;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getJobId() {
        return jobId;
    }

    public void setJobId(String jobId) {
        this.jobId = jobId;
    }

    public Object getRating() {
        return rating;
    }

    public void setRating(Object rating) {
        this.rating = rating;
    }

    public Object getFeedback() {
        return feedback;
    }

    public void setFeedback(Object feedback) {
        this.feedback = feedback;
    }

    public Integer getJobType() {
        return jobType;
    }

    public void setJobType(Integer jobType) {
        this.jobType = jobType;
    }

    public Object getCancellationReason() {
        return cancellationReason;
    }

    public void setCancellationReason(Object cancellationReason) {
        this.cancellationReason = cancellationReason;
    }

    public Integer getAmountCollected() {
        return amountCollected;
    }

    public void setAmountCollected(Integer amountCollected) {
        this.amountCollected = amountCollected;
    }

    public Integer getDeliveryPartnerUserId() {
        return deliveryPartnerUserId;
    }

    public void setDeliveryPartnerUserId(Integer deliveryPartnerUserId) {
        this.deliveryPartnerUserId = deliveryPartnerUserId;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }
}
