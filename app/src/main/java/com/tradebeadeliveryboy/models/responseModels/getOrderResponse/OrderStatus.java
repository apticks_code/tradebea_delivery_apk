package com.tradebeadeliveryboy.models.responseModels.getOrderResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.LinkedList;

public class OrderStatus {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("current_status")
    @Expose
    private String currentStatus;
    @SerializedName("all_statuses_list")
    @Expose
    private LinkedList<String> listOfOrderStatus;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCurrentStatus() {
        return currentStatus;
    }

    public void setCurrentStatus(String currentStatus) {
        this.currentStatus = currentStatus;
    }

    public LinkedList<String> getListOfOrderStatus() {
        return listOfOrderStatus;
    }

    public void setListOfOrderStatus(LinkedList<String> listOfOrderStatus) {
        this.listOfOrderStatus = listOfOrderStatus;
    }
}
