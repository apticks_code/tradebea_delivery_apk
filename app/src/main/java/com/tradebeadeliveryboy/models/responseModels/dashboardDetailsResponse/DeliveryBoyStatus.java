package com.tradebeadeliveryboy.models.responseModels.dashboardDetailsResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DeliveryBoyStatus {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("current_status")
    @Expose
    private String currentStatus;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCurrentStatus() {
        return currentStatus;
    }

    public void setCurrentStatus(String currentStatus) {
        this.currentStatus = currentStatus;
    }
}
