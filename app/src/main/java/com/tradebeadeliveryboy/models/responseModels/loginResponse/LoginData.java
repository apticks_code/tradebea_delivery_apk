package com.tradebeadeliveryboy.models.responseModels.loginResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LoginData {

    @SerializedName("token")
    @Expose
    private String token;

    @SerializedName("am_i_a_delivery_superintendent")
    @Expose
    private Boolean amIADeliverySuperintendent;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Boolean getAmIADeliverySuperintendent() {
        return amIADeliverySuperintendent;
    }

    public void setAmIADeliverySuperintendent(Boolean amIADeliverySuperintendent) {
        this.amIADeliverySuperintendent = amIADeliverySuperintendent;
    }
}
