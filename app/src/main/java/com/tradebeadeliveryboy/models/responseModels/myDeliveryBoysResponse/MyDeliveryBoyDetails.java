package com.tradebeadeliveryboy.models.responseModels.myDeliveryBoysResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MyDeliveryBoyDetails {
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("superintendent_user_id")
    @Expose
    private Integer superintendentUserId;
    @SerializedName("delivery_partner_user_id")
    @Expose
    private Integer deliveryPartnerUserId;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private Object updatedAt;
    @SerializedName("deleted_at")
    @Expose
    private Object deletedAt;
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("user_details")
    @Expose
    private UserDetails userDetails;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getSuperintendentUserId() {
        return superintendentUserId;
    }

    public void setSuperintendentUserId(Integer superintendentUserId) {
        this.superintendentUserId = superintendentUserId;
    }

    public Integer getDeliveryPartnerUserId() {
        return deliveryPartnerUserId;
    }

    public void setDeliveryPartnerUserId(Integer deliveryPartnerUserId) {
        this.deliveryPartnerUserId = deliveryPartnerUserId;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public Object getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Object updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Object getDeletedAt() {
        return deletedAt;
    }

    public void setDeletedAt(Object deletedAt) {
        this.deletedAt = deletedAt;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public UserDetails getUserDetails() {
        return userDetails;
    }

    public void setUserDetails(UserDetails userDetails) {
        this.userDetails = userDetails;
    }
}
