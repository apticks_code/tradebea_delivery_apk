package com.tradebeadeliveryboy.models.responseModels.orderDetailsResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class OrderDetail {

    @SerializedName("order_id")
    @Expose
    private Integer orderId;
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("product_id")
    @Expose
    private Integer productId;
    @SerializedName("variant_id")
    @Expose
    private Integer variantId;
    @SerializedName("seller_variant_id")
    @Expose
    private Integer sellerVariantId;
    @SerializedName("qty")
    @Expose
    private Integer qty;
    @SerializedName("price")
    @Expose
    private float price;
    @SerializedName("tax")
    @Expose
    private float tax;
    @SerializedName("delivery_fee")
    @Expose
    private float deliveryFee;
    @SerializedName("sub_total")
    @Expose
    private float subTotal;
    @SerializedName("discount")
    @Expose
    private float discount;
    @SerializedName("promo_discount")
    @Expose
    private float promoDiscount;
    @SerializedName("total")
    @Expose
    private float total;
    @SerializedName("product")
    @Expose
    private Product product;
    @SerializedName("varinat")
    @Expose
    private Varinat varinat;
    @SerializedName("seller_varinat")
    @Expose
    private SellerVarinat sellerVarinat;


    public Integer getOrderId() {
        return orderId;
    }

    public void setOrderId(Integer orderId) {
        this.orderId = orderId;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    public Integer getVariantId() {
        return variantId;
    }

    public void setVariantId(Integer variantId) {
        this.variantId = variantId;
    }

    public Integer getSellerVariantId() {
        return sellerVariantId;
    }

    public void setSellerVariantId(Integer sellerVariantId) {
        this.sellerVariantId = sellerVariantId;
    }

    public Integer getQty() {
        return qty;
    }

    public void setQty(Integer qty) {
        this.qty = qty;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public float getTax() {
        return tax;
    }

    public void setTax(float tax) {
        this.tax = tax;
    }

    public float getDeliveryFee() {
        return deliveryFee;
    }

    public void setDeliveryFee(float deliveryFee) {
        this.deliveryFee = deliveryFee;
    }

    public float getSubTotal() {
        return subTotal;
    }

    public void setSubTotal(float subTotal) {
        this.subTotal = subTotal;
    }

    public float getDiscount() {
        return discount;
    }

    public void setDiscount(float discount) {
        this.discount = discount;
    }

    public float getPromoDiscount() {
        return promoDiscount;
    }

    public void setPromoDiscount(float promoDiscount) {
        this.promoDiscount = promoDiscount;
    }

    public float getTotal() {
        return total;
    }

    public void setTotal(float total) {
        this.total = total;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public Varinat getVarinat() {
        return varinat;
    }

    public void setVarinat(Varinat varinat) {
        this.varinat = varinat;
    }

    public SellerVarinat getSellerVarinat() {
        return sellerVarinat;
    }

    public void setSellerVarinat(SellerVarinat sellerVarinat) {
        this.sellerVarinat = sellerVarinat;
    }
}
