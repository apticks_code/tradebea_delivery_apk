package com.tradebeadeliveryboy.models.responseModels.getSelecedPaybackResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetSelectedPaybackResponse {
    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("http_code")
    @Expose
    private Integer httpCode;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private SelectedPaybackData selectedPaybackData;

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public Integer getHttpCode() {
        return httpCode;
    }

    public void setHttpCode(Integer httpCode) {
        this.httpCode = httpCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public SelectedPaybackData getSelectedPaybackData() {
        return selectedPaybackData;
    }

    public void setSelectedPaybackData(SelectedPaybackData selectedPaybackData) {
        this.selectedPaybackData = selectedPaybackData;
    }
}
