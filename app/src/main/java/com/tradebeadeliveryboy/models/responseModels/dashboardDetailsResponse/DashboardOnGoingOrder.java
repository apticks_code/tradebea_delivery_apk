package com.tradebeadeliveryboy.models.responseModels.dashboardDetailsResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.tradebeadeliveryboy.models.responseModels.orderDetailsResponse.DeliveryJobs;

import java.util.LinkedList;

public class DashboardOnGoingOrder {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("track_id")
    @Expose
    private String trackId;
    @SerializedName("preparation_time")
    @Expose
    private Integer preparationTime;
    @SerializedName("order_delivery_otp")
    @Expose
    private Object orderDeliveryOtp;
    @SerializedName("order_pickup_otp")
    @Expose
    private Integer orderPickupOtp;
    @SerializedName("delivery_address_id")
    @Expose
    private Integer deliveryAddressId;
    @SerializedName("payment_method_id")
    @Expose
    private Integer paymentMethodId;
    @SerializedName("payment_id")
    @Expose
    private Integer paymentId;
    @SerializedName("promo_id")
    @Expose
    private Object promoId;
    @SerializedName("message")
    @Expose
    private Object message;
    @SerializedName("created_user_id")
    @Expose
    private Integer createdUserId;
    @SerializedName("seller_user_id")
    @Expose
    private Integer sellerUserId;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("order_status")
    @Expose
    private OrderStatus orderStatus;
    @SerializedName("time")
    @Expose
    private String time;
    @SerializedName("date")
    @Expose
    private String date;
    @SerializedName("seller_lat_long")
    @Expose
    private DashboardSellerLatLong dashboardSellerLatLong;
    @SerializedName("delivery_boy_status")
    @Expose
    private DeliveryBoyStatus deliveryBoyStatus;
    @SerializedName("delivery_jobs")
    @Expose
    private LinkedList<DeliveryJobs> listOfDeliveryJobs;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTrackId() {
        return trackId;
    }

    public void setTrackId(String trackId) {
        this.trackId = trackId;
    }

    public Integer getPreparationTime() {
        return preparationTime;
    }

    public void setPreparationTime(Integer preparationTime) {
        this.preparationTime = preparationTime;
    }

    public Object getOrderDeliveryOtp() {
        return orderDeliveryOtp;
    }

    public void setOrderDeliveryOtp(Object orderDeliveryOtp) {
        this.orderDeliveryOtp = orderDeliveryOtp;
    }

    public Integer getOrderPickupOtp() {
        return orderPickupOtp;
    }

    public void setOrderPickupOtp(Integer orderPickupOtp) {
        this.orderPickupOtp = orderPickupOtp;
    }

    public Integer getDeliveryAddressId() {
        return deliveryAddressId;
    }

    public void setDeliveryAddressId(Integer deliveryAddressId) {
        this.deliveryAddressId = deliveryAddressId;
    }

    public Integer getPaymentMethodId() {
        return paymentMethodId;
    }

    public void setPaymentMethodId(Integer paymentMethodId) {
        this.paymentMethodId = paymentMethodId;
    }

    public Integer getPaymentId() {
        return paymentId;
    }

    public void setPaymentId(Integer paymentId) {
        this.paymentId = paymentId;
    }

    public Object getPromoId() {
        return promoId;
    }

    public void setPromoId(Object promoId) {
        this.promoId = promoId;
    }

    public Object getMessage() {
        return message;
    }

    public void setMessage(Object message) {
        this.message = message;
    }

    public Integer getCreatedUserId() {
        return createdUserId;
    }

    public void setCreatedUserId(Integer createdUserId) {
        this.createdUserId = createdUserId;
    }

    public Integer getSellerUserId() {
        return sellerUserId;
    }

    public void setSellerUserId(Integer sellerUserId) {
        this.sellerUserId = sellerUserId;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public OrderStatus getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(OrderStatus orderStatus) {
        this.orderStatus = orderStatus;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public DashboardSellerLatLong getDashboardSellerLatLong() {
        return dashboardSellerLatLong;
    }

    public void setDashboardSellerLatLong(DashboardSellerLatLong dashboardSellerLatLong) {
        this.dashboardSellerLatLong = dashboardSellerLatLong;
    }

    public DeliveryBoyStatus getDeliveryBoyStatus() {
        return deliveryBoyStatus;
    }

    public void setDeliveryBoyStatus(DeliveryBoyStatus deliveryBoyStatus) {
        this.deliveryBoyStatus = deliveryBoyStatus;
    }

    public LinkedList<DeliveryJobs> getListOfDeliveryJobs() {
        return listOfDeliveryJobs;
    }

    public void setListOfDeliveryJobs(LinkedList<DeliveryJobs> listOfDeliveryJobs) {
        this.listOfDeliveryJobs = listOfDeliveryJobs;
    }
}
