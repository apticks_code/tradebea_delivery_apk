package com.tradebeadeliveryboy.models.responseModels.getTransactionDetailsResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.tradebeadeliveryboy.models.responseModels.walletBalanceResponse.AccountDetails;

import java.util.LinkedList;

public class TransactionDetailsData {
    @SerializedName("account_details")
    @Expose
    private AccountDetails accountDetails;
    @SerializedName("txns")
    @Expose
    private LinkedList<TransactionDetails> listOfTransactionDetails;

    public AccountDetails getAccountDetails() {
        return accountDetails;
    }

    public void setAccountDetails(AccountDetails accountDetails) {
        this.accountDetails = accountDetails;
    }

    public LinkedList<TransactionDetails> getListOfTransactionDetails() {
        return listOfTransactionDetails;
    }

    public void setListOfTransactionDetails(LinkedList<TransactionDetails> listOfTransactionDetails) {
        this.listOfTransactionDetails = listOfTransactionDetails;
    }
}
