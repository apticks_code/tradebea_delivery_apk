package com.tradebeadeliveryboy.models;

public class NavigationMenuItems {

    private String menuItemName;

    private int menuItemImage;

    public NavigationMenuItems(int menuItemImage, String menuItemName) {
        this.menuItemName = menuItemName;
        this.menuItemImage = menuItemImage;
    }

    public String getMenuItemName() {
        return menuItemName;
    }

    public void setMenuItemName(String menuItemName) {
        this.menuItemName = menuItemName;
    }

    public int getMenuItemImage() {
        return menuItemImage;
    }

    public void setMenuItemImage(int menuItemImage) {
        this.menuItemImage = menuItemImage;
    }

}
