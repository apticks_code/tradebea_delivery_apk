package com.tradebeadeliveryboy.models.requestModels.socialLoginDetailsRequest;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SocialLoginDetailsRequest {

    @SerializedName("name")
    @Expose
    public String name;
    @SerializedName("group_id")
    @Expose
    public Integer groupId;
    @SerializedName("auth_token")
    @Expose
    public String authToken;
    @SerializedName("auth_id")
    @Expose
    public String authId;
    @SerializedName("profile_image")
    @Expose
    public String profileImage;
    @SerializedName("mobile")
    @Expose
    public String mobile;
    @SerializedName("mail")
    @Expose
    public String mail;

}
