package com.tradebeadeliveryboy.models.requestModels.paybackOnlinePaymentRequest;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PaybackOnlinePaymentRequest {
    @SerializedName("txn_id")
    @Expose
    public String txnId;
    @SerializedName("amount")
    @Expose
    public String amount;
    @SerializedName("payback_req_id")
    @Expose
    public String paybackReqId;
    @SerializedName("status")
    @Expose
    public Boolean status;
}
