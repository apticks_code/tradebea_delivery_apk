package com.tradebeadeliveryboy.models.requestModels;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BasicProfileDetailsUpdateRequest {

    @SerializedName("first_name")
    @Expose
    public String firstName;

}
