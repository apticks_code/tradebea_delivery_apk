package com.tradebeadeliveryboy.models.requestModels.verifyPickUpOTPRequest;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class VerifyPickUpOTPRequest {

    @SerializedName("order_id")
    @Expose
    private String orderId;
    @SerializedName("delivery_job_id")
    @Expose
    private String deliveryJobId;
    @SerializedName("otp")
    @Expose
    private String otp;


    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getDeliveryJobId() {
        return deliveryJobId;
    }

    public void setDeliveryJobId(String deliveryJobId) {
        this.deliveryJobId = deliveryJobId;
    }

    public String getOtp() {
        return otp;
    }

    public void setOtp(String otp) {
        this.otp = otp;
    }
}
