package com.tradebeadeliveryboy.models.requestModels.verifyPaybackRequest;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class VerifyPaybackRequest {
    @SerializedName("payback_req_id")
    @Expose
    private String paybackReqId;
    @SerializedName("otp")
    @Expose
    private String otp;

    public String getPaybackReqId() {
        return paybackReqId;
    }

    public void setPaybackReqId(String paybackReqId) {
        this.paybackReqId = paybackReqId;
    }

    public String getOtp() {
        return otp;
    }

    public void setOtp(String otp) {
        this.otp = otp;
    }
}
