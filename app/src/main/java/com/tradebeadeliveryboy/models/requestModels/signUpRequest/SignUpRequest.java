package com.tradebeadeliveryboy.models.requestModels.signUpRequest;

import com.google.gson.annotations.SerializedName;

public class SignUpRequest {
    @SerializedName("first_name")
    public String firstName;
    @SerializedName("last_name")
    public String lastName;
    public String gender;
    public String mobile;
    public String email;
    public String password;
    @SerializedName("confirm_password")
    public String confirmPassword;
    @SerializedName("ref_id")
    public String referral_ID;
    public String group;
}
