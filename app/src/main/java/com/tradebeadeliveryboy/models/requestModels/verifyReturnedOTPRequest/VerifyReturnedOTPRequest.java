package com.tradebeadeliveryboy.models.requestModels.verifyReturnedOTPRequest;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class VerifyReturnedOTPRequest {

    @SerializedName("return_request_id")
    @Expose
    private Integer returnRequestId;
    @SerializedName("delivery_job_id")
    @Expose
    private Integer deliveryJobId;
    @SerializedName("returned_otp")
    @Expose
    private String returnedOtp;


    public Integer getReturnRequestId() {
        return returnRequestId;
    }

    public void setReturnRequestId(Integer returnRequestId) {
        this.returnRequestId = returnRequestId;
    }

    public Integer getDeliveryJobId() {
        return deliveryJobId;
    }

    public void setDeliveryJobId(Integer deliveryJobId) {
        this.deliveryJobId = deliveryJobId;
    }

    public String getReturnedOtp() {
        return returnedOtp;
    }

    public void setReturnedOtp(String returnedOtp) {
        this.returnedOtp = returnedOtp;
    }
}
