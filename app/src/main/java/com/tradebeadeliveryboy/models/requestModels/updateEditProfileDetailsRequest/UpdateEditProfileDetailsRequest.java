package com.tradebeadeliveryboy.models.requestModels.updateEditProfileDetailsRequest;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UpdateEditProfileDetailsRequest {

    @SerializedName("first_name")
    @Expose
    public String firstName;
    @SerializedName("middle_name")
    @Expose
    public String middleName;
    @SerializedName("last_name")
    @Expose
    public String lastName;
    @SerializedName("gender")
    @Expose
    public String gender;
    @SerializedName("address")
    @Expose
    public String address;
    @SerializedName("aadhar_number")
    @Expose
    public String aadharNumber;
    @SerializedName("driving_license_number")
    @Expose
    public String drivingLicenseNumber;
    @SerializedName("mobile")
    @Expose
    public String mobile;
    @SerializedName("email")
    @Expose
    public String email;
    @SerializedName("group")
    @Expose
    public Integer group;
    @SerializedName("aadhar_card_image")
    @Expose
    public String aadharCardImage;
    @SerializedName("driving_license_image")
    @Expose
    public String drivingLicenseImage;
    @SerializedName("electricity_bill_image")
    @Expose
    public String electricityBillImage;
    @SerializedName("profile_image")
    @Expose
    public String profileImage;
}
