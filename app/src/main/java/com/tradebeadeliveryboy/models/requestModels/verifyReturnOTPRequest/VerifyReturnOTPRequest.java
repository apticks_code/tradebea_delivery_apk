package com.tradebeadeliveryboy.models.requestModels.verifyReturnOTPRequest;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class VerifyReturnOTPRequest {
    @SerializedName("return_request_id")
    @Expose
    private Integer returnRequestId;
    @SerializedName("delivery_job_id")
    @Expose
    private Integer deliveryJobId;
    @SerializedName("pickup_otp")
    @Expose
    private String pickupOtp;


    public Integer getReturnRequestId() {
        return returnRequestId;
    }

    public void setReturnRequestId(Integer returnRequestId) {
        this.returnRequestId = returnRequestId;
    }

    public Integer getDeliveryJobId() {
        return deliveryJobId;
    }

    public void setDeliveryJobId(Integer deliveryJobId) {
        this.deliveryJobId = deliveryJobId;
    }

    public String getPickupOtp() {
        return pickupOtp;
    }

    public void setPickupOtp(String pickupOtp) {
        this.pickupOtp = pickupOtp;
    }
}
