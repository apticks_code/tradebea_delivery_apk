package com.tradebeadeliveryboy.models.requestModels.updateDeliveryBoySessionRequest;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UpdateDeliveryBoySessionRequest {
    @SerializedName("login_status")
    @Expose
    public Integer loginStatus;
    @SerializedName("latitude")
    @Expose
    public String latitude;
    @SerializedName("longitude")
    @Expose
    public String longitude;
    @SerializedName("address")
    @Expose
    public String address;
}
