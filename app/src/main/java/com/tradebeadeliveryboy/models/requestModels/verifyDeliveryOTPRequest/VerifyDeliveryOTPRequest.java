package com.tradebeadeliveryboy.models.requestModels.verifyDeliveryOTPRequest;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class VerifyDeliveryOTPRequest {

    @SerializedName("order_id")
    @Expose
    private String orderId;
    @SerializedName("delivery_job_id")
    @Expose
    private String deliveryJobId;
    @SerializedName("otp")
    @Expose
    private String otp;
    @SerializedName("amount_collected")
    @Expose
    private String amountCollected;
    @SerializedName("remarks")
    @Expose
    private String remarks;


    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getDeliveryJobId() {
        return deliveryJobId;
    }

    public void setDeliveryJobId(String deliveryJobId) {
        this.deliveryJobId = deliveryJobId;
    }

    public String getOtp() {
        return otp;
    }

    public void setOtp(String otp) {
        this.otp = otp;
    }

    public String getAmountCollected() {
        return amountCollected;
    }

    public void setAmountCollected(String amountCollected) {
        this.amountCollected = amountCollected;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }
}
