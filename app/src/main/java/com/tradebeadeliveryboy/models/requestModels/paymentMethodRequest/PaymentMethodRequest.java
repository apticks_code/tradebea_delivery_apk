package com.tradebeadeliveryboy.models.requestModels.paymentMethodRequest;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PaymentMethodRequest {
    @SerializedName("amount")
    @Expose
    public String amount;
    @SerializedName("source")
    @Expose
    public String source;
    @SerializedName("receiver_user_id")
    @Expose
    public String receiverUserId;
}
