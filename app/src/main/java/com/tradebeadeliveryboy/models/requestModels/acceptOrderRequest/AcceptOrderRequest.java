package com.tradebeadeliveryboy.models.requestModels.acceptOrderRequest;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AcceptOrderRequest {
    @SerializedName("notification_id")
    @Expose
    public Integer notificationId;
    @SerializedName("order_id")
    @Expose
    public Integer orderId;
    @SerializedName("job_type")
    @Expose
    public Integer jobType;
}
