package com.tradebeadeliveryboy.models.requestModels.reachedToPickUpPointRequest;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ReachedToPickUpPointRequest {

    @SerializedName("order_id")
    @Expose
    private Integer orderId;
    @SerializedName("delivery_job_id")
    @Expose
    private Integer deliveryJobId;
    @SerializedName("status")
    @Expose
    private Integer status;


    public Integer getOrderId() {
        return orderId;
    }

    public void setOrderId(Integer orderId) {
        this.orderId = orderId;
    }

    public Integer getDeliveryJobId() {
        return deliveryJobId;
    }

    public void setDeliveryJobId(Integer deliveryJobId) {
        this.deliveryJobId = deliveryJobId;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }
}
