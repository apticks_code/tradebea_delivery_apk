package com.tradebeadeliveryboy;

import android.app.ActivityManager;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.AudioAttributes;
import android.os.Build;
import android.text.Html;
import android.util.Log;
import androidx.core.app.NotificationCompat;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.tradebeadeliveryboy.activities.PushNotificationHandlerActivity;
import com.tradebeadeliveryboy.fcm.NotificationID;
import com.tradebeadeliveryboy.utils.Constants;
import com.tradebeadeliveryboy.utils.PreferenceConnector;

import org.jetbrains.annotations.NotNull;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Random;


public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private String data = "";
    private String title = "";
    private String message = "";
    private String orderId = "";
    private String currentActivity = "";

    private int count = 0;


    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        Map<String, String> mapOfKeysAndValues = remoteMessage.getData();
        try {
            ActivityManager activityManager = (ActivityManager) this.getSystemService(ACTIVITY_SERVICE);
            assert activityManager != null;
            List<ActivityManager.RunningTaskInfo> taskInfo = activityManager.getRunningTasks(1);
            this.currentActivity = taskInfo.get(0).topActivity.getClassName();
            List<String> listOfKeys = new ArrayList<>(mapOfKeysAndValues.keySet());
            List<String> listOfValues = new ArrayList<>(mapOfKeysAndValues.values());
            Log.d("keys****", listOfKeys.toString() + "::::::::" + listOfValues.toString());
            if (mapOfKeysAndValues.containsKey("data")) {
                data = mapOfKeysAndValues.get("data");
                if (data != null) {
                    if (!data.isEmpty()) {
                        JSONObject jsonObject = new JSONObject(data);
                        String payloadString = jsonObject.getString("payload");
                        JSONObject payloadJsonObject = new JSONObject(payloadString);
                        title = jsonObject.getString("title");
                        message = jsonObject.getString("message");
                        orderId = payloadJsonObject.getString("order_id");
                        showNotification();
                    }
                }
            }
        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }

    @Override
    public void onNewToken(@NotNull String token) {
        PreferenceConnector.writeString(getApplicationContext(), getApplicationContext().getString(R.string.device_token), token);
    }

    private void showNotification() {
        try {
            Bitmap notificationIcon = BitmapFactory.decodeResource(getResources(), R.drawable.ic_logo);
            NotificationManager mNotificationManager = (NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);
            Intent intent = new Intent(this, PushNotificationHandlerActivity.class);
            //intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            // intent.addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS | Intent.FLAG_ACTIVITY_CLEAR_TASK);

            Random random = new Random();
            int requestCode = random.nextInt(999999);
            PendingIntent contentIntent = PendingIntent.getActivity(this, requestCode, intent, PendingIntent.FLAG_UPDATE_CURRENT | PendingIntent.FLAG_ONE_SHOT);
            NotificationCompat.Builder mBuilder = null;
            mBuilder = new NotificationCompat.Builder(this, Constants.NOTIFICATION_CHANNEL_ID)
                    //.setDefaults(Notification.DEFAULT_LIGHTS)
                    .setLargeIcon(notificationIcon)
                    .setSmallIcon(R.drawable.ic_logo)
                    .setContentTitle(Html.fromHtml(title))
                    .setContentText(message)
                    .setStyle(new NotificationCompat.BigTextStyle().bigText(message))
                    .setAutoCancel(true)
                    .setVibrate(new long[]{0, 500, 1000})
                    .setDefaults(Notification.DEFAULT_LIGHTS)
                    //.setSound(Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://" + getPackageName() + "/" + R.raw.notification))
                    .setColor(Color.parseColor("#ff669900"))
                    .setPriority(NotificationCompat.PRIORITY_HIGH)
                    .setContentIntent(contentIntent);


            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                int importance = NotificationManager.IMPORTANCE_HIGH;
                NotificationChannel notificationChannel = new NotificationChannel(Constants.NOTIFICATION_CHANNEL_ID, "NOTIFICATION_CHANNEL_NAME", importance);
                notificationChannel.enableLights(true);
                notificationChannel.setLightColor(Color.GREEN);
                notificationChannel.enableVibration(true);
                AudioAttributes audioAttributes = new AudioAttributes.Builder()
                        .setContentType(AudioAttributes.CONTENT_TYPE_SONIFICATION)
                        .setUsage(AudioAttributes.USAGE_NOTIFICATION)
                        .build();
                //notificationChannel.setSound(Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://" + getPackageName() + "/" + R.raw.notification), audioAttributes);
                notificationChannel.setVibrationPattern(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400});
                assert mNotificationManager != null;
                mBuilder.setChannelId(Constants.NOTIFICATION_CHANNEL_ID);
                mNotificationManager.createNotificationChannel(notificationChannel);
            }

            assert mNotificationManager != null;
            mNotificationManager.notify(NotificationID.getID(), mBuilder.build());
        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }
}



