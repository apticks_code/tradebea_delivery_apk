package com.tradebeadeliveryboy.customviews;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.util.AttributeSet;

import androidx.appcompat.widget.AppCompatTextView;

import com.tradebeadeliveryboy.R;


public class CustomTextView extends AppCompatTextView {
    /**
     * @param context context as parameter
     */
    public CustomTextView(Context context) {
        super(context);
    }

    /**
     * @param context context as parameter
     * @param attrs   AttributeSet as parameter
     */
    public CustomTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        setCustomFont(context, attrs);
    }

    /**
     * @param context      context as parameter
     * @param attrs        AttributeSet as parameter
     * @param defStyleAttr attribute style as parameter
     */
    public CustomTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setCustomFont(context, attrs);
    }

    /**
     * @param ctx   context
     * @param attrs attributes
     */
    @SuppressLint("CustomViewStyleable")
    private void setCustomFont(Context ctx, AttributeSet attrs) {
        TypedArray typedArray = ctx.obtainStyledAttributes(attrs, R.styleable.tradebeaDeliveryBoy);
        String customFont = typedArray.getString(R.styleable.tradebeaDeliveryBoy_custom_font);
        setCustomFont(ctx, customFont);
        typedArray.recycle();
    }

    /**
     * to set custom font to textview
     *
     * @param ctx   context as parameter
     * @param asset custom font style from assets
     * @return custom font
     */
    public boolean setCustomFont(Context ctx, String asset) {
        Typeface typeface = null;
        try {
            typeface = Typeface.createFromAsset(ctx.getAssets(), asset);
        } catch (Exception e) {
            return false;
        }
        setTypeface(typeface);
        return true;
    }
}
