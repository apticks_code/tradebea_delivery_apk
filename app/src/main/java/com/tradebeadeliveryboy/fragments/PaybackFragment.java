package com.tradebeadeliveryboy.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.gson.Gson;
import com.tradebeadeliveryboy.ApiCalls.GetPaybackRequestsApiCall;
import com.tradebeadeliveryboy.R;
import com.tradebeadeliveryboy.activities.PaymentMethodActivity;
import com.tradebeadeliveryboy.adapters.PaybackFragmentAdapter;
import com.tradebeadeliveryboy.interfaces.HttpReqResCallBack;
import com.tradebeadeliveryboy.models.responseModels.paybackResponse.PaybackData;
import com.tradebeadeliveryboy.models.responseModels.paybackResponse.PaybackResponse;
import com.tradebeadeliveryboy.utils.Constants;
import com.tradebeadeliveryboy.utils.PreferenceConnector;

import java.util.LinkedList;

public class PaybackFragment extends BaseFragment implements View.OnClickListener, HttpReqResCallBack {

    private TextView tvError;
    private RecyclerView rvPaybackDetails;
    private SwipeRefreshLayout swipeRefreshLayout;
    private FloatingActionButton fabPaymentMethod;
    private String token = "";
    private LinkedList<PaybackData> listOfPaybackData;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.layout_payback_fragment, container, false);
        initializeUI(view);
        initializeListeners();
        prepareDetails();
        refreshContent();
        return view;
    }

    private void initializeUI(View view) {
        tvError = view.findViewById(R.id.tvError);
        fabPaymentMethod = view.findViewById(R.id.fabPaymentMethod);
        rvPaybackDetails = view.findViewById(R.id.rvPaybackDetails);
        swipeRefreshLayout = view.findViewById(R.id.swipeRefreshLayout);

        PreferenceConnector.writeBoolean(getActivity(), getString(R.string.refresh_pay_back), false);
    }

    private void initializeListeners() {
        fabPaymentMethod.setOnClickListener(this);
    }

    private void prepareDetails() {
        showProgressBar(getActivity());
        token = PreferenceConnector.readString(getActivity(), getString(R.string.user_token), "");
        GetPaybackRequestsApiCall.serviceCallToGetPaybackRequests(getActivity(), this, null, token);
    }

    private void refreshContent() {
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeRefreshLayout.setRefreshing(false);
                prepareDetails();
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        boolean refreshPayback = PreferenceConnector.readBoolean(getActivity(), getString(R.string.refresh_pay_back), false);
        if (refreshPayback) {
            prepareDetails();
            PreferenceConnector.writeBoolean(getActivity(), getString(R.string.refresh_pay_back), false);
        }
    }

    @Override
    public void onClick(View view) {
        int id = fabPaymentMethod.getId();
        if (id == R.id.fabPaymentMethod) {
            preparePaymentMethod();
        }
    }

    private void preparePaymentMethod() {
        Intent paymentMethodIntent = new Intent(getActivity(), PaymentMethodActivity.class);
        startActivity(paymentMethodIntent);
    }

    @Override
    public void jsonResponseReceived(String jsonResponse, int statusCode, int requestType) {
        if (requestType == Constants.SERVICE_CALL_TO_GET_PAYBACK_REQUEST) {
            if (jsonResponse != null) {
                PaybackResponse paybackResponse = new Gson().fromJson(jsonResponse, PaybackResponse.class);
                if (paybackResponse != null) {
                    boolean status = paybackResponse.getStatus();
                    if (status) {
                        listOfPaybackData = paybackResponse.getListOfPaybackData();
                        if (listOfPaybackData != null) {
                            if (listOfPaybackData.size() != 0) {
                                listIsFull();
                                initializeAdapter();
                            } else {
                                listIsEmpty();
                            }
                        } else {
                            listIsEmpty();
                        }
                    }
                }
            }
            closeProgressbar();
        }
    }

    private void initializeAdapter() {
        PaybackFragmentAdapter paybackFragmentAdapter = new PaybackFragmentAdapter(getActivity(), listOfPaybackData);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
        rvPaybackDetails.setLayoutManager(layoutManager);
        rvPaybackDetails.setItemAnimator(new DefaultItemAnimator());
        rvPaybackDetails.setAdapter(paybackFragmentAdapter);
    }

    private void listIsFull() {
        tvError.setVisibility(View.GONE);
        rvPaybackDetails.setVisibility(View.VISIBLE);
    }

    private void listIsEmpty() {
        tvError.setVisibility(View.VISIBLE);
        rvPaybackDetails.setVisibility(View.GONE);
    }
}
