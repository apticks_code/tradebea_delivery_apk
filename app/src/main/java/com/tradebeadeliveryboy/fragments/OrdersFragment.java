package com.tradebeadeliveryboy.fragments;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.gson.Gson;
import com.tradebeadeliveryboy.ApiCalls.GetOrdersApiCall;
import com.tradebeadeliveryboy.R;
import com.tradebeadeliveryboy.adapters.OrdersFragmentAdapter;
import com.tradebeadeliveryboy.interfaces.HttpReqResCallBack;
import com.tradebeadeliveryboy.models.responseModels.getOrderResponse.GetOrderResponse;
import com.tradebeadeliveryboy.models.responseModels.getOrderResponse.OrderData;
import com.tradebeadeliveryboy.utils.CommonMethods;
import com.tradebeadeliveryboy.utils.Constants;

import java.util.Calendar;
import java.util.LinkedList;

public class OrdersFragment extends BaseFragment implements HttpReqResCallBack, View.OnClickListener {

    private RecyclerView rvOrders;
    private DatePickerDialog datePickerDialog;
    private SwipeRefreshLayout swipeRefreshLayout;
    private TextView tvFromDate, tvToDate, tvApply, tvError;

    private LinkedList<OrderData> listOfOrderData;

    private String endDate = "";
    private String startDate = "";

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.layout_orders_fragment, container, false);
        initializeUi(view);
        initializeListeners();
        prepareDetails();
        refreshContent();
        return view;
    }

    private void initializeUi(View view) {
        tvError = view.findViewById(R.id.tvError);
        tvApply = view.findViewById(R.id.tvApply);
        tvToDate = view.findViewById(R.id.tvToDate);
        rvOrders = view.findViewById(R.id.rvOrders);
        tvFromDate = view.findViewById(R.id.tvFromDate);
        swipeRefreshLayout = view.findViewById(R.id.swipeRefreshLayout);
    }

    private void initializeListeners() {
        tvApply.setOnClickListener(this);
        tvToDate.setOnClickListener(this);
        tvFromDate.setOnClickListener(this);
    }

    private void prepareDetails() {
        startDate = CommonMethods.currentDate();
        endDate = CommonMethods.currentDate();

        tvFromDate.setText(startDate);
        tvToDate.setText(endDate);

        showProgressBar(getActivity());
        GetOrdersApiCall.serviceCallToGetOrders(getActivity(), this, startDate, endDate);
    }

    private void refreshContent() {
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeRefreshLayout.setRefreshing(false);
                showProgressBar(getActivity());
                GetOrdersApiCall.serviceCallToGetOrders(getActivity(), OrdersFragment.this, startDate, endDate);
            }
        });
    }

    private void initializeAdapter() {
        OrdersFragmentAdapter ordersFragmentAdapter = new OrdersFragmentAdapter(getActivity(), listOfOrderData);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
        rvOrders.setLayoutManager(layoutManager);
        rvOrders.setItemAnimator(new DefaultItemAnimator());
        rvOrders.setAdapter(ordersFragmentAdapter);
    }

    @Override
    public void jsonResponseReceived(String jsonResponse, int statusCode, int requestType) {
        if (requestType == Constants.SERVICE_CALL_TO_GET_ORDERS) {
            if (jsonResponse != null) {
                GetOrderResponse getOrderResponse = new Gson().fromJson(jsonResponse, GetOrderResponse.class);
                boolean status = getOrderResponse.getStatus();
                String message = getOrderResponse.getMessage();
                if (status) {
                    listOfOrderData = getOrderResponse.getListOfOrderData();
                    if (listOfOrderData != null) {
                        if (listOfOrderData.size() != 0) {
                            listIsFull();
                            initializeAdapter();
                        } else {
                            listIsEmpty();
                        }
                    } else {
                        listIsEmpty();
                    }
                } else {
                    Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                }
            }
            closeProgressbar();
        }
    }

    private void listIsFull() {
        tvError.setVisibility(View.GONE);
        rvOrders.setVisibility(View.VISIBLE);
    }

    private void listIsEmpty() {
        tvError.setVisibility(View.VISIBLE);
        rvOrders.setVisibility(View.GONE);
    }

    @SuppressLint("NonConstantResourceId")
    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id) {
            case R.id.tvFromDate:
                showDatePicker(tvFromDate);
                break;
            case R.id.tvToDate:
                showDatePicker(tvToDate);
                break;
            case R.id.tvApply:
                String fromDate = tvFromDate.getText().toString();
                String toDate = tvToDate.getText().toString();
                if (!fromDate.isEmpty()) {
                    if (!toDate.isEmpty()) {
                        showProgressBar(getActivity());
                        GetOrdersApiCall.serviceCallToGetOrders(getActivity(), this, fromDate, toDate);
                    } else {
                        Toast.makeText(getActivity(), getString(R.string.please_select_to_date), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(getActivity(), getString(R.string.please_select_from_date), Toast.LENGTH_SHORT).show();
                }
                break;
            default:
                break;
        }
    }

    private void showDatePicker(TextView textView) {

        Calendar calendar = Calendar.getInstance();
        Calendar date = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        datePickerDialog = new DatePickerDialog(getActivity(), R.style.DateAndTimePicker, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                date.set(year, monthOfYear, dayOfMonth);
                String date = year + "-" + ((monthOfYear + 1) < 10 ? ("0" + (monthOfYear + 1)) : ((monthOfYear + 1))) + "-" + (dayOfMonth < 10 ? ("0" + dayOfMonth) : (dayOfMonth));
                //String date = (dayOfMonth < 10 ? ("0" + dayOfMonth) : (dayOfMonth)) + "-" + ((monthOfYear + 1) < 10 ? ("0" + (monthOfYear + 1)) : ((monthOfYear + 1))) + "-" + year;
                textView.setText(date);
            }
        }, year, month, day);
        datePickerDialog.show();
    }
}
