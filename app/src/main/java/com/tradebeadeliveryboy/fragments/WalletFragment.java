package com.tradebeadeliveryboy.fragments;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.tradebeadeliveryboy.ApiCalls.GetTransactionDetails;
import com.tradebeadeliveryboy.ApiCalls.GetWalletBalance;
import com.tradebeadeliveryboy.R;
import com.tradebeadeliveryboy.activities.EarningsFloatingsTransactionsActivity;
import com.tradebeadeliveryboy.activities.TransactionDetailsFilterActivity;
import com.tradebeadeliveryboy.adapters.TransactionDetailsAdapter;
import com.tradebeadeliveryboy.interfaces.HttpReqResCallBack;
import com.tradebeadeliveryboy.interfaces.TransactionDetailsFilterCallBack;
import com.tradebeadeliveryboy.models.responseModels.getTransactionDetailsResponse.GetTransactionDetailsResponse;
import com.tradebeadeliveryboy.models.responseModels.getTransactionDetailsResponse.TransactionDetails;
import com.tradebeadeliveryboy.models.responseModels.getTransactionDetailsResponse.TransactionDetailsData;
import com.tradebeadeliveryboy.models.responseModels.walletBalanceResponse.AccountDetails;
import com.tradebeadeliveryboy.models.responseModels.walletBalanceResponse.WalletBalanceResponse;
import com.tradebeadeliveryboy.models.responseModels.walletBalanceResponse.WalletData;
import com.tradebeadeliveryboy.utils.Constants;
import com.tradebeadeliveryboy.utils.PreferenceConnector;
import com.tradebeadeliveryboy.utils.UserData;

import java.util.LinkedList;

public class WalletFragment extends BaseFragment implements View.OnClickListener, HttpReqResCallBack, TransactionDetailsFilterCallBack {

    private RecyclerView rvTransactionDetails;
    private LinearLayout llTransactionDetails, llEarnings, llFloatings;
    private TextView tvEarnings, tvFloatings, tvFilter, tvError, tvTransactionName, tvFloatingsKey, tvEarningsKey;

    private LinkedList<TransactionDetails> listOfTransactionDetails;

    private String startDate = "";
    private String endDate = "";
    private String lastDays = "30";
    private String selectedTransaction = "";

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.layout_wallet_fragment, container, false);
        initializeUi(view);
        initializeListeners();
        prepareDetails();
        return view;
    }

    private void initializeUi(View view) {
        tvError = view.findViewById(R.id.tvError);
        tvFilter = view.findViewById(R.id.tvFilter);
        tvEarnings = view.findViewById(R.id.tvEarnings);
        tvFloatings = view.findViewById(R.id.tvFloatings);
        llEarnings = view.findViewById(R.id.llEarnings);
        llFloatings = view.findViewById(R.id.llFloatings);
        tvEarningsKey = view.findViewById(R.id.tvEarningsKey);
        tvFloatingsKey = view.findViewById(R.id.tvFloatingsKey);
        tvTransactionName = view.findViewById(R.id.tvTransactionName);
        llTransactionDetails = view.findViewById(R.id.llTransactionDetails);
        rvTransactionDetails = view.findViewById(R.id.rvTransactionDetails);
    }

    private void initializeListeners() {
        tvFilter.setOnClickListener(this);

        tvEarnings.setOnClickListener(this);
        tvFloatings.setOnClickListener(this);

        llEarnings.setOnClickListener(this);
        llFloatings.setOnClickListener(this);
    }

    private void prepareDetails() {
        showProgressBar(getActivity());
        String token = PreferenceConnector.readString(getActivity(), getString(R.string.user_token), "");
        GetWalletBalance.serviceCallToGetWalletBalance(getActivity(), WalletFragment.this, null, token);
    }

    @Override
    public void onResume() {
        super.onResume();
        UserData.getInstance().setWalletFragment(this);
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        if (id == R.id.tvFilter) {
            prepareFilterDetails();
        } else if (id == R.id.tvEarnings) {
            selectedTransaction = "1";
            tvEarningsKey.setTextColor(Color.parseColor("#F87C32"));
            tvFloatingsKey.setTextColor(Color.parseColor("#000000"));
            tvTransactionName.setText(getString(R.string.earning_transactions));
            prepareTransactionDetails(selectedTransaction);
        } else if (id == R.id.llEarnings) {
            selectedTransaction = "1";
            tvEarningsKey.setTextColor(Color.parseColor("#F87C32"));
            tvFloatingsKey.setTextColor(Color.parseColor("#000000"));
            tvTransactionName.setText(getString(R.string.earning_transactions));
            prepareTransactionDetails(selectedTransaction);
        } else if (id == R.id.tvFloatings) {
            selectedTransaction = "2";
            tvEarningsKey.setTextColor(Color.parseColor("#000000"));
            tvFloatingsKey.setTextColor(Color.parseColor("#F87C32"));
            tvTransactionName.setText(getString(R.string.floating_transactions));
            prepareTransactionDetails(selectedTransaction);
        } else if (id == R.id.llFloatings) {
            selectedTransaction = "2";
            tvEarningsKey.setTextColor(Color.parseColor("#000000"));
            tvFloatingsKey.setTextColor(Color.parseColor("#F87C32"));
            tvTransactionName.setText(getString(R.string.floating_transactions));
            prepareTransactionDetails(selectedTransaction);
        }
    }

    private void prepareEarningFloatings(String comingFrom) {
        Intent earningFloatingsIntent = new Intent(getActivity(), EarningsFloatingsTransactionsActivity.class);
        Bundle bundle = new Bundle();
        bundle.putString(getString(R.string.coming_from), comingFrom);
        earningFloatingsIntent.putExtras(bundle);
        startActivity(earningFloatingsIntent);
    }

    private void prepareFilterDetails() {
        Intent transactionDetailsFilterIntent = new Intent(getActivity(), TransactionDetailsFilterActivity.class);
        startActivity(transactionDetailsFilterIntent);
    }

    @Override
    public void jsonResponseReceived(String jsonResponse, int statusCode, int requestType) {
        if (requestType == Constants.SERVICE_CALL_TO_GET_WALLET_BALANCE) {
            if (jsonResponse != null) {
                WalletBalanceResponse walletBalanceResponse = new Gson().fromJson(jsonResponse, WalletBalanceResponse.class);
                if (walletBalanceResponse != null) {
                    boolean status = walletBalanceResponse.getStatus();
                    if (status) {
                        WalletData walletData = walletBalanceResponse.getWalletData();
                        if (walletData != null) {
                            AccountDetails accountDetails = walletData.getAccountDetails();
                            if (accountDetails != null) {
                                int wallet = accountDetails.getWallet();
                                int floatingWallet = accountDetails.getFloatingWallet();

                                tvEarnings.setText("Rs " + "" + wallet + "/-");
                                tvFloatings.setText("Rs " + "" + floatingWallet + "/-");
                            }
                        }
                    }
                }
            }
            closeProgressbar();
            prepareTransactionDetails("");
        } else if (requestType == Constants.SERVICE_CALL_TO_GET_TRANSACTION_DETAILS) {
            if (jsonResponse != null) {
                GetTransactionDetailsResponse getTransactionDetailsResponse = new Gson().fromJson(jsonResponse, GetTransactionDetailsResponse.class);
                if (getTransactionDetailsResponse != null) {
                    boolean status = getTransactionDetailsResponse.getStatus();
                    if (status) {
                        TransactionDetailsData transactionDetailsData = getTransactionDetailsResponse.getTransactionDetailsData();
                        if (transactionDetailsData != null) {
                            listOfTransactionDetails = transactionDetailsData.getListOfTransactionDetails();
                            if (listOfTransactionDetails != null) {
                                if (listOfTransactionDetails.size() != 0) {
                                    listIsFull();
                                    initializeAdapter();
                                } else {
                                    listIsEmpty();
                                }
                            } else {
                                listIsEmpty();
                            }
                        }
                    } else {
                        listIsEmpty();
                    }
                }
            }
            closeProgressbar();
        }
    }

    private void initializeAdapter() {
        TransactionDetailsAdapter transactionDetailsAdapter = new TransactionDetailsAdapter(getActivity(), listOfTransactionDetails);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
        rvTransactionDetails.setLayoutManager(layoutManager);
        rvTransactionDetails.setItemAnimator(new DefaultItemAnimator());
        rvTransactionDetails.setAdapter(transactionDetailsAdapter);
    }

    private void prepareTransactionDetails(String status) {
        showProgressBar(getActivity());
        String token = PreferenceConnector.readString(getActivity(), getString(R.string.user_token), "");
        GetTransactionDetails.serviceCallToGetTransactionDetails(getActivity(), WalletFragment.this, null, token, startDate, endDate, lastDays, status);
    }

    private void listIsFull() {
        tvError.setVisibility(View.GONE);
        rvTransactionDetails.setVisibility(View.VISIBLE);
    }

    private void listIsEmpty() {
        tvError.setVisibility(View.VISIBLE);
        rvTransactionDetails.setVisibility(View.GONE);
    }

    @Override
    public void transactionDetailsFilter(String startDate, String endDate, String lastDays) {
        this.startDate = startDate;
        this.endDate = endDate;
        this.lastDays = lastDays;
        prepareTransactionDetails(selectedTransaction);
    }
}
