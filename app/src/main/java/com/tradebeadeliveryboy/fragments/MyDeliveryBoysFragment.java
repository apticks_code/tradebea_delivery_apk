package com.tradebeadeliveryboy.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.tradebeadeliveryboy.ApiCalls.MyDeliveryBoyDetailsApiCall;
import com.tradebeadeliveryboy.R;
import com.tradebeadeliveryboy.adapters.MyDeliveryBoysAdapter;
import com.tradebeadeliveryboy.interfaces.HttpReqResCallBack;
import com.tradebeadeliveryboy.models.responseModels.myDeliveryBoysResponse.MyDeliveryBoyDetails;
import com.tradebeadeliveryboy.models.responseModels.myDeliveryBoysResponse.MyDeliveryBoysResponse;
import com.tradebeadeliveryboy.utils.Constants;
import com.tradebeadeliveryboy.utils.PreferenceConnector;

import java.util.LinkedList;

public class MyDeliveryBoysFragment extends BaseFragment implements HttpReqResCallBack {

    private TextView tvError;
    private RecyclerView rvMyDeliveryBoyDetails;
    private LinkedList<MyDeliveryBoyDetails> listOfMyDeliveryBoyDetails;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_my_delivery_boy, container, false);
        initializeUi(view);
        prepareDetails();
        return view;
    }

    private void initializeUi(View view) {
        tvError = view.findViewById(R.id.tvError);
        rvMyDeliveryBoyDetails = view.findViewById(R.id.rvMyDeliveryBoyDetails);
    }

    private void prepareDetails() {
        showProgressBar(getActivity());
        String token = PreferenceConnector.readString(getActivity(), getString(R.string.user_token), "");
        MyDeliveryBoyDetailsApiCall.serviceCallToGetMyDeliveryBoyDetails(getActivity(), this, null, token);
    }

    @Override
    public void jsonResponseReceived(String jsonResponse, int statusCode, int requestType) {
        if (requestType == Constants.SERVICE_CALL_TO_GET_MY_DELIVERY_BOY_DETAILS) {
            if (jsonResponse != null) {
                MyDeliveryBoysResponse myDeliveryBoysResponse = new Gson().fromJson(jsonResponse, MyDeliveryBoysResponse.class);
                if (myDeliveryBoysResponse != null) {
                    boolean status = myDeliveryBoysResponse.getStatus();
                    if (status) {
                        listOfMyDeliveryBoyDetails = myDeliveryBoysResponse.getListOfMyDeliveryBoyDetails();
                        if (listOfMyDeliveryBoyDetails != null) {
                            if (listOfMyDeliveryBoyDetails.size() != 0) {
                                listIsFull();
                                initializeAdapter();
                            } else {
                                listIsEmpty();
                            }
                        } else {
                            listIsEmpty();
                        }
                    } else {
                        listIsEmpty();
                        Toast.makeText(getActivity(), getString(R.string.status_error), Toast.LENGTH_SHORT).show();
                    }
                }
            }
            closeProgressbar();
        }
    }

    private void initializeAdapter() {
        MyDeliveryBoysAdapter myDeliveryBoysAdapter = new MyDeliveryBoysAdapter(getActivity(), listOfMyDeliveryBoyDetails);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
        rvMyDeliveryBoyDetails.setLayoutManager(layoutManager);
        rvMyDeliveryBoyDetails.setItemAnimator(new DefaultItemAnimator());
        rvMyDeliveryBoyDetails.setAdapter(myDeliveryBoysAdapter);
    }

    private void listIsFull() {
        tvError.setVisibility(View.GONE);
        rvMyDeliveryBoyDetails.setVisibility(View.VISIBLE);
    }

    private void listIsEmpty() {
        tvError.setVisibility(View.VISIBLE);
        rvMyDeliveryBoyDetails.setVisibility(View.GONE);
    }
}
