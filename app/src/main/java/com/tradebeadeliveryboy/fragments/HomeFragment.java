package com.tradebeadeliveryboy.fragments;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.gson.Gson;
import com.tradebeadeliveryboy.ApiCalls.DashboardDetailsApiCall;
import com.tradebeadeliveryboy.ApiCalls.DeviceTokenApiCall;
import com.tradebeadeliveryboy.ApiCalls.GetOrdersApiCall;
import com.tradebeadeliveryboy.ApiCalls.LoginDetailsApiCall;
import com.tradebeadeliveryboy.R;
import com.tradebeadeliveryboy.activities.NotificationActivity;
import com.tradebeadeliveryboy.activities.OrderDetailsActivity;
import com.tradebeadeliveryboy.interfaces.HttpReqResCallBack;
import com.tradebeadeliveryboy.interfaces.OrdersNavigationCallBack;
import com.tradebeadeliveryboy.models.responseModels.dashboardDetailsResponse.DashboardData;
import com.tradebeadeliveryboy.models.responseModels.dashboardDetailsResponse.DashboardDetailsResponse;
import com.tradebeadeliveryboy.models.responseModels.dashboardDetailsResponse.DashboardLocation;
import com.tradebeadeliveryboy.models.responseModels.dashboardDetailsResponse.DashboardOnGoingOrder;
import com.tradebeadeliveryboy.models.responseModels.dashboardDetailsResponse.DeliverySuperintendent;
import com.tradebeadeliveryboy.models.responseModels.dashboardDetailsResponse.TradebeaUser;
import com.tradebeadeliveryboy.models.responseModels.deviceTokenResponse.DeviceTokenResponse;
import com.tradebeadeliveryboy.models.responseModels.orderDetailsResponse.DeliveryJobs;
import com.tradebeadeliveryboy.utils.Constants;
import com.tradebeadeliveryboy.utils.PreferenceConnector;
import com.tradebeadeliveryboy.utils.UserData;

import java.util.LinkedList;

public class HomeFragment extends BaseFragment implements View.OnClickListener, HttpReqResCallBack {

    private LinearLayout llOnGoingOrders;
    private SwipeRefreshLayout swipeRefreshLayout;
    private TextView tvOrderDate, tvOrderId, tvOrderStatus, tvPreparationTime, tvTravelDistance, tvTodayEarnings, tvFloatingCash, tvTodayDeliveries, tvTotalDeliveries;

    private LinkedList<DeliveryJobs> listOfDeliveryJobs;

    private String token = "";

    private int orderId = 0;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.layout_home_fragment, container, false);
        initializeUi(view);
        initializeListeners();
        refreshContent();
        prepareDetails();
        return view;
    }

    private void initializeUi(View view) {
        tvOrderId = view.findViewById(R.id.tvOrderId);
        tvOrderDate = view.findViewById(R.id.tvOrderDate);
        tvOrderStatus = view.findViewById(R.id.tvOrderStatus);
        tvFloatingCash = view.findViewById(R.id.tvFloatingCash);
        tvTodayEarnings = view.findViewById(R.id.tvTodayEarnings);
        llOnGoingOrders = view.findViewById(R.id.llOnGoingOrders);
        tvTravelDistance = view.findViewById(R.id.tvTravelDistance);
        tvPreparationTime = view.findViewById(R.id.tvPreparationTime);
        tvTodayDeliveries = view.findViewById(R.id.tvTodayDeliveries);
        tvTotalDeliveries = view.findViewById(R.id.tvTotalDeliveries);
        swipeRefreshLayout = view.findViewById(R.id.swipeRefreshLayout);
    }

    private void initializeListeners() {
        llOnGoingOrders.setOnClickListener(this);
        PreferenceConnector.writeBoolean(getActivity(), getString(R.string.refresh_dashboard), false);
    }

    private void refreshContent() {
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeRefreshLayout.setRefreshing(false);
                prepareDetails();
            }
        });
    }

    private void prepareDetails() {
        preparePushNotificationDetails();
        serviceCallToGetDashboardDetails();
    }

    private void preparePushNotificationDetails() {
        token = PreferenceConnector.readString(getActivity(), getString(R.string.user_token), "");
        String deviceToken = PreferenceConnector.readString(getActivity(), getString(R.string.device_token), "");
        new Thread(new Runnable() {
            @Override
            public void run() {
                if (!deviceToken.isEmpty()) {
                    DeviceTokenApiCall.serviceCallForDeviceToken(getActivity(), HomeFragment.this, null, deviceToken, token);
                }
            }
        }).start();
    }

    @Override
    public void onResume() {
        super.onResume();
        boolean refreshDashboard = PreferenceConnector.readBoolean(getActivity(), getString(R.string.refresh_dashboard), false);
        if (refreshDashboard) {
            serviceCallToGetDashboardDetails();
            PreferenceConnector.writeBoolean(getActivity(), getString(R.string.refresh_dashboard), false);
        }
    }

    private void serviceCallToGetDashboardDetails() {
        showProgressBar(getActivity());
        DashboardDetailsApiCall.serviceCallToGetDashboardDetails(getActivity(), this);
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        if (id == R.id.llOnGoingOrders) {
            if (getActivity() != null) {
                //OrdersNavigationCallBack ordersNavigationCallBack = (OrdersNavigationCallBack) getActivity();
                //ordersNavigationCallBack.orderNavigation();
                Intent orderDetailsIntent = new Intent(getActivity(), OrderDetailsActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString(getActivity().getString(R.string.order_id), String.valueOf(orderId));
                orderDetailsIntent.putExtras(bundle);
                startActivity(orderDetailsIntent);
            }
        }
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void jsonResponseReceived(String jsonResponse, int statusCode, int requestType) {
        switch (requestType) {
            case Constants.SERVICE_CALL_TO_GET_DASHBOARD_DETAILS:
                if (jsonResponse != null) {
                    DashboardDetailsResponse dashboardDetailsResponse = new Gson().fromJson(jsonResponse, DashboardDetailsResponse.class);
                    if (dashboardDetailsResponse != null) {
                        Boolean status = dashboardDetailsResponse.getStatus();
                        String message = dashboardDetailsResponse.getMessage();
                        if (status) {
                            DashboardData dashboardData = dashboardDetailsResponse.getDashboardData();
                            if (dashboardData != null) {
                                DashboardOnGoingOrder dashboardOnGoingOrder = dashboardData.getDashboardOnGoingOrder();
                                if (dashboardData.getDeliverySuperintendent() != null) {
                                    DeliverySuperintendent deliverySuperintendent = dashboardData.getDeliverySuperintendent();
                                    UserData.getInstance().setDeliverySuperintendent(deliverySuperintendent);
                                }
                                if (dashboardData.getTradebeaUser() != null) {
                                    TradebeaUser tradebeaUser = dashboardData.getTradebeaUser();
                                    UserData.getInstance().setTradebeaUser(tradebeaUser);
                                }
                                Integer todayEarnings = dashboardData.getTodayEarnings();
                                Integer todayFloatingCash = dashboardData.getTodayFloatingCash();
                                Integer todayDeliveries = dashboardData.getTodayDeliveries();
                                Integer totalDeliveries = dashboardData.getTotalDeliveries();


                                if (dashboardOnGoingOrder != null) {
                                    llOnGoingOrders.setVisibility(View.VISIBLE);
                                    String date = dashboardOnGoingOrder.getDate();
                                    int preparationTime = dashboardOnGoingOrder.getPreparationTime();
                                    String trackId = dashboardOnGoingOrder.getTrackId();
                                    orderId = dashboardOnGoingOrder.getId();
                                    listOfDeliveryJobs = dashboardOnGoingOrder.getListOfDeliveryJobs();
                                    String orderStatus = dashboardOnGoingOrder.getOrderStatus().getCurrentStatus();
                                    DashboardLocation dashboardLocation = dashboardOnGoingOrder.getDashboardSellerLatLong().getDashboardLocation();

                                    tvOrderDate.setText("Date :" + " " + date);
                                    tvOrderId.setText("Track Id : " + "" + trackId);
                                    tvPreparationTime.setText("Prep Time :" + " " + preparationTime + "mins");
                                    tvOrderStatus.setText(orderStatus);
                                    tvTravelDistance.setText("Travel Dst : 0.90KMS");

                                    if (listOfDeliveryJobs != null) {
                                        if (listOfDeliveryJobs.size() != 0) {
                                            if (listOfDeliveryJobs.size() == 2) {
                                                tvPreparationTime.setVisibility(View.GONE);
                                            } else {
                                                tvPreparationTime.setVisibility(View.VISIBLE);
                                            }
                                        }
                                    }

                                    if (dashboardLocation != null) {
                                        Double latitude = dashboardLocation.getLatitude();
                                        Double longitude = dashboardLocation.getLongitude();
                                    }
                                } else {
                                    llOnGoingOrders.setVisibility(View.GONE);
                                }
                                tvTodayEarnings.setText(getString(R.string.ruppee) + " " + todayEarnings + " /-");
                                tvFloatingCash.setText(getString(R.string.ruppee) + " " + todayFloatingCash + " /-");
                                tvTodayDeliveries.setText(String.valueOf(todayDeliveries));
                                tvTotalDeliveries.setText(String.valueOf(totalDeliveries));
                            }
                        } else {
                            Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                        }
                    }
                }
                closeProgressbar();
                break;
            case Constants.SERVICE_CALL_FOR_DEVICE_TOKEN:
                if (jsonResponse != null) {
                    DeviceTokenResponse deviceTokenResponse = new Gson().fromJson(jsonResponse, DeviceTokenResponse.class);
                    if (deviceTokenResponse != null) {
                        boolean status = deviceTokenResponse.getStatus();
                        if (status) {
                            String message = deviceTokenResponse.getMessage();
                            //Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                        }
                    }
                }
                break;
            default:
                break;
        }
    }
}
