package com.tradebeadeliveryboy.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.tradebeadeliveryboy.R;
import com.tradebeadeliveryboy.utils.PreferenceConnector;

public class ReferralPageFragment extends BaseFragment implements View.OnClickListener {

    private ImageView ivShare;
    private LinearLayout llShare;
    private TextView tvShare, tvReferralId;
    private String uniqueId = "";

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_referral_page, container, false);
        initializeUi(view);
        initializeListeners();
        setUniqueId();
        return view;
    }

    private void initializeUi(View view) {
        llShare = view.findViewById(R.id.llShare);
        tvShare = view.findViewById(R.id.tvShare);
        ivShare = view.findViewById(R.id.ivShare);
        tvReferralId = view.findViewById(R.id.tvReferralId);
    }

    private void initializeListeners() {
        ivShare.setOnClickListener(this);
        llShare.setOnClickListener(this);
        tvShare.setOnClickListener(this);
    }

    private void setUniqueId() {
        uniqueId = PreferenceConnector.readString(getActivity(), getString(R.string.pref_unique_id), "");
        if (uniqueId != null) {
            if (!uniqueId.isEmpty()) {
                tvReferralId.setText(uniqueId);
            } else {
                tvReferralId.setText(getString(R.string.empty));
            }
        } else {
            tvReferralId.setText(getString(R.string.empty));
        }
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        if (id == R.id.llShare) {
            prepareShareDetails();
        } else if (id == R.id.tvShare) {
            prepareShareDetails();
        } else if (id == R.id.ivShare) {
            prepareShareDetails();
        }
    }

    private void prepareShareDetails() {
        Intent shareIntent = new Intent(Intent.ACTION_SEND);
        shareIntent.setType("text/plain");
        shareIntent.putExtra(Intent.EXTRA_TEXT, "referral code " + uniqueId);
        shareIntent.putExtra(Intent.EXTRA_SUBJECT, "referral code");
        startActivity(Intent.createChooser(shareIntent, "Share..."));
    }
}
