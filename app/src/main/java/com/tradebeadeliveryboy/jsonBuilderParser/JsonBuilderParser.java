package com.tradebeadeliveryboy.jsonBuilderParser;

import com.google.gson.Gson;
import com.tradebeadeliveryboy.utils.CommonMethods;

import org.json.JSONException;
import org.json.JSONObject;
import java.io.Reader;
import java.util.logging.Level;
import java.util.logging.Logger;

public class JsonBuilderParser {
    /**
     * private constructor to prevent creating instance from outside of class
     */
    private JsonBuilderParser() {
    }

    /**
     * to convert java object to json object
     *
     * @param object object
     * @return json object
     */
    public static JSONObject jsonBuilder(Object object) {
        Gson gson = new Gson();
        JSONObject jsonObject = null;
        try {
            jsonObject = new JSONObject(gson.toJson(object));
        } catch (JSONException e) {
            Logger.getLogger(JsonBuilderParser.class.getName()).log(Level.SEVERE, null, e);
        }
        return jsonObject;
    }

    /**
     * parses response to model object
     *
     * @param response    response
     * @param parserModel parserModel
     * @param <T>         type
     * @return model object
     */
    public static <T extends Object> T jsonParser(JSONObject response, Class<T> parserModel) {
        Gson gson = new Gson();
        Reader reader = null;
        try {
            reader = CommonMethods.getInputStream(response.toString());
        } catch (Exception e) {
            Logger.getLogger(JsonBuilderParser.class.getName()).log(Level.SEVERE, null, e);
        }
        return gson.fromJson(reader, parserModel);
    }
}
