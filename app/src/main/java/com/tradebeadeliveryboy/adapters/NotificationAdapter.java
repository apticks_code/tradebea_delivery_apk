package com.tradebeadeliveryboy.adapters;

import android.app.Dialog;
import android.content.Context;
import android.location.Location;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.tradebeadeliveryboy.ApiCalls.AcceptOrderApiCall;
import com.tradebeadeliveryboy.R;
import com.tradebeadeliveryboy.interfaces.HttpReqResCallBack;
import com.tradebeadeliveryboy.models.responseModels.acceptOrderResponse.AcceptOrderResponse;
import com.tradebeadeliveryboy.models.responseModels.notificationsResponse.DeliveryLocation;
import com.tradebeadeliveryboy.models.responseModels.notificationsResponse.NotificationData;
import com.tradebeadeliveryboy.models.responseModels.notificationsResponse.SellerLocation;
import com.tradebeadeliveryboy.utils.Constants;
import com.tradebeadeliveryboy.utils.DialogUtils;
import com.tradebeadeliveryboy.utils.PreferenceConnector;

import java.util.LinkedList;

public class NotificationAdapter extends RecyclerView.Adapter<NotificationAdapter.ViewHolder> implements HttpReqResCallBack {

    private Context context;
    private Dialog progressDialog;

    private LinkedList<NotificationData> listOfNotificationData;

    private double latitude = 0.0;
    private double longitude = 0.0;

    public NotificationAdapter(Context context, LinkedList<NotificationData> listOfNotificationData, double latitude, double longitude) {
        this.context = context;
        this.listOfNotificationData = listOfNotificationData;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.layout_notification_items, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        NotificationData notificationData = listOfNotificationData.get(position);
        if (notificationData != null) {
            int orderId = 0;
            if (notificationData.getOrderId() != null) {
                orderId = notificationData.getOrderId();
            }
            String createdAt = notificationData.getCreatedAt();
            DeliveryLocation deliveryLocation = notificationData.getNotificationOrderDetails().getDeliveryLocation();
            SellerLocation sellerLocation = notificationData.getNotificationOrderDetails().getSellerLocation();
            double shopDistance = calculateDistanceBetweenTwoLatLngs(sellerLocation.getLatitude(), sellerLocation.getLongitude());
            double deliveryDistance = calculateDistanceBetweenTwoLatLngs(deliveryLocation.getLatitude(), deliveryLocation.getLongitude());

            if (createdAt.contains(" ")) {
                String[] createdDateTime = createdAt.split(" ");
                if (createdDateTime.length == 2) {
                    String date = createdDateTime[0];
                    holder.tvDateOfOrder.setText(date);
                } else {
                    holder.tvDateOfOrder.setText(createdAt);
                }
            } else {
                holder.tvDateOfOrder.setText(createdAt);
            }
            if (orderId != 0) {
                holder.tvOrderId.setVisibility(View.VISIBLE);
                holder.tvOrderId.setText("Order ID :" + " " + "#" + orderId);
            } else {
                holder.tvOrderId.setVisibility(View.GONE);
            }
            holder.tvShopDistance.setText("ShopDistance :" + " " + shopDistance + " " + "KMS");
            holder.tvDeliveryDistance.setText("Delivery Distance :" + " " + deliveryDistance + " " + "KMS");
        }
    }

    private double calculateDistanceBetweenTwoLatLngs(Double endLatitude, Double endLongitude) {
        Location currentLocation = new Location("locationA");
        currentLocation.setLatitude(latitude);
        currentLocation.setLongitude(longitude);
        Location destination = new Location("locationB");
        destination.setLatitude(endLatitude);
        destination.setLongitude(endLongitude);
        return currentLocation.distanceTo(destination);
    }

    @Override
    public int getItemCount() {
        return listOfNotificationData.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private TextView tvOrderId, tvShopDistance, tvDeliveryDistance, tvDateOfOrder, tvEarnings, tvReadyIn, tvAccept, tvAccepted;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            tvOrderId = itemView.findViewById(R.id.tvOrderId);
            tvShopDistance = itemView.findViewById(R.id.tvShopDistance);
            tvDeliveryDistance = itemView.findViewById(R.id.tvDeliveryDistance);
            tvDateOfOrder = itemView.findViewById(R.id.tvDateOfOrder);
            tvEarnings = itemView.findViewById(R.id.tvEarnings);
            tvReadyIn = itemView.findViewById(R.id.tvReadyIn);
            tvAccept = itemView.findViewById(R.id.tvAccept);
            tvAccepted = itemView.findViewById(R.id.tvAccepted);

            tvAccept.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            int id = view.getId();
            switch (id) {
                case R.id.tvAccept:
                    NotificationData notificationData = listOfNotificationData.get(getLayoutPosition());
                    if (notificationData != null) {
                        showProgressBar();
                        int notificationId = notificationData.getId();
                        int orderId = notificationData.getOrderId();
                        AcceptOrderApiCall.serviceCallToAcceptOrder(context, null, NotificationAdapter.this, notificationId, orderId);
                    }
                    break;
                default:
                    break;
            }
        }
    }

    @Override
    public void jsonResponseReceived(String jsonResponse, int statusCode, int requestType) {
        switch (requestType) {
            case Constants.SERVICE_CALL_TO_ACCEPT_ORDER:
                if (jsonResponse != null) {
                    AcceptOrderResponse acceptOrderResponse = new Gson().fromJson(jsonResponse, AcceptOrderResponse.class);
                    if (acceptOrderResponse != null) {
                        boolean status = acceptOrderResponse.getStatus();
                        String message = acceptOrderResponse.getMessage();
                        if (status) {
                            PreferenceConnector.writeBoolean(context, context.getString(R.string.refresh_dashboard), true);
                            Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
                        }
                    }
                }
                closeProgressbar();
                break;
            default:
                break;
        }
    }

    public void showProgressBar() {
        try {
            if (progressDialog != null && !progressDialog.isShowing()) {
                progressDialog = DialogUtils.getDialogUtilsInstance().progressDialog(context);
            } else {
                closeProgressbar();
                progressDialog = null;
                progressDialog = DialogUtils.getDialogUtilsInstance().progressDialog(context);
            }
        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }

    public void closeProgressbar() {
        try {
            if (progressDialog != null) {
                progressDialog.dismiss();
            }
        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }
}
