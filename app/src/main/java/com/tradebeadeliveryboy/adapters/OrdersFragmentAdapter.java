package com.tradebeadeliveryboy.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.tradebeadeliveryboy.R;
import com.tradebeadeliveryboy.activities.OrderDetailsActivity;
import com.tradebeadeliveryboy.models.responseModels.getOrderResponse.OrderData;
import com.tradebeadeliveryboy.utils.UserData;

import java.util.LinkedList;

public class OrdersFragmentAdapter extends RecyclerView.Adapter<OrdersFragmentAdapter.ViewHolder> {

    private Context context;
    private LinkedList<OrderData> listOfOrderData;

    public OrdersFragmentAdapter(Context context, LinkedList<OrderData> listOfOrderData) {
        this.context = context;
        this.listOfOrderData = listOfOrderData;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.layout_orders_fragment_items, parent, false);
        return new ViewHolder(view);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        OrderData orderData = listOfOrderData.get(position);
        String jobId = orderData.getJobId();
        String orderCurrentStatus = orderData.getOrderStatus().getCurrentStatus();
        int deliveryFee = orderData.getDeliveryFee();
        int orderId = orderData.getId();
        String trackId = orderData.getTrackId();
        String otp = orderData.getOrderPickupOtp();

        holder.tvJobId.setText(jobId);
        holder.tvOrderId.setText(trackId);
        holder.tvOrderStatus.setText(orderCurrentStatus);
        holder.tvOTP.setText("OTP :" + " " + otp);
        holder.tvOTP.setVisibility(View.GONE);
        holder.tvAmount.setText("Earnings :" + " " + deliveryFee + "/-");
    }

    @Override
    public int getItemCount() {
        return listOfOrderData.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private TextView tvOrderId, tvOrderStatus, tvAmount, tvOTP, tvJobId;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            tvOTP = itemView.findViewById(R.id.tvOTP);
            tvJobId = itemView.findViewById(R.id.tvJobId);
            tvAmount = itemView.findViewById(R.id.tvAmount);
            tvOrderId = itemView.findViewById(R.id.tvOrderId);
            tvOrderStatus = itemView.findViewById(R.id.tvOrderStatus);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            OrderData selectedOrderData = listOfOrderData.get(getLayoutPosition());
            UserData.getInstance().setSelectedOrderData(selectedOrderData);
            Intent orderDetailsIntent = new Intent(context, OrderDetailsActivity.class);
            Bundle bundle = new Bundle();
            bundle.putString(context.getString(R.string.order_id), selectedOrderData.getId().toString());
            bundle.putString(context.getString(R.string.job_id), selectedOrderData.getJobId());
            orderDetailsIntent.putExtras(bundle);
            context.startActivity(orderDetailsIntent);
        }
    }
}
