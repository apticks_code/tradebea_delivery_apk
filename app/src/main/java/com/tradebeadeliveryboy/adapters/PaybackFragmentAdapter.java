package com.tradebeadeliveryboy.adapters;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.tradebeadeliveryboy.R;
import com.tradebeadeliveryboy.activities.SenderReceiverActivity;
import com.tradebeadeliveryboy.models.responseModels.paybackResponse.PaybackData;

import java.util.LinkedList;

public class PaybackFragmentAdapter extends RecyclerView.Adapter<PaybackFragmentAdapter.ViewHolder> {

    private Context context;
    private LinkedList<PaybackData> listOfPaybackData;

    public PaybackFragmentAdapter(Context context, LinkedList<PaybackData> listOfPaybackData) {
        this.context = context;
        this.listOfPaybackData = listOfPaybackData;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.layout_payback_fragment_items, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        PaybackData paybackData = listOfPaybackData.get(position);
        setSenderUniqueId(holder, paybackData);
        setAmount(holder, paybackData);
        setTime(holder, paybackData);
    }

    private void setSenderUniqueId(ViewHolder holder, PaybackData paybackData) {
        String senderId = paybackData.getSenderUniqueId();
        holder.tvSenderId.setText(senderId);
    }

    private void setAmount(ViewHolder holder, PaybackData paybackData) {
        if (paybackData.getAmount() != null) {
            int amount = paybackData.getAmount();
            holder.tvAmount.setText(String.valueOf(amount));
        } else {
            holder.tvAmount.setText("N/A");
        }
    }

    private void setTime(ViewHolder holder, PaybackData paybackData) {
        String time = paybackData.getTime();
        holder.tvTime.setText(time);
    }

    @Override
    public int getItemCount() {
        return listOfPaybackData.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private TextView tvSenderId, tvAmount, tvTime;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tvTime = itemView.findViewById(R.id.tvTime);
            tvAmount = itemView.findViewById(R.id.tvAmount);
            tvSenderId = itemView.findViewById(R.id.tvSenderId);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            PaybackData paybackData = listOfPaybackData.get(getLayoutPosition());
            if (paybackData != null) {
                int paybackId = paybackData.getId();
                String paybackSourceId = paybackData.getPaybackSource().getId().toString();
                String paybackSourceName = paybackData.getPaybackSource().getLabel();
                int isThisMyRequest = paybackData.getIsThisMyReq();
                Integer otp = paybackData.getSuperintendentOtp();
                String otpString = "";
                if (otp != null) {
                    otpString = otp.toString();
                }
                Intent senderReceiverIntent = new Intent(context, SenderReceiverActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString(context.getString(R.string.pay_back_id), String.valueOf(paybackId));
                bundle.putString(context.getString(R.string.is_this_my_request), String.valueOf(isThisMyRequest));
                bundle.putString(context.getString(R.string.pay_back_source_name), paybackSourceName);
                bundle.putString(context.getString(R.string.pay_back_source_id), paybackSourceId);
                bundle.putString(context.getString(R.string.otp), otpString);
                senderReceiverIntent.putExtras(bundle);
                context.startActivity(senderReceiverIntent);
            }
        }
    }
}
