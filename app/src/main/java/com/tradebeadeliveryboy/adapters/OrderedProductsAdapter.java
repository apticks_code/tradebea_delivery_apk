package com.tradebeadeliveryboy.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.makeramen.roundedimageview.RoundedTransformationBuilder;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Transformation;
import com.tradebeadeliveryboy.R;
import com.tradebeadeliveryboy.models.responseModels.orderDetailsResponse.OrderDetail;
import com.tradebeadeliveryboy.models.responseModels.orderDetailsResponse.Product;
import com.tradebeadeliveryboy.models.responseModels.orderDetailsResponse.ProductImage;

import java.util.LinkedList;

public class OrderedProductsAdapter extends RecyclerView.Adapter<OrderedProductsAdapter.ViewHolder> {

    private Context context;
    private Transformation transformation;
    private LinkedList<OrderDetail> listOfOrderDetails;

    public OrderedProductsAdapter(Context context, LinkedList<OrderDetail> listOfOrderDetails) {
        this.context = context;
        this.listOfOrderDetails = listOfOrderDetails;

        transformation = new RoundedTransformationBuilder()
                .cornerRadius(0, 5)
                .cornerRadius(1, 5)
                .borderColor(Color.parseColor("#00000000"))
                .borderWidthDp(1)
                .oval(false)
                .build();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.layout_ordered_product_items, parent, false);
        return new ViewHolder(view);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        OrderDetail orderDetail = listOfOrderDetails.get(position);

        int orderId = orderDetail.getOrderId();
        int quantity = orderDetail.getQty();
        float totalPrice = orderDetail.getTotal();
        float subTotal = orderDetail.getSubTotal();
        Product product = orderDetail.getProduct();
        String productName = product.getName();
        LinkedList<ProductImage> listOfProductImages = product.getListOfProductImages();
        if (listOfProductImages != null) {
            if (listOfProductImages.size() != 0) {
                ProductImage productImage = listOfProductImages.get(0);
                String imageUrl = productImage.getImage();
                setProductImage(holder, imageUrl);
            }
        }

        holder.tvProductName.setText(productName);
        holder.tvProductDesc.setText("N/A");
        holder.tvQuantity.setText("Quantity :" + " " + quantity);
        holder.tvOriginalPrice.setText(context.getString(R.string.current_currency) + " " + subTotal);
        holder.tvDiscountedPrice.setText(context.getString(R.string.current_currency) + " " + totalPrice);
    }

    private void setProductImage(ViewHolder holder, String imageUrl) {
        if (imageUrl != null) {
            if (!imageUrl.isEmpty()) {
                Picasso.get()
                        .load(imageUrl)
                        .error(R.drawable.image_placeholder)
                        .placeholder(R.drawable.image_placeholder)
                        .transform(transformation)
                        .fit().centerCrop()
                        .into(holder.ivProductPic);
            } else {
                Picasso.get()
                        .load(R.drawable.image_placeholder)
                        .error(R.drawable.image_placeholder)
                        .placeholder(R.drawable.image_placeholder)
                        .transform(transformation)
                        .fit().centerCrop()
                        .into(holder.ivProductPic);
            }
        } else {
            Picasso.get()
                    .load(R.drawable.image_placeholder)
                    .error(R.drawable.image_placeholder)
                    .placeholder(R.drawable.image_placeholder)
                    .transform(transformation)
                    .fit().centerCrop()
                    .into(holder.ivProductPic);
        }
    }

    @Override
    public int getItemCount() {
        return listOfOrderDetails.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private ImageView ivProductPic;
        private TextView tvProductName, tvProductDesc, tvDiscountedPrice, tvOriginalPrice, tvQuantity;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            tvQuantity = itemView.findViewById(R.id.tvQuantity);
            ivProductPic = itemView.findViewById(R.id.ivProductPic);
            tvProductName = itemView.findViewById(R.id.tvProductName);
            tvProductDesc = itemView.findViewById(R.id.tvProductDesc);
            tvOriginalPrice = itemView.findViewById(R.id.tvOriginalPrice);
            tvDiscountedPrice = itemView.findViewById(R.id.tvDiscountedPrice);
        }
    }
}
