package com.tradebeadeliveryboy.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.tradebeadeliveryboy.R;
import com.tradebeadeliveryboy.models.responseModels.getTransactionDetailsResponse.TransactionDetails;

import java.util.LinkedList;

public class TransactionDetailsAdapter extends RecyclerView.Adapter<TransactionDetailsAdapter.ViewHolder> {

    private Context context;
    private LinkedList<TransactionDetails> listOfTransactionDetails;
    private LinkedList<Integer> listOfSelectedPositions;

    public TransactionDetailsAdapter(Context context, LinkedList<TransactionDetails> listOfTransactionDetails) {
        this.context = context;
        this.listOfTransactionDetails = listOfTransactionDetails;
        listOfSelectedPositions = new LinkedList<>();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context).inflate(R.layout.layout_transaction_details_item, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        TransactionDetails transactionDetails = listOfTransactionDetails.get(position);

        String transactionId = transactionDetails.getTxnId();
        String orderTrackId = transactionDetails.getOrderTrackId();
        int amount = transactionDetails.getAmount();
        int balance = transactionDetails.getBalance();
        String type = transactionDetails.getType();
        String message = transactionDetails.getMessage();
        String createdAt = transactionDetails.getCreatedAt();
        String walletType = transactionDetails.getWalletType();

        setTransactionId(holder, transactionId);
        setOrderTrackId(holder, orderTrackId);
        setAmount(holder, amount);
        setBalance(holder, balance);
        setType(holder, type);
        setMessage(holder, message);
        setWalletType(holder, walletType);
        setTransactionDate(holder, createdAt);
        setMoreLessDetails(holder, position);
    }

    private void setTransactionId(ViewHolder holder, String transactionId) {
        if (transactionId != null) {
            if (!transactionId.isEmpty()) {
                holder.tvTransactionId.setText(transactionId);
            } else {
                holder.tvTransactionId.setText("NA");
            }
        } else {
            holder.tvTransactionId.setText("NA");
        }
    }

    private void setOrderTrackId(ViewHolder holder, String orderTrackId) {
        if (orderTrackId != null) {
            if (!orderTrackId.isEmpty()) {
                holder.tvTrackId.setText(orderTrackId);
            } else {
                holder.tvTrackId.setText("NA");
            }
        } else {
            holder.tvTrackId.setText("NA");
        }
    }

    private void setAmount(ViewHolder holder, int amount) {
        holder.tvAmount.setText(String.valueOf(amount));
    }

    private void setBalance(ViewHolder holder, int balance) {
        holder.tvBalance.setText(String.valueOf(balance));
    }

    private void setType(ViewHolder holder, String type) {
        if (type != null) {
            if (!type.isEmpty()) {
                holder.tvType.setText(type);
            } else {
                holder.tvType.setText("NA");
            }
        } else {
            holder.tvType.setText("NA");
        }
    }

    private void setMessage(ViewHolder holder, String message) {
        if (message != null) {
            if (!message.isEmpty()) {
                holder.tvMessage.setText(message);
            } else {
                holder.tvMessage.setText("NA");
            }
        } else {
            holder.tvMessage.setText("NA");
        }
    }

    private void setWalletType(ViewHolder holder, String walletType) {
        if (walletType != null) {
            if (!walletType.isEmpty()) {
                holder.tvWalletType.setText(walletType);
            } else {
                holder.tvWalletType.setText("NA");
            }
        } else {
            holder.tvWalletType.setText("NA");
        }
    }

    private void setTransactionDate(ViewHolder holder, String createdAt) {
        if (createdAt != null) {
            if (!createdAt.isEmpty()) {
                holder.tvCreatedDate.setText(createdAt);
            } else {
                holder.tvCreatedDate.setText("NA");
            }
        } else {
            holder.tvCreatedDate.setText("NA");
        }
    }

    private void setMoreLessDetails(ViewHolder holder, int position) {
        holder.tvMore.setTag(holder);
        if (listOfSelectedPositions.contains(position)) {
            holder.llContainer.setVisibility(View.VISIBLE);
            holder.tvMore.setText(context.getString(R.string.less));
            holder.tvMore.setCompoundDrawablesRelativeWithIntrinsicBounds(
                    0,
                    0,
                    R.drawable.ic_drop_up,
                    0
            );
        } else {
            holder.llContainer.setVisibility(View.GONE);
            holder.tvMore.setText(context.getString(R.string.more));
            holder.tvMore.setCompoundDrawablesRelativeWithIntrinsicBounds(
                    0,
                    0,
                    R.drawable.ic_drop_down,
                    0
            );
        }
    }

    @Override
    public int getItemCount() {
        return listOfTransactionDetails.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private LinearLayout llContainer;
        private TextView tvTransactionId, tvTrackId, tvMore, tvAmount, tvBalance, tvType, tvMessage, tvCreatedDate, tvWalletType;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            tvType = itemView.findViewById(R.id.tvType);
            tvMore = itemView.findViewById(R.id.tvMore);
            tvAmount = itemView.findViewById(R.id.tvAmount);
            tvBalance = itemView.findViewById(R.id.tvBalance);
            tvTrackId = itemView.findViewById(R.id.tvTrackId);
            tvMessage = itemView.findViewById(R.id.tvMessage);
            llContainer = itemView.findViewById(R.id.llContainer);
            tvWalletType = itemView.findViewById(R.id.tvWalletType);
            tvCreatedDate = itemView.findViewById(R.id.tvCreatedDate);
            tvTransactionId = itemView.findViewById(R.id.tvTransactionId);

            tvMore.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            int id = view.getId();
            if (id == R.id.tvMore) {
                ViewHolder selectedHolder = (ViewHolder) view.getTag();
                viewHideMoreDetails(selectedHolder, getLayoutPosition());
            }
        }
    }

    private void viewHideMoreDetails(ViewHolder selectedHolder, int position) {
        if (selectedHolder.llContainer.getVisibility() == View.VISIBLE) {
            selectedHolder.llContainer.setVisibility(View.GONE);
            if (listOfSelectedPositions.contains(position)) {
                int idx = listOfSelectedPositions.indexOf(position);
                if (idx != -1) {
                    listOfSelectedPositions.remove(idx);
                }
            }
            selectedHolder.tvMore.setText(context.getString(R.string.more));
            selectedHolder.tvMore.setCompoundDrawablesRelativeWithIntrinsicBounds(
                    0,
                    0,
                    R.drawable.ic_drop_down,
                    0
            );
        } else {
            selectedHolder.llContainer.setVisibility(View.VISIBLE);
            listOfSelectedPositions.add(position);
            selectedHolder.tvMore.setText(context.getString(R.string.less));
            selectedHolder.tvMore.setCompoundDrawablesRelativeWithIntrinsicBounds(
                    0,
                    0,
                    R.drawable.ic_drop_up,
                    0
            );
        }
    }
}
