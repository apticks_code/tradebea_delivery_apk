package com.tradebeadeliveryboy.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.tradebeadeliveryboy.R;

import java.util.LinkedList;

public class CustomArrayAdapterForSpinner extends ArrayAdapter<String> {

    public LayoutInflater inflater;
    public LinkedList<String> listOfData;
    public int resourceId;

    public CustomArrayAdapterForSpinner(Context context, int resourceId, LinkedList<String> objects) {
        super(context, resourceId, objects);
        listOfData = objects;
        this.resourceId = resourceId;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public View getDropDownView(int position, View convertView, @NonNull ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }


    @NonNull
    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }

    private View getCustomView(int position, View convertView, ViewGroup parent) {
        if (convertView == null)
            convertView = inflater.inflate(resourceId, parent, false);
        TextView tvTypes = convertView.findViewById(R.id.tvTypes);
        if (position == 0) {
            tvTypes.setTextColor(getContext().getResources().getColor(R.color.search_font));
        } else {
            tvTypes.setTextColor(getContext().getResources().getColor(R.color.black));
        }
        tvTypes.setText(listOfData.get(position));
        return convertView;
    }
}