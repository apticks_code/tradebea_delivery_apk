package com.tradebeadeliveryboy.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.tradebeadeliveryboy.R;
import com.tradebeadeliveryboy.activities.DeliverySuperintendentDashboardActivity;
import com.tradebeadeliveryboy.models.responseModels.myDeliveryBoysResponse.MyDeliveryBoyDetails;
import com.tradebeadeliveryboy.models.responseModels.myDeliveryBoysResponse.UserDetails;

import java.util.LinkedList;

public class MyDeliveryBoysAdapter extends RecyclerView.Adapter<MyDeliveryBoysAdapter.ViewHolder> {

    private Context context;
    private LinkedList<MyDeliveryBoyDetails> listOfMyDeliveryBoyDetails;
    private int selectedPosition = -1;

    public MyDeliveryBoysAdapter(Context context, LinkedList<MyDeliveryBoyDetails> listOfMyDeliveryBoyDetails) {
        this.context = context;
        this.listOfMyDeliveryBoyDetails = listOfMyDeliveryBoyDetails;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.layout_my_delivery_boy_items, parent, false);
        return new ViewHolder(view);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        MyDeliveryBoyDetails myDeliveryBoyDetails = listOfMyDeliveryBoyDetails.get(position);
        int status = myDeliveryBoyDetails.getStatus();
        UserDetails userDetails = myDeliveryBoyDetails.getUserDetails();
        if (userDetails != null) {
            String firstName = userDetails.getFirstName();
            String uniqueId = userDetails.getUniqueId();
            Long mobile = userDetails.getMobile();
            String email = userDetails.getEmail();

            setDeliveryBoyName(holder, firstName);
            setUniqueCode(holder, uniqueId);
            setMobileNumber(holder, mobile.toString());
            setEmail(holder, email);
        } else {
            setDeliveryBoyName(holder, "NA");
            setUniqueCode(holder, "NA");
            setMobileNumber(holder, "NA");
            setEmail(holder, "NA");
        }
        setStatus(holder, status);
    }

    private void setStatus(ViewHolder holder, int status) {
        if (status == 1) {
            holder.tvStatus.setText(context.getString(R.string.online));
        } else {
            holder.tvStatus.setText(context.getString(R.string.offline));
        }
    }

    @SuppressLint("SetTextI18n")
    private void setDeliveryBoyName(ViewHolder holder, String firstName) {
        if (firstName != null) {
            if (!firstName.isEmpty()) {
                holder.tvDeliveryBoyName.setText(firstName);
            } else {
                holder.tvDeliveryBoyName.setText("NA");
            }
        } else {
            holder.tvDeliveryBoyName.setText("NA");
        }
    }

    @SuppressLint("SetTextI18n")
    private void setUniqueCode(ViewHolder holder, String uniqueId) {
        if (uniqueId != null) {
            if (!uniqueId.isEmpty()) {
                holder.tvUniqueCode.setText(uniqueId);
            } else {
                holder.tvDeliveryBoyName.setText("NA");
            }
        } else {
            holder.tvDeliveryBoyName.setText("NA");
        }
    }

    @SuppressLint("SetTextI18n")
    private void setMobileNumber(ViewHolder holder, String mobile) {
        if (mobile != null) {
            if (!mobile.isEmpty()) {
                holder.tvMobileNumber.setText(mobile);
            } else {
                holder.tvMobileNumber.setText("NA");
            }
        } else {
            holder.tvMobileNumber.setText("NA");
        }
    }

    @SuppressLint("SetTextI18n")
    private void setEmail(ViewHolder holder, String email) {
        if (email != null) {
            if (!email.isEmpty()) {
                holder.tvEmail.setText(email);
            } else {
                holder.tvDeliveryBoyName.setText("NA");
            }
        } else {
            holder.tvDeliveryBoyName.setText("NA");
        }
    }

    @Override
    public int getItemCount() {
        return listOfMyDeliveryBoyDetails.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private TextView tvDeliveryBoyName, tvUniqueCode, tvMobileNumber, tvEmail, tvStatus;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            tvEmail = itemView.findViewById(R.id.tvEmail);
            tvStatus = itemView.findViewById(R.id.tvStatus);
            tvUniqueCode = itemView.findViewById(R.id.tvUniqueCode);
            tvMobileNumber = itemView.findViewById(R.id.tvMobileNumber);
            tvDeliveryBoyName = itemView.findViewById(R.id.tvDeliveryBoyName);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            selectedPosition = getLayoutPosition();
            MyDeliveryBoyDetails myDeliveryBoyDetails = listOfMyDeliveryBoyDetails.get(selectedPosition);
            if (myDeliveryBoyDetails != null) {
                Integer id = myDeliveryBoyDetails.getUserDetails().getUserId();
                //Integer id = myDeliveryBoyDetails.getId();
                Intent deliverySuperintendentIntent = new Intent(context, DeliverySuperintendentDashboardActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString(context.getString(R.string.id), id.toString());
                deliverySuperintendentIntent.putExtras(bundle);
                context.startActivity(deliverySuperintendentIntent);
            }
        }
    }
}