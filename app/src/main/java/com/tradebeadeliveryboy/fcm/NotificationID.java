package com.tradebeadeliveryboy.fcm;

import java.util.concurrent.atomic.AtomicInteger;

public class NotificationID {
    private final static AtomicInteger integer = new AtomicInteger(0);

    /**
     * Used to get the unique notification id
     *
     * @return
     */
    public static int getID() {
        return integer.incrementAndGet();
    }
}
