package com.tradebeadeliveryboy.interfaces;

public interface LocationUpdateCallBack {
    void OnLatLongReceived(double latitude, double longitude);
}
