package com.tradebeadeliveryboy.interfaces;

public interface TransactionDetailsFilterCallBack {
    void transactionDetailsFilter(String startDate, String endDate, String lastDays);
}
