package com.tradebeadeliveryboy.interfaces;

public interface CloseCallBack {
    void close();
}
