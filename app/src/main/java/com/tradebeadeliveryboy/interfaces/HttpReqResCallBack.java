package com.tradebeadeliveryboy.interfaces;

public interface HttpReqResCallBack {
    void jsonResponseReceived(String jsonResponse, int statusCode, int requestType);
}
